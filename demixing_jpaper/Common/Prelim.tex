\section{Preliminaries}
\label{Sec::Perm}

%In this section, we introduce some notation and key definitions. 
Throughout this paper, $\|.\|_p$ denotes the $\ell_p$-norm of a vector in $\mathbb{R}^n$, and $\|A\|$ denotes the spectral norm of the matrix $A\in\mathbb{R}^{m\times n}$. Let $\Phi$ and $\Psi$ be orthonormal bases of $\R^n$. Define the set of sparse vectors in the bases $\Phi$ and $\Psi$ as follows:
\begin{align*}
K_1 &= \{\Phi a\ | \ \|a\|_0\leq s_1\}, \\ 
K_2 &= \{\Psi a\ | \ \|a\|_0\leq s_2\},
\end{align*}
and define $K = \{a\ | \ \|a\|_0\leq s\}.$
 %Also, $\|.\|_0$ is used t the the number of nonzero entries in a vector given in $\mathbb{R}^n$. 
\new{We use $B_2^n$ to denote the unit $\ell_2$ ball}. \new{Whenever we use the notation $t=[w;z]$, the vector $t$ is comprised by stacking column vectors $w$ and $z$.} We first define the following geometric concepts, restated from~\cite{plan2014high}.
\begin{definition}\label{def 3.3}
For a given set $K \in\mathbb{R}^n$, the local gaussian mean width is defined as follows $\forall~{\rho}>0$:
$$W_{\rho}(K) = \mathbb{E}\sup_{x, y\in K, \|x-y\|_2 \leq {\rho}}\langle g, x-y\rangle.$$
where $g\sim\mathcal{N}(0,I_{n \times n})$.
\end{definition}

%Next, we define the notion of a \emph{polar norm} with respect to a given subset $Q$ of the signal space:
\begin{definition}
For a given $x\in \mathbb{R}^n$ and a subset of $Q\in \mathbb{R}^n$, the polar norm with respect to $Q$ is defined as follows:
$$\|x\|_{Q^o} = \sup_{u\in Q}\langle x, u\rangle.$$
\end{definition} 
Furthermore, for a given subset of $Q\in \mathbb{R}^n$, we define $Q_{\rho} = (Q-Q)\cap \rho B_2^n$ $,\forall~{\rho}>0$. Since $Q_{\rho}$ is a symmetric set, one can show that the polar norm with respect to $Q_{\rho}$ defines a semi-norm. Next, we use the following standard notions from random matrix theory~\cite{vershynin2010introduction}:

\begin{definition} \label{subgau}
A random variable $X$ is called subgaussian if it satisfies the following:
\begin{align*}
\mathbb{E}\exp\left(\frac{c X^2}{\|X\|_{\psi_2}^2}\right)\leq 2,
\end{align*}
where $c >0$ is an absolute constant and $\|X\|_{\psi_2}$ denotes the $\psi_2$-norm which is defined as follows:
\begin{align*}
\|X\|_{\psi_2}=\sup_{p\geq 1}\frac{1}{\sqrt{p}}(\mathbb{E}|X|^p)^{\frac{1}{p}}.
\end{align*}
\new{A random vector $f\in\mathbb{R}^n$ is called a subgaussian random vector if all its marginals are subgaussian random variables, i.e, $\langle f,h\rangle$ is subgaussian for all $h\in\mathbb{R}^n$.} 
\end{definition}

\begin{definition}
A random vector-valued variable $v\in\mathbb{R}^n$ is called isotropic if  $\mathbb{E} vv^T=I_{n\times n}$.
\end{definition}

In order to analyze the computational aspects of our proposed algorithms (in particular, \textsc{DHT}), we will need the following definition from~\cite{negahban2009unified}:
\begin{definition}\label{rssrsc}
\new{Let $f:\mathbb{R}^{2n}\rightarrow\mathbb{R}$}. A loss function $f$ satisfies \textit{Restricted Strong Convexity/Smoothness (RSC/RSS)} if:
%\begin{align*}
%\frac{m_{4s}}{2}\|t_2-t_1\|^2 &\leq f(t_2) - f(t_1) - \langle\nabla f(t_1) , t_2-t_1\rangle \\
%& \leq\frac{M_{4s}}{2}\|t_2-t_1\|^2,
%\end{align*}
\begin{align*}
m_{4s}\leq\|\nabla^2_{\xi} f(t)\|\leq M_{4s}, \ \ \ t\in\mathbb{R}^{2n},
\end{align*}
where $\xi = \mathrm{supp}(t_1) \bigcup \mathrm{supp}(t_2)$, for all $\|t_i\|_0\leq 2s$ and $i=1,2$.
%where $\|t_i\|_0\leq 2s$ for $i=1,2$,
Also, $m_{4s}$ and $M_{4s}$ are called the RSC and RSS constants. \new{Here $\nabla^2_{\xi} f(t)$ denotes a $4s\times 4s$ sub-matrix of the Hessian matrix, $\nabla^2 f(t)$, comprised of row/column indices in $\xi$.}
\end{definition} 

As discussed earlier, the underlying assumption in all demixing problems of the form \eqref{MainModell} is that the constituent bases are sufficiently \textit{incoherent} as follows:
\begin{definition}\label{incoherence}
The orthonormal bases $\Phi$ and $\Psi$ are said to be $\varepsilon$-incoherent if:  
\begin{align}
\varepsilon = \sup_{\substack{\|u\|_0\leq s,\ \|v\|_0\leq s  \\ \|u\|_2 = 1,\ \|v\|_2 = 1}}|\langle{\Phi u, \Psi v}\rangle|.
\end{align}
\end{definition}

The parameter $\varepsilon$ is related to the so-called {mutual coherence} parameter of a matrix. If we consider the (overcomplete) dictionary $\Gamma = [\Phi \, \Psi]$, then the mutual coherence of $\Gamma$ is given by $\gamma = \max_{i \neq j} | (\Gamma^T \Gamma)_{ij} |$. Moreover, one can show that $\varepsilon \leq s \gamma$  \cite{foucart2013}.  

We now formally establish our \emph{signal model}. Consider a signal $x\in \mathbb{R}^n$ that is the superposition of a pair of sparse vectors in different bases, i.e.,
\begin{equation}\label{superpositoin}
x = \Phi w + \Psi z \, ,
\end{equation}
where $\Phi, \Psi\in \mathbb{R}^{n\times n}$ are orthonormal bases, and $w, z\in \mathbb{R}^n$ such that $\|w\|_0\leq s$, and  $\|z\|_0\leq s$.
We define the following quantities:
\new{
\begin{align}\label{sp}
\bar{x} = \frac{\Phi {w} + \Psi {z}}{\|\Phi {w} + \Psi {z}\|}_2 = \alpha(\Phi {w} + \Psi {z}) ,
\end{align}
}
where 
$\alpha = \frac{1}{\|\Phi {w} + \Psi {z}\|_2}$. %~\bar{w} = \frac{w}{\|w\|_2},~\bar{z} = \frac{z}{\|z\|_2}.$ 
Also, we define $t =  [w ;z ]\in\mathbb{R}^{2n}$ as the vector obtaining by stacking the individual coefficient vectors $w$ and $z$ of the components.
We now state our \emph{measurement model}. Consider the nonlinear observation model:
\begin{align}\label{MainModell}
y_i = g(a_i^Tx) + e_i, \ i=1\dots m,
\end{align}
where $x \in \mathbb{R}^n$ is the superposition signal given in~\eqref{superpositoin}, and $g : \mathbb{R} \mapsto \mathbb{R}$ is a nonlinear link function. 
We denote $\Theta(x)$ as the antiderivative of $g$, i.e., $\Theta'(x) = g(x)$. As mentioned above, depending on the knowledge of the link function $g$, we consider two scenarios:

\begin{enumerate}[leftmargin=*]
\item 
In the first scenario, $g$ may be non-smooth, non-invertible, or even unknown. %(However, for analysis purposes we will stipulate that $g$ satisfies certain additional regularity conditions, such as )
In this setting, we assume the noiseless observation model, i.e., $y = g(Ax)$. In addition, we assume that the measurement matrix is populated by i.i.d. standard normal random variables.

\item
In this setup, $g$ represents a known nonlinear, differentiable, and strictly monotonic function. Further, in this scenario, we assume that the observation $y_i$ is corrupted by a subgaussian additive noise with $\|e_i\|_{\psi_2}\leq\tau$ for $i=1, \ldots,m$ that is zero-mean and independent from $a_i$, i.e., $\mathbb{E}\left(e_i\right) = 0$ for $i=1, \ldots, m$. In addition, we assume that the measurement matrix consists of either (2a) isotropic random vectors incoherent with $\Phi$ and $\Psi$, or (2b) populated with subgaussian random variables.

\end{enumerate}

We highlight some additional clarifications for the second case. In particular, we make the following:
\begin{ass}
\label{ass_g}
There exist positive parameters $l_1, l_2 >0 \ (\text{resp., negative parameters } l_1, l_2<0)$ such that  $0<l_1\leq\ g^{\prime}(x)\leq l_2 \ (\text{resp.}~l_1\leq\ g^{\prime}(x)\leq l_2<0)$.
\end{ass} 
%In words, the derivative of the link function is strictly bounded either within a positive interval or within a negative interval. 
In this paper, we focus on the case when $0<l_1\leq\ g^{\prime}(x)\leq l_2$. The analysis of the complementary case is similar. 

The lower bound on $g^\prime(x)$ guarantees that the function $g$ is a monotonic function, i.e., if $x_1 < x_2$  then $g(x_1) < g(x_2)$. Moreover, the upper bound on $g^\prime(x)$ guarantees that the function $g$ is \textit{Lipschitz} with constant $l_2$. %As we will see in the next section, the lower bound assumption makes the objective function convex (or concave for negative interval, $i.e., l_1, l_2 <0$). 
Such assumptions are common in the nonlinear recovery literature~\cite{negahban2009unified, yang2015sparse}.%Also, bounding $g$ from below by a strictly positive number is an underlying assumption in the proof of Restricted Strong Convexity (RSC) of our objective function (see section~\ref{Sec::proof}).
%\new{I added remark 3.8}
%\begin{remark}
\footnote{Using the monotonicity property of $g$ that arises from Assumption~\ref{ass_g}, one might be tempted to  simply apply the inverse of the link function on the measurements $y_i$ in~\eqref{MainModell} convert the nonlinear demixing problem to the more amenable case of linear demixing, and then use any algorithm (e.g.,~\cite{spinIT}) for recovery of the constituent signals. However, this na\"{i}ve way could result in a large error in the estimation of the components, particularly in the presence of the noise $e_i$ in~\eqref{MainModell}; see~\cite{yang2015sparse}.}% for generic nonlinear recovery both from a theoretical as well as empirical standpoint.}  
%\end{remark}

%As discussed before, for the measurement matrix $A$, we consider two cases; case (i): we assume that the measurement vectors $a_i$ are independent, isotropic random vectors that are \emph{incoherent} with the bases $\Phi$ and $\Psi$  This assumption is more general than the i.i.d.\ standard normal assumption on the measurement matrix made in the first scenario, and is applicable to a wider range of measurement models. and case (ii): we assume that the rows of the matrix $A$ are independent subgaussian isotropic random vectors. We note that the standard normal measurement matrix which we assumed for the first scenario is a special instance of the this case.

In Case 2a, the vectors $a_i$ (i.e., the rows of $A$) are independent isotropic random vectors. For this case, in addition to incoherence between the component bases, we also need to define a measure of \emph{cross}-coherence between the measurement matrix $A$ and the dictionary $\Gamma$. %, i.e., we need that $A$ and $\Gamma$ are incoherent enough. %In other words, in order that the recovery of the constituent signals is possible, one needs to require that the rows of $A$ and the columns of $\Gamma$ to be dissimilar or incoherent enough. 
The following notion of cross-coherence was introduced in~\cite{candes2007sparsity}:

\begin{definition}\label{mutualcoherence}
The cross-coherence parameter between the measurement matrix $A$ and the dictionary $\Gamma=[\Phi \ \Psi]$ is defined as follows:
\begin{align}
\vartheta = \max_{i,j}\frac{a_i^T\Gamma_j}{\|a_i\|_2},
\end{align}
where $a_i$ and $\Gamma_j$ denote the $i^{\textrm{th}}$ row of the measurement matrix $A$ and the $j^{\textrm{th}}$ column of the dictionary $\Gamma$.% which has unit $\ell_2$-norm, respectively. %Since the matrix $\Gamma$ is the concatenation of the two orthonormal bases. its columns have unit $\ell_2$ norm.   
\end{definition}

The cross-coherence assumption implies that $\big{\|}a_i^T\Gamma_{\xi}\big{\|}_{\infty}\leq\vartheta$ for $i=1,\ldots,m$, where $\Gamma_{\xi}$ denotes the restriction of the columns of the dictionary to a subset of indices $\xi \subseteq [2n]$, with $|\xi|\leq 4s$ such that $2s$ columns are selected from each of $\Phi$ and $\Psi$.

