%Be Sure that the CVX toolbox is already installed! %

close all;
clear all;
M = 100;
N = 1000;
k = 6;      % Sparsity %

A = randn(M,N);     
D = diag(1./sqrt(diag(A'*A)));
A = A*D;            %Generate Isotropic Random Matrix%

idx = randperm(N);

idx_nz = idx(1:1:k);
coef_nz = 2*rand(1,k)-1;

w = zeros(N,1);

w(idx_nz) = coef_nz;
w = w./norm(w);
b = (A*w);           % Linear Measuement Vector %
% y = 2*b+sin(b);      % Nonlinear function %
y = sign(b);       % Another Nonlinear Function%

% y = awgn(b,50,'measured')
bn= zeros(M,1);

% For nonlinear g, replace b with y%
cvx_begin 
    variable w_est(N) ;
    minimize norm(w_est-A'*b/M,2)     %The target function according to the paper, do not work!%
%     minimize norm(w_est-A'*b/1,2)   % This target could not work either%
%     minimize norm(A*w_est-b,2)      % Only this function could work, but this is surely not the target of the paper%  
    subject to 
        norm(w_est,1)<sqrt(k)
cvx_end

disp(norm(w_est,2))     %Show the l2-norm of w_est, check if it has unit l2-norm%


plot(w);
hold on 
plot(w_est,'r')
