\section{Algorithms and Theoretical Results}
\label{Sec::Alg}

Having defined the above quantities, we now present our main results. As per the previous section, we study two distinct scenarios:

\subsection{When the link function $g$ is unknown}
Recall that we wish to recover components $w$ and $z$ given the nonlinear measurements $y$ and the matrix $A$. Here and below, for simplicity we assume that the sparsity levels $s_1$ and $s_2$, specifying the sets $K_1$ and $K_2$, are equal, i.e., $s_1 = s_2 = s$. 
The algorithm (and analysis) effortlessly extends to the case of unequal sparsity levels.
Our proposed algorithm, that we call \textsc{OneShot}, is described in pseudocode form below as Algorithm~\ref{alg:oneshot}.

The mechanism of \textsc{OneShot} is simple, and \emph{deliberately} myopic. At a high level, \textsc{OneShot} first constructs a \emph{linear estimator} of the target superposition signal, denoted by $\widehat{x}_\text{\lin} = \frac{1}{m} A^T y$. Then, it performs independent projections of $\widehat{x}_\text{\lin}$ onto the constraint sets $K_1$ and $K_2$. Finally, it combines these two projections to obtain the final estimate of the target superposition signal.

\begin{algorithm}[!t]
\caption{\label{alg:oneshot}
\textsc{OneShot}}
\textbf{Inputs:} Basis matrices $\Phi$ and $\Psi$, measurement matrix $A$, measurements $y$, sparsity level $s$. \\
\textbf{Outputs:} Estimates  $\widehat{x}=\Phi\widehat{w} + \Psi\widehat{z}$, $\widehat{w}\in K_1$, $\widehat{z}\in K_2$

\setlength{\parskip}{1em}
$\widehat{x}_\text{\lin}\leftarrow\frac{1}{m}A^{T}y$\qquad~\{form linear estimator\}\\
$b_1\leftarrow\Phi^*\widehat{x}_\lin$\qquad~~~\{forming first proxy\}\\
$\widehat{w}\leftarrow\mathcal{P}_s(b_1)$\qquad~~~\{sparse projection\}\\
$b_2\leftarrow\Psi^*\widehat{x}_\lin$\qquad~~~\{forming second proxy\}\\
$\widehat{z}\leftarrow\mathcal{P}_s(b_2)$\qquad~~~~\{sparse projection\}\\
$\widehat{x}\leftarrow\Phi\widehat{w} + \Psi\widehat{z}$\quad~~~\{Estimating $\widehat{x}$\}
\end{algorithm}

In the above description of \textsc{OneShot}, we have used the following \emph{projection} operators:
$$\widehat{w} = \mathcal{P}_s(\Phi^*\widehat{x}_\text{\lin}),  \quad \hat{z} = \mathcal{P}_s(\Psi^*\widehat{x}_\text{\lin}).$$
Here, $\mathcal{P}_s$ denotes the projection onto the set of (canonical) $s$-sparse signals $K$ and can be implemented by hard thresholding, i.e., any procedure that retains the $s$ largest coefficients of a vector (in terms of absolute value) and sets the others to zero\footnote{The typical way is to sort the coefficients by magnitude and retain the $s$ largest entries, but other methods such as randomized selection can also be used.}. Ties between coefficients are broken arbitrarily. Observe that  \textsc{OneShot} is \emph{not} an iterative algorithm, and this in fact enables us to achieve a fast running time. 

We now provide a rigorous performance analysis of \textsc{OneShot}. Our proofs follow the geometric approach provided in~\cite{plan2014high}, specialized to the demixing problem. In particular, we derive an upper bound on the estimation error of the \emph{component} signals $w$ and $z$, modulo scaling factors.
In our proofs, we use the following result from~\cite{plan2014high}, restated here for completeness.
\begin{lemma}(Quality of linear estimator).\label{lemma 4.1}
Given the model in Equation \eqref{superpositoin}, the linear estimator, $\widehat{x}_\text{\lin}$, is an unbiased estimator of $\bar{x}$ (defined in~\eqref{sp}) up to constants. That is, $\mathbb{E}(\widehat{x}_\textrm{lin})= \mu\bar{x}$ and:
$\mathbb{E}\|\widehat{x}_\textrm{lin} - \mu\bar{x}\|_2^2 = \frac{1}{m}[\sigma^2 +\eta^2(n-1)],$
where 
$\mu = \mathbb{E}(y_1\langle a_1, \bar{x}\rangle),~\sigma^2 = Var(y_1\langle a_1, \bar{x}\rangle),~\eta^2 = \mathbb{E}(y_1^2).
$
\end{lemma}

We now state our first main theoretical result, with the full proof provided below in Section \ref{Sec::proof}.

\begin{theorem}
\label{thm:main}
Let $y\in\mathbb{R}^m$ be the set of measurements \new{generated using a nonlinear function $g$ that satisfies the conditions of Lemma (4.9) in~\cite{plan2014high}}\footnote{Based on this lemma, the nonlinear function $g$ is odd, nondecreasing, and sub-multiplicative on $\mathbb{R}^+$.}. Let $A\in \mathbb{R}^{m\times n}$ be a random matrix with i.i.d.\ standard normal entries. Also, let $\Phi, \Psi\in \mathbb{R}^{n\times n}$ are bases with $\varepsilon \leq 0.65$, where $\varepsilon$ is as defined in Def.\ \ref{incoherence}. If we use \textsc{Oneshot} to recover estimates of $w$ and $z$ (modulo a scaling) described in equations \eqref{superpositoin} and \eqref{sp}, then the estimation error for $w$ (similarly, $z$) satisfies the following upper bound in expectation $\forall {\rho} >0$: 
\begin{align}\label{MT}
\mathbb{E}\|\widehat{w}-\mu\alpha{w}\|_2\leq {\rho} + \frac{2}{\sqrt{m}}\left(4\sigma + \eta\frac{W_{\rho}(K)}{\rho}\right) +8\mu\varepsilon \, .
\end{align}
\end{theorem}
The constant $0.65$ is chosen for convenience and can be strengthened. The authors of~\cite{planRmanrobust,plan2014high} provide upper bounds on the local mean width $W_{\rho}(K)$ of the set of $s$-sparse vectors. In particular, for any ${\rho} > 0$ they show that $W_{\rho}(K) \leq C {\rho} \sqrt{s \log (2n/s)}$ for some absolute constant $C$. By plugging in this bound and letting ${\rho}\rightarrow 0$, we can combine components $\widehat{w}$ and $\widehat{z}$ which gives the following:
\begin{corollary}
\label{corr:estx}
With the same assumptions as Theorem~\ref{thm:main}, the error of nonlinear estimation incurred by the final output $\widehat{x}$ satisfies the upper bound:
\begin{align}\label{eq:estx}
\mathbb{E}\|\widehat{x}-\mu\bar{x}\|_2\leq\frac{4}{\sqrt{m}}\left(4\sigma + C\eta\sqrt{s \log (2n/s)}\right) +16\mu\varepsilon.
\end{align}
\end{corollary}

\begin{corollary}(Example quantitative result). The constants $\sigma, \eta, \mu$ depend on the nature of the nonlinear function $f$, and are often rather mild. For example, if $f(x) = \mathrm{sign}(x)$, then we may substitute
$$\mu = \sqrt{\frac{2}{\pi}}\approx 0.8,\qquad\sigma^2 = 1- \frac{2}{\pi}\approx 0.6,\qquad\eta^2=1,$$
in the above statement.
Hence, the bound in \eqref{eq:estx} becomes:
\begin{align}
\mathbb{E}\|\widehat{x}-\mu\bar{x}\|_2\leq\frac{4}{\sqrt{m}}\left(3.1 + C\sqrt{s \log (2n/s)}\right) +13\varepsilon \, .
\end{align}
\end{corollary}

\begin{proof}
Using Lemma \ref{lemma 4.1}, $\mu = \mathbb{E}(y_i\langle a_i, \bar{x}\rangle)$ where $y_i = \mathrm{sign}(\langle a_i, x\rangle)$. Since $a_i\sim\mathcal{N}(0,I)$ and $\bar{x}$ has unit norm, $\langle a_i, \bar{x}\rangle\sim\mathcal{N}(0,1)$. Thus, $\mu = \mathbb{E}|g| = \sqrt{\frac{2}{\pi}}$ where $g\sim\mathcal{N}(0,I)$. Moreover, we can write $\sigma^2 = \mathbb{E}(|g|^2)-\mu^2 = 1- \frac{2}{\pi}$. Here, we have used the fact that $|g|^2$ obeys the $\chi^2_1$ distribution with mean 1. Finally, $\eta^2 = \mathbb{E}(y_1^2) = 1$.
\end{proof}

In contrast with demixing algorithms for traditional (linear) observation models, our estimated signal $\widehat{x}$ outputting from \textsc{OneShot} can differ from the true signal $x$ by a scale factor. Next, suppose we fix $\kappa > 0$ as a small constant, and suppose that the incoherence parameter $\varepsilon = c\kappa$ for some constant $c$, and that the number of measurements scales as: 
\begin{equation}
\label{eq:oneshot_samples}
m = \mathcal{O}\left(\frac{s}{\kappa^2}\log\frac{n}{s}\right).
\end{equation}
Then, the (expected) estimation error $\| \widehat{x} - \mu \bar{x} \| \leq O(\kappa)$. In other words, the \emph{sample complexity} of \textsc{OneShot} is given by $m = \mathcal{O}(\frac{1}{\kappa^2} s \log(n/s))$, which resembles results for the linear observation case~\cite{spinIT,plan2014high}\footnote{Here, we use the term ``sample-complexity" as the number of measurements required by a given algorithm to achieve an estimation error $\kappa$. However, we must mention that algorithms for the linear observation model are able to achieve stronger sample complexity bounds that are independent of $\kappa$.}.

We observe that the estimation error in \eqref{eq:estx} is upper-bounded by $\mathcal{O}(\varepsilon)$. This  is meaningful only when $\varepsilon \ll 1$, or when $s \gamma \ll 1$. Per the Welch Bound~\cite{foucart2013}, the mutual coherence $\gamma$ satisfies $\gamma \geq 1/\sqrt{n}$. Therefore, Theorem \ref{thm:main} provides non-trivial results only when $s = o(\sqrt{n})$. This is consistent with the \emph{square-root bottleneck} that is often observed in demixing problems; see~\cite{tropp07} for detailed discussions.

The above theorem obtains a bound on the expected value of the estimation error. We can derive a similar upper bound that holds with high probability. In this theorem, we assume that the \emph{measurements} $y_i$ for $i=1,2,\ldots,m$ have a \emph{sub-gaussian} distribution (according to Def.~\ref{subgau}). We obtain the following result, with full proof deferred to Section \ref{Sec::proof}.

\begin{theorem}(High-probability version of Thm.\ \ref{thm:main}.)
\label{thm:high}
Let $y\in\mathcal{R}^m$ be a set of measurements with a sub-gaussian distribution. Assume that $A\in \mathbb{R}^{m\times n}$ is a random matrix with $i.i.d$ standard normal entries. Also, assume that $\Phi, \Psi\in \mathbb{R}^{n\times n}$ are two bases with incoherence $\varepsilon\leq 0.65$ as in Definition \ref{incoherence}. Let $0\leq s'\leq\sqrt{m}$. If we use \textsc{Oneshot} to recover $w$ and $z$ (up to a scaling) described in \eqref{superpositoin} and \eqref{sp}, then the estimation error of the output of \textsc{Oneshot} satisfies the following:

\begin{align} 
\|\widehat{x} - \mu\bar{x}\|_2 \leq \frac{4\eta}{\sqrt{m}}\left(3s'+C'\sqrt{s\log\frac{2n}{s}}\right) + 16\mu\varepsilon,
\end{align}
with probability at least $1-4\exp(-\frac{cs'^2\eta^4}{\|y_1\|_{\psi_2}^4})$ where $C', c> 0$ are absolute constants. The coefficients $\mu, \sigma$, and $\eta$ are given in Lemma \ref{lemma 4.1}. Here, $\|y_1\|_{\psi_2}$ denotes the $\psi_2$-norm of the first measurement $y_1$ (Definition~\ref{subgau}). 

\end{theorem}

%\begin{remark}
In Theorem~\ref{thm:high}, we stated the tail probability bound of the estimation error for the superposition signal, $x$. Similar to Theorem~\ref{thm:main}, we can derive a completely analogous tail probability bound in terms of the constituent signals $w$ and $z$.
%\end{remark}

\subsection{When the link function $g$ is known}\label{SubDHT}

The advantages of \textsc{OneShot} is that it enables fast demixing, and can handle even unknown, non-differentiable link functions. But its primary weakness is that the sparse components are recovered only up to an arbitrary scale factor. This can lead to high estimation errors in practice, and this can be unsatisfactory in applications. Moreover, even for reliable recovery up to a scale factor, its sample complexity is inversely dependent on the estimation error. To solve these problems, we propose a different, iterative algorithm for recovering the signal components. Here, the main difference is that the algorithm is assumed to possess (perfect) knowledge of the nonlinear link function, $g$. 

Recall that we define $\Gamma = [\Phi \ \Psi]$ and $t = [w; z]\in\mathbb{R}^{2n}$. First, we formulate our demixing problem as the minimization of a special loss function $F(t)$:
\begin{equation} \label{optprob}
\begin{aligned}
& \underset{t \in \mathbb{R}^{2n}}{\text{min}}
\ \ F(t) = \frac{1}{m}\sum_{i=1}^m \Theta(a_i^T\Gamma t) - y_i a_i^T\Gamma t \\
& \text{s.\ t.}  \quad \|t\|_0\leq 2s.
\end{aligned}
\end{equation}

Observe that the loss function $F(t)$ is \emph{not} the typical squared-error function commonly encountered in statistics and signal processing applications. In contrast, it heavily depends on the nonlinear link function $g$ (via its integral $\Theta$). Instead, such loss functions are usually used in GLM and SIM estimation in the statistics literature~\cite{negahban2009unified}. 
In fact, the objective function in~\eqref{optprob} can be considered as the \emph{sample} version of the problem: 
$$\min_{t \in \mathbb{R}^{2n}} \  \mathbb{E}(\Theta(a^T\Gamma t) - y a^T\Gamma t),$$ 
where $a, y$ and $\Gamma$ satisfies the model~\eqref{MainModell}. It is not hard to show that the solution of this problem satisfies $\mathbb{E}(y_i|a_i) = g(a_i^T\Gamma t)$. % which coincides with the assumption on the subgaussian noise in section~\ref{Sec::Perm}~\cite{ganti2015learning}. 
We note that the gradient of the loss function can be calculated in closed form:
\begin{align}\label{GradientofOb}
\nabla F(t) &= \frac{1}{m}\sum_{i=1}^{m}\Gamma^T a_i g(a_i^T\Gamma t) - y_i\Gamma^Ta_i, \\
& = \frac{1}{m} \Gamma^T A^T (g(A \Gamma t) - y) \nonumber.
\end{align}

We now propose an \emph{iterative} algorithm for solving~\eqref{optprob} that we call it \textsc{Demixing with Hard Thresholding (DHT)}. The method is detailed in Algorithm \ref{algDHT}.
At a high level, \textsc{DHT} iteratively refines its estimates of the constituent signals $w, z$ (and the superposition signal $x$). At any given iteration, it constructs the gradient using~\eqref{GradientofOb}. Next, it updates the current estimate according to the gradient update being determined in Algorithm \ref{algDHT}. Then, it performs hard thresholding using the operator $\mathcal{P}_{2s}$ to obtain the new estimate of the components $w$ and $z$. This procedure is repeated until a stopping criterion is met. See Section~\ref{Sec::Res} for the choice of stopping criterion and other details.
%\begin{remark}
We mention that the initialization step in Algorithm~\ref{algDHT} is arbitrary and can be implemented (for example) by running \textsc{OneShot} and obtaining initial points $\left(x^0, w^0, z^0\right)$. We use this initialization in our simulation results.
%\end{remark}

\begin{algorithm}[t]
\caption{Demixing with Hard Thresholding (DHT)
\label{algDHT}}
\begin{algorithmic}
\STATE \textbf{Inputs:} Bases $\Phi$ and $\Psi$, measurement matrix $A$, link function $g$, measurements $y$, sparsity level $s$, step size $\eta'$. 
\STATE \textbf{Outputs:} Estimates  $\widehat{x}=\Phi\widehat{w} + \Psi\widehat{z}$, $\widehat{w}$, $\widehat{z}$
\STATE\textbf{Initialization:}
\STATE$\left(x^0, w^0, z^0\right)\leftarrow\textsc{arbitrary initialization}$
\STATE$k \leftarrow 0$
\WHILE{$k\leq N$}
\STATE $t^k \leftarrow [ w^k ; z^k ]$\quad\quad\quad\quad\quad~\{forming constituent vector\}
\STATE $t_1^k\leftarrow\frac{1}{m}\Phi^TA^T(g(Ax^k) - y)$ 
\STATE$t_2^k\leftarrow\frac{1}{m}\Psi^TA^T(g(Ax^k) - y)$
\STATE$\nabla F^k \leftarrow [ t_1^k ; t_2^k ]$
\quad\quad\quad\quad~\{forming gradient\}
\STATE${\tilde{t}}^k = t^k - \eta'\nabla F^k$
\quad\quad\quad~~\{gradient update\}
\STATE$[ w^k ; z^k ]\leftarrow\mathcal{P}_{2s}\left(\tilde{t}^k\right)$  
\quad\quad~~\{sparse projection\}
\STATE$x^k\leftarrow\Phi w^k + \Psi z^k$\quad\quad\quad~~\{estimating $\widehat{x}$\}
\STATE$k\leftarrow k+1$
\ENDWHILE
\STATE\textbf{Return:} $\left(\widehat{w}, \widehat{z}\right)\leftarrow \left(w^N, z^N\right)$
\end{algorithmic}
\end{algorithm}

Implicitly, we have again assumed that both component vectors $w$ and $z$ are $s$-sparse; however, as above we mention that Algorithm~\ref{algDHT} and the corresponding analysis easily extend to differing levels of sparsity in the two components. In Algorithm \ref{algDHT}, $\mathcal{P}_{2s}$ denotes the projection of vector $\tilde{t}^k\in\mathbb{R}^{2n}$ on the set of $2s$ sparse vectors, again implemented via hard thresholding.

We now provide our second main theoretical result, supporting the convergence analysis of \textsc{DHT}. In particular, we derive an upper bound on the estimation error of the constituent vector $t$ (and therefore, the component signals $w,z$). The proofs of Theorems~\ref{mainThConvergence},~\ref{SampleComplexityNonormal} and~\ref{SampleComplexitysubg} are deferred to section~\ref{Sec::proof}.

\begin{theorem}(Performance of \textsc{DHT})
\label{mainThConvergence}
Consider the measurement model~\eqref{MainModell} with all the assumptions mentioned for the second scenario in Section~\ref{Sec::Perm}. Suppose that the corresponding objective function $F$ satisfies the RSS/RSC properties with constants $M_{6s}$ and $m_{6s}$ on the set $J_k$ with $|J_k|\leq 6s$ ($k$ denotes the $k^{th}$ iteration) such that $1\leq\frac{M_{6s}}{m_{6s}}\leq\frac{2}{\sqrt{3}}$. Choose a step size parameter $\eta'$ with $\frac{0.5}{M_{6s}}<\eta^{\prime}<\frac{1.5}{m_{6s}}.$
%Let $y\in\mathbb{R}^m$ be given nonlinear measurements according to model~\eqref{MainModell}. Also, assume that $A\in\mathbb{R}^{m\times n}$ be a measurement matrix which its rows are isotropic random vector which independently drawn from a known distribution and we have $\big{\|}a_i^T\Gamma_{\xi}\big{\|}_{\infty}\leq\vartheta$, $i=1\dots m$ and $\vartheta$ denotes the mutual coherence between $A$ and $\Gamma_{\xi}$. Also, let $\Phi, \Psi$ be two  $\varepsilon$-incoherence bases and $g$ be a known link function which is a smooth function such that $0<l_1\leq g^{\prime}\leq l_2$ for some $l_2\geq l_1 >0$. Furthermore, let $e_i$ denote the additive subgaussian noise corresponding to the measurement $i$ with $\|e_i\|_{\psi_2}\leq\tau$ for $i=1\dots m$. 
Then, \textsc{DHT} outputs a sequence of estimates $t^k = [w^k; z^k]$ that satisfies the following upper bound (in expectation) for $k\geq 1$: %\new{I dropped E from both sides of (4.8), but express the expectation of the norm of gradient}
\begin{align}
\label{eq:linconverge}
\|t^{k+1} - t^*\|_2\leq\left(2q\right)^k\|t^0-t^*\|_2 + C\tau\sqrt{\frac{s}{m}}, 
\end{align}
where $q = \sqrt{1+{\eta^{\prime}}^2M_{6s}^2-2\eta^{\prime} m_{6s}}$  and $C>0$ is a constant that  depends on the step size $\eta^{\prime}$ and the convergence rate $q$. % Also, $s$ denotes the sparsity of the constituent signals, $n$ represents the ambient dimension of the constituent signals, $t^*$ denotes the solution of~\eqref{optprob}
Also, $t^{*} = [w ;z]$ where $w$ and $z$ are the true (unknown) vectors in model~\eqref{MainModel}. 
\end{theorem}

Equation \eqref{eq:linconverge} indicates that Algorithm~\ref{algDHT} (\textsc{DHT}) enjoys a linear rate of convergence. In particular, for the noiseless case $\tau = 0$, this implies that Alg.\ \ref{algDHT} returns a solution with accuracy $\kappa$ after $N = \mathcal{O}(\log \frac{\norm{t^0 - t}_2}{\kappa})$ iterations. The proof of Theorem~\ref{mainThConvergence} leverages the fact that the objective function $F(t)$ in~\eqref{optprob} satisfies the RSC/RSS conditions specified in Definition~\ref{rssrsc}. Please refer to Section~\ref{Sec::proof} for a more detailed discussion.
%\begin{remark}
Moreover, we observe that in contrast with \textsc{OneShot}, \textsc{DHT} can recover the components $w$ and $z$ without any ambiguity in scaling factor, as depicted in the bound~\eqref{eq:linconverge}. We also verify this observation empirically in our simulation results in Section~\ref{Sec::Res}.
%\end{remark}

Echoing our discussion in Section~\ref{Sec::Perm}, we consider two different models for the measurement matrix $A$ and derive upper bounds on the sample complexity of \textsc{DHT} corresponding to each case. First, we present the sample complexity of Alg.\ \ref{algDHT} when the measurements are chosen to be isotropic random vectors, corresponding to Case (2a) described in the introduction:

\begin{theorem}(Sample complexity when the rows of $A$ are isotropic.)
\label{SampleComplexityNonormal}
Suppose that the rows of $A$ are independent isotropic random vectors.
In order to achieve the requisite RSS/RSC properties of Theorem~\ref{mainThConvergence}, the number of samples needs to scale as:
\[
m=\mathcal{O}(s\log n\log^2s\log(s\log n)),
\]
%\new{--I think we need to keep this sentence--with the optimization error equals to $\mathcal{O}(1)$
provided that the bases $\Phi$ and $\Psi$ are incoherent enough.
\end{theorem}
%\new{We have not mentioned that the exponent in the sample complexity can be slightly improved }

The sample complexity mentioned in Theorem~\ref{SampleComplexityNonormal} incurs an extra (possibly parasitic) poly-logarithmic factor relative to the sample complexity of \textsc{OneShot}, stated in \eqref{eq:oneshot_samples}. However, the drawback of \textsc{OneShot} is that the sample complexity depends inversely on the estimation error $\kappa$, and therefore a very small target error would incur a high overhead in terms of number of samples. 

Removing all the extra logarithmic factors remains an open problem in general (although some improvements can be obtained using the method of~\cite{fourier_samples}). However, if we assume  additional structure in the measurement matrix $A$, we can decrease the sample complexity even further. This corresponds to Case 2b.

\begin{theorem}(Sample complexity when the elements of $A$ are subgaussian.)
\label{SampleComplexitysubg}
Assume that all assumptions and definitions in Theorem~\ref{mainThConvergence} holds except that the rows of matrix $A$ are independent subgaussian isotropic random vectors. Then, in order to achieve the requisite RSS/RSC properties of Theorem~\ref{mainThConvergence}, the number of samples needs to scale as:
\[
m=\mathcal{O}\left(s\log \frac{n}{s} \right),
\]
provided that the bases $\Phi$ and $\Psi$ are incoherent enough.
\end{theorem}

The leading big-Oh constant in the expression for $m$ in Theorems~\ref{SampleComplexityNonormal} and~\ref{SampleComplexitysubg} is somewhat complicated, and hides the dependence on the incoherence parameter $\varepsilon$, the mutual coherence $\vartheta$, the RSC/RSS constants, and the growth parameters of the link function $l_1$ and $l_2$. Please see section~\ref{Sec::proof} for more details.

%As a specific instance of the measurement matrix $A$ with independent subgaussian isotropic rows, we focus on the matrix $A$ with entries which are standard normal random variables and restate the result of Theorem~\ref{mainThConvergence} for \textsc{DHT} using Corollary~\ref{corr:estx} as follows:
%
%\begin{corollary}(Linear convergence of \textsc{DHT} for standard normal measurement matrix)
%\label{mainThConvergenceNormal}
%Assume that all assumptions and definitions in Theorem~\ref{mainThConvergence} holds except that the entries of matrix $A$ are independent standard normal random variables. Then, \textsc{DHT} with \textsc{OneShot} as the initialization step outputs $w^N\in K_1, z^N\in K_2$ such that the estimation error of the constituent vector satisfies the following upper bound for any $k\geq 1$:
%\begin{align*}
%\mathbb{E}\|t^{k+1} - t^*\|_2\leq\left(2q\right)^k\left(\mathcal{O}(\varepsilon)+\sqrt{\frac{s\log\frac{n}{s}}{m}}+2\mathbb{E}\|w^*-\mu\alpha\bar{w}^*\|_2\right) + C\tau\sqrt{\frac{s}{m}},
%\end{align*}
%where $q, \ C$ and other quantities are defined as the same in Theory~\ref{mainThConvergence}. In particular, for the noiseless case $\tau = 0$, this implies that Alg.\ \ref{algDHT} returns a solution with accuracy $\kappa$ after $N = \left\lceil{\log\frac{\mathcal{O}(\varepsilon)+\sqrt{\frac{s\log\frac{n}{s}}{m} }+2\mathbb{E}\|w^*-\mu\alpha\bar{w}^*\|_2}{\kappa}}\right\rceil$. In addition, the sample complexity or the required number of measurements to reliably recover constituent signals $w$ and $z$ is given by $m=\mathcal{O}(s\log\frac{n}{s})$ with the optimization error which is given by $\mathcal{O}(1)$.
%\end{corollary}
%
%\begin{proof}
%If we initialize \textsc{DHT} with \textsc{OneShot}, we can use the bound in~\eqref{MT} in the expression of the estimation error in~\eqref{eq:linconverge}. Also, we know that constituent vector satisfies $\|t^0-t^*\|_2\leq\|w^0-w^*\| + \|z^0-z^*\|_2$. Thus,  
%\begin{align}\label{normalinstan}
%\mathbb{E}\|t^0-t^*\|_2\leq 2\mathbb{E}\|w^0-w^*\|_2\leq2\mathbb{E}\|w^0-\mu\alpha\bar{w}^*\|_2 + 2\mathbb{E}\|w^*-\mu\alpha\bar{w}^*\|_2
%\end{align}
%where $\bar{w}^*$ denotes the normalized vector $w^*$ such that it has unit $\ell_2$-norm and parameters $\mu$ and $\alpha$ have been defined in section~\ref{Sec::Perm} and lemma~\ref{lemma 4.1}, respectively. Now if we plug in~\eqref{normalinstan} into the bound in~\eqref{eq:linconverge} and use the bound in~\eqref{MT}, we obtain:
%\begin{align}
%\mathbb{E}\|t^{k+1} - t^*\|_2\leq\left(2q\right)^k\left(\mathcal{O}(\varepsilon)+\sqrt{\frac{s\log\frac{n}{s}}{m}}+2\mathbb{E}\|w^*-\mu\alpha\bar{w}^*\|_2\right)+ C\tau\sqrt{\frac{s}{m}},
%\end{align}
%where $\varepsilon$ denotes the incoherence parameter between bases $\Phi$ and $\Psi$ being defined in definition~\ref{incoherence}. Now in the noiseless case ($\tau=0$), we obtain the required number of iterations for achieving the accuracy $\kappa$ as claimed in the corollary by solving the inequality $\left(2q\right)^k\left(\mathcal{O}(\varepsilon)+\sqrt{\frac{s\log\frac{n}{s}}{m}}+2\mathbb{E}\|w^*-\mu\alpha\bar{w}^*\|_2\right)\leq\kappa$. Since the measurement matrix in this corollary is the special case of our second case mentioned in Theorem~\ref{SampleComplexityNonormal}, the sample complexity and the optimization error is followed exactly by the same analysis for Theorem~\ref{SampleComplexityNonormal} (see section~\ref{Sec::proof}).
%\end{proof}

%\begin{remark}
In Theorem~\ref{mainThConvergence}, we expressed the upper bounds on the estimation error in terms of the constituent vector, $t$. It is easy to translate these results in terms of the component vectors $w$ and $z$ using the triangle inequality:
\begin{align*}
\max\{\|w^0-w^*\|_2,\|z^0-z^*\|_2\}\leq\|t^0-t^*\|_2\leq\|w^0-w^*\|_2 + \|z^0-z^*\|_2.
\end{align*}
See Section~\ref{Sec::proof} for proofs and futher details.
%\end{remark}
%\new{\begin{remark} ---I am not sure to mention this or not---
%Also, in Theorem~\ref{mainThConvergence}, we derive bounds on the \emph{expected} value of the estimation error. It is also possible to state these results in terms of tail probability, analogous to Theorem~\ref{thm:high}. The extension is straightforward using a union-bound argument and we omit it here.
%\end{remark}}