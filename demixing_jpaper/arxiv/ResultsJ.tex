\section{Experimental Results}
\label{Sec::Res}

In this section, we provide a range of numerical experiments for our proposed algorithms based on synthetic and real data. We compare the performance of \textsc{OneShot} and \textsc{DHT} with a LASSO-type technique for demixing, as well as a heuristic version of \textsc{OneShot} based on soft thresholding (inspired by the approach proposed in~\cite{yang}). We call these methods \emph{Nonlinear convex demixing with LASSO} or \textsc{(NlcdLASSO)}, and \emph{Demixing with Soft Thresholding} or \textsc{DST}, respectively. Before describing our simulation results, we briefly describe these two methods. 

\textsc{NlcdLASSO} is a \new{heuristic} method \new{motivated by}~\cite{plan2014high}, although it was not explicitly developed in the demixing context. Using our notation from Section \ref{Sec::Perm} and \ref{Sec::Alg}, \textsc{NlcdLASSO} solves the following convex problem:
\begin{equation} \label{prob 6.1}
\begin{aligned}
& \underset{z,w}{\text{min}}
& &  \big{\|}\widehat{x}_{\lin} - [\Phi~\Psi][w ; z] \big{\|}_2\\
& \text{subject to} 
& & \|w\|_1\leq \sqrt{s},\quad\|z\|_1\leq \sqrt{s}.
%& & \big{\|}[w ;z]\big{\|}_1\leq 2\sqrt{s}.
\end{aligned}
\end{equation}
Here, $\widehat{x}_{\lin}$ denotes the proxy of $x$ (equal to $\frac{1}{m} A^T y$) and $s$ denotes the sparsity level of signals $w$ and $z$ in basis $\Phi$ and $\Psi$, respectively. The constraints in problem \eqref{prob 6.1} are convex penalties reflecting the knowledge that $w$ and $z$ are $s$-sparse and have unit $\ell_2$-norm (since the
nonlinearity is unknown, we have a scale ambiguity, and therefore w.l.o.g. we can assume that the
underlying signals lie in the unit ball). % \new{--Should we say that in simulation we normalize $w$ and $z$--}. 
The outputs of this algorithm are the estimates $\widehat{w}$, $\widehat{x}$, and $\widehat{x} = \Phi\widehat{w} + \Psi\widehat{z}.$

To solve the optimization problem in \eqref{prob 6.1}, we have used the SPGL1 solver \cite{Berg2008,spgl12007}. This solver can handle large scale problems, which is the scenario that we have used in our experimental evaluations. We impose the joint constraint $\|t\|_1 = \| [w; z] \|_1 \leq 2\sqrt{s}$ which is a slight relaxation of the constraints in ~\ref{prob 6.1}. The upper-bound of $\sqrt{s}$ in the constraints is a worst-case criterion; therefore, for a fairer comparison, we also include simulation results with the constraint $\|t\|_1\leq \varrho$, where $\varrho$ has been tuned to the best of our ability. 
%We also have to mention that problem~\eqref{prob 6.1} can be solved using ADMM algorithm~\cite{gabay1976dual,hajinezhad2015nonconvex}.

On the other hand, \textsc{DST} solves the optimization problem~\eqref{optprob} via a convex relaxation of the sparsity constraint. In other words, this method attempts to  solve the following relaxed version of the problem~\eqref{optprob}:

\begin{equation}
\begin{aligned}
& \underset{t}{\text{min}}
\ \ \frac{1}{m}\sum_{i=1}^m \Theta(a_i^T\Gamma t) - y_i a_i^T\Gamma t + \beta'\|t\|_1,
\end{aligned}
\end{equation}
where $\|t\|_1$ represents $l_1$-norm of the constituent vector $t$ and $\beta' >0$ denotes the tuning parameter. The solution of this problem at iteration $k$ is given by soft thresholding operator as follows:
\begin{align*}
t^{k+1} = S_{\beta'\eta^{\prime}}(t^k - \eta^{\prime}\nabla F(t^k)),
\end{align*}
where $\eta^{\prime}$ denotes the step size, and the soft thresholding operator, $S_{\lambda}(.)$ is given by:
\[
    S_{\lambda}(y) =
\begin{cases}
    y-\lambda \, , \ & \text{if } y> \lambda\\
    0        \, ,       \ & \text{if } |y| \leq\lambda\\
    y+\lambda  \, , \ & \text{if } y< -\lambda.
\end{cases}
\]

Both \textsc{OneShot} and \textsc{NlcdLASSO} do not assume knowledge of the link function, and consequently return a solution up to a scalar ambiguity. Therefore, to compare performance across algorithms, we use the (scale-invariant) \textit{cosine similarity} between the original superposition signal $x$ and the output of a given algorithm $\widehat{x}$ defined as: 
$$\cos(x,\widehat{x}) = \frac{x^T\widehat{x}}{\|x\|_2\|\widehat{x}\|_2}.$$ 
%In what follows, we first mention our simulation based on synthetic data, and then we present the experimental results on real image data. 
 
\subsection{Synthetic Data}
As discussed above, for successful recovery we require the constituent signals to be sufficiently incoherent. To achieve this, we choose $\Phi$ to be the 1D Haar wavelets basis, and $\Psi$ to be the noiselet basis\footnote{These bases are known to be maximally incoherent relative to each other~\cite{coifman2001noiselets}}. For the measurement operator $A$, we choose a partial DFT matrix. Such matrices are known to have similar recovery performance as random Gaussian matrices, but enable fast numerical operations~\cite{candes2006robust}. Also, we present our experiments based on both non-smooth as well as differentiable link functions. For the non-smooth case, we choose $g(x) = \text{sign}(x)$; here, we only present recovery results using \textsc{OneShot} and \textsc{NlcdLASSO} since in our analysis \textsc{DHT} and \textsc{DST} can only handle smooth link functions. %Next, we provide a comparison between all four algorithms with different differentiable link functions. 

The results of our first experiment are shown in Figure~\ref{figCos}(a) and Figure~\ref{figCos}(b). The test signal is generated as follows: set length $n={2^{20}}$, and generate the vectors $w$ and $z$  by randomly selecting a signal support with $s$ nonzero elements, and populating the nonzero entries with random $\pm 1$ coefficients. The plot illustrates the performance of \textsc{Oneshot} and \textsc{NlcdLASSO} measured by the cosine similarity for different choices of sparsity level $s$, where the nonlinear link function is set to $g(x) = \text{sign(x)}$ \new{and we have used both $\|t\|_1\leq 2\sqrt{s}$ and $\|t\|_1\leq \varrho$ constraints}. The horizontal axis denotes an increasing number of measurements. Each data point in the plot is obtained by conducting a Monte Carlo experiment in which a new random measurement matrix $A$ is generated, recording the cosine similarity between the true signal $x$ and the reconstructed estimate and averaging over $20$ trials. 

\begin{figure}[t]
\begin{center}
\begin{tabular}{ccc}
%\includegraphics[width=0.21\textwidth]{../Figs2/allel1Dic.pdf} & 
%\includegraphics[height=2.1in]{../Figs2/all.pdf} & [trim = 15mm 1mm 32mm 5mm, clip, width=0.43\linewidth]
%\includegraphics[width=0.21\textwidth]{../Figs2/RTs5log.pdf} \\
\includegraphics[trim = 5mm 2mm 5mm 5mm, clip, width=0.32\linewidth]{./allel1Dic.png} & 
\includegraphics[trim = 5mm 2mm 5mm 5mm, clip, width=0.32\linewidth]{./all.png} & 
\includegraphics[trim = 5mm 1mm 5mm 5mm, clip, width=0.32\linewidth]{./RTs5log.png} \\
(a)  & (b)  & (c) 
\end{tabular}
\end{center}
\vskip -.2in\caption{\small\emph{\new{Performance of \textsc{OneShot} and \textsc{NlcdLASSO} according to the \textsc{Cosine Similarity} for different choices of sparsity level $s$ for $g(x) = sign(x)$. (a) \textsc{NlcdLASSO} with $\|t\|_1\leq 2\sqrt{s}$. (b) \textsc{NlcdLASSO} with $\|t\|_1\leq \varrho$. (c) Comparison of running times of \textsc{OneShot} with \textsc{NlcdLASSO}.} }}
%, and $(b)$ $f(x) = \frac{e^x-1}{e^x+1}$ as the nonlinear functions.}}
\label{figCos}
\end{figure}

As we can see, notably, the performance of \textsc{NlcdLASSO} is worse than \textsc{OneShot} for any fixed choice of $m$ and $s$ no matter what upper bound we use on $t$. Even when the number of measurements is high (for example, at $m = 4550$ in plot (b)), we see that \textsc{OneShot} outperforms \textsc{NlcdLASSO} by a significant degree. In this case, \textsc{NlcdLASSO} is at least $70\%$ worse in terms of signal estimation quality, while \textsc{OneShot} recovers the (normalized) signal perfectly. This result indicates the inefficiency of \textsc{NlcdLASSO} for nonlinear demixing.

%Figure \ref{SignalComponent} shows the true signal and its components ($x$, $\Phi w$ and $\Phi z$) as well as the reconstructed signals using \textsc{Oneshot}. In this experiment, we have used a standard normal random matrix as the measurement matrix $A$, $g(x)=sign(x)$ as the nonlinear link function, and a test signal of length $2^{15}$. The sparsity parameter $s$ is set to 15, and the number of measurements is set to $5000$. From visual inspection, we can observe that both true and reconstructed signals as well as true constituent signals and their estimations are close to each other (up to a scaling factor). This ambiguity in the scaling of the recovery components and consequently superposition signal was predictable due to the Theorem~\ref{thm:main} which can be considered as one of the drawbacks of \textsc{Oneshot}.
%
%\begin{figure}[t]
%\centering
%\subfloat[]{\includegraphics[scale=0.24]{../Figs/true1.png}} 
%\subfloat[]{\includegraphics[scale=0.24]{../Figs/oneshot1.png}} 
%\caption{(a) Ground truth $x$, $\Phi w$, and $\Phi z$. (b) \textsc{Oneshot} estimates $\hat{x}$, $\Phi\hat{w}$, and $\Phi\hat{z}$.}
%\label{SignalComponent}
%\end{figure}

Next, we contrast the running time of both algorithms, illustrated in Figure \ref{figCos}(c). In this experiment, we measure the wall-clock running time of the two recovery algorithms (\textsc{OneShot} and \textsc{NlcdLASSO}), by varying signal size $x$ from $n = 2^{10}$ to $n = 2^{20}$.  Here, we set $m = 500$, $s=5$, and the number of Monte Carlo trials to $20$. Also, the nonlinear link function is considered as $g(x)=\text{sign}(x)$. As we can see from the plot, \textsc{OneShot} is at least $6$ times faster than \textsc{NlcdLASSO} when the size of signal equals to $2^{20}$. Overall, \textsc{OneShot} is efficient even for large-scale nonlinear demixing problems.  We mention that in the above setup, the main computational costs incurred in \textsc{OneShot} involve a matrix-vector multiplication followed by a thresholding step, both of which can be performed in time that is \emph{nearly-linear} in terms of the signal length $n$ for certain choices of $A,\Phi,\Psi$
%
%\begin{figure}
%\centering
%\includegraphics[width=0.4\linewidth]{../Figs/RTs5log.eps}
%\caption{\emph{Comparison of running times of \textsc{OneShot} with \textsc{NlcdLASSO}. \textsc{OneShot} is non-iterative, and in contrast with LASSO-based techniques, has a running time that is \emph{nearly-linear} in the signal dimension $n$ for certain structured sparsifying bases.}}
%\label{RunninTime}
%\end{figure}

Next, we turn to differentiable link functions. In this case, we generate the constituent signal coefficient vectors, $w, z$ with $n= 2^{16}$, and compare performance of the four above algorithms. The nonlinear link function is chosen to be $g(x) = 2x + \sin(x)$; it is easy to check that the derivative of this function is strictly bounded between $l_1 = 1$ and $l_2 = 3$. The maximal number of iterations for both \textsc{DHT} and \textsc{DST} is set to to $1000$ with an early stopping criterion if convergence is detected. The step size is hard to estimate in practice, and therefore is chosen by manual tuning such that both \textsc{DHT} and \textsc{DST} obtain the best respective performance.

 Figure~\ref{PT} illustrates the performance of the four algorithms in terms of \emph{phase transition} plots, following~\cite{mccoyTropp2014}. In these plots, we varied both the sparsity level $s$ and the number of measurements $m$. For each pair $(s,m)$, as above we randomly generate the test superposition signal by choosing both the support and coefficients of $x$ at random, as well as the measurement matrix. We repeat this experiment over $20$ Monte Carlo trials.  We calculate the empirical probability of successful recovery as the number of trials in which the output cosine similarity is greater than $0.99$. Pixel intensities in each figure are normalized to lie between 0 and 1, indicating the probability of successful recovery.

%\new{we have not shown phase transition plots for scaling of the algorithm. Since we already claim that DHT can solve scaling ambiguity}

\begin{figure}[t]
\begin{center}
\begin{tabular}{cccc}
\hskip -.2in
\includegraphics[trim = 22mm 1mm 22mm 5mm, clip, width=0.24\linewidth]{HTM.png} &
\includegraphics[trim = 22mm 1mm 22mm 5mm, clip, width=0.24\linewidth]{STM.png} &
% (a) DHT & (b) DST
\includegraphics[trim = 22mm 1mm 22mm 5mm, clip, width=0.24\linewidth]{Oneshot.png} & 
\includegraphics[trim = 22mm 1mm 22mm 5mm, clip, width=0.24\linewidth]{NLCDLASSO.png} \\
 (a) DHT & (b) DST &
 (c) \textsc{OneShot} & (d) \textsc{NlcdLASSO} 
\end{tabular}
\end{center}
\vskip -.2in \caption{\emph{Phase transition plots of various algorithms for solving the demixing problem~\eqref{MainModell} as a function of sparsity level $s$ and number of measurements $m$ with cosine similarity as the criterion. Dimension of the signals $n= 2^{16}$.}}\label{PT}
\end{figure}

As we observe in Fig.~\ref{PT}, \textsc{DHT} has the best performance among the different methods, and in particular, outperforms both the convex-relaxation based methods. The closest algorithm to \textsc{DHT} in terms of the signal recovery is \textsc{DST}, while the \textsc{LASSO}-based method fails to recover the superposition signal $x$ (and consequently the constituent signals $w$ and $z$).  The improvements over \textsc{OneShot} are to be expected since as discussed before, this algorithm does not leverage the knowledge of the link function $g$ and is not iterative. 

In Fig.~\ref{4algProb}, we fix the sparsity level $s=50$ and plot the probability of recovery of different algorithms with a varying number of measurements. The number of Monte Carlo trials is set to 20 and the empirical probability of successful recovery is defined as the number of trials in which the output cosine similarity is greater than $0.95$. The nonlinear link function is set to be $g(x) = 2x + \sin(x)$ for figure (a) and $g(x) = \frac{1}{1+e^{-x}}$ for figure (b). As we can see, \textsc{DHT} has the best performance, while \textsc{NlcdLASSO} for figure (a) and \textsc{Oneshot}, and \textsc{NlcdLASSO} for figure (b) cannot recover the superposition signal even with the maximum number of measurements.
%$m=5000$. We note that in figure (b), we have used \textit{sigmoid} function which is used in logistic regression. This function is not odd and \textsc{Oneshot} is failed in recovery of super position signal and demixing of constituent signals~\cite{SoltaniHegde,plan2014high}. we have not mentioned that \textsc{Oneshot} has problem with non odd function berfore}

\begin{figure}[h]
\begin{center}
\begin{tabular}{cc}
\includegraphics[trim = 5mm 65mm 22mm 60mm, clip, width=0.42\linewidth]{4algProb.pdf}&
\includegraphics[trim = 5mm 65mm 22mm 60mm, clip, width=0.42\linewidth]{4algProbSigmoid.pdf}\\
(a) & (b) 
\end{tabular}
\end{center}
\vskip -.2in\caption{\emph{Probability of recovery for four algorithms; \textsc{DHT}, \textsc{STM}, \textsc{Oneshot}, and \textsc{NlcdLASSO}. Sparsity level is set to $s =50$ and dimension of the signals equal to $n= 2^{16}$. (a) $g(x) = 2x + \sin(x)$, (b) $g(x) = \frac{1}{1+e^{-x}}$.}}
\label{4algProb}
\end{figure}

%As our final experiment with synthetic data, in figure~\ref{PTnorm}, we illustrate the performance of all four algorithms in recovery of the correct scaling of the superposition signal, $x$ with differentiable link function giving by $g(x) = 2x + \sin(x)$. We use the relative error between the original superposition signal $x$ and the output of a given algorithm $\widehat{x}$ defined as $\frac{\|\widehat{x}-x\|_2}{\|x\|_2}$. All the parameters are the same as experiment illustrated in figure~\ref{PT} and we calculate the empirical probability of successful recovery as the number of trials in which the output relative error is less than $0.01$. As we can see in figure~\ref{PTnorm}, \textsc{DHT} can only recover the superposition signal with correct scaling and solve the ambiguity scaling with appropriate values of $m$ and $s$, while all the other algorithms fail to recover the correct scaling factor in superposition signal, $x$.  
%
%\begin{figure}[h]
%\begin{center}
%\begin{tabular}{cccc}
%\includegraphics[trim = 22mm 1mm 35mm 10mm, clip, width=0.21\linewidth]{Figs/HTMnorm.png} &
%\includegraphics[trim = 22mm 1mm 35mm 10mm, clip, width=0.21\linewidth]{Figs/STMnorm.png} &
%% (a) DHT & (b) DST \\
%\includegraphics[trim = 22mm 1mm 35mm 10mm, clip, width=0.21\linewidth]{Figs/Oneshotnorm.png} & 
%\includegraphics[trim = 22mm 1mm 35mm 10mm, clip, width=0.21\linewidth]{Figs/NLCDLASSOnorm.png} \\
% (a) DHT & (b) DST &
% (c) \textsc{OneShot} & (d) \textsc{NlcdLASSO} \\
%\end{tabular}
%\end{center}
%\caption{\emph{Phase transition plots of various algorithms for solving the demixing problem~\eqref{MainModell} as a function of sparsity level $s$ and number of measurements $m$ with relative error as the criterion.}}\label{PTnorm}
%\end{figure}

\subsection{Real Data}

In this section, we provide representative results on real-world 2D image data using \textsc{Oneshot} and \textsc{NlcdLASSO} for non-smooth link function given by $g(x) = \text{sign}(x)$. In addition, we illustrate results for all four algorithms using smooth $g(x) = \frac{1-e^{-x}}{1+e^{-x}}$ as our link function.

We begin with a $256\times 256$ test image. First, we obtain its 2D Haar wavelet decomposition and retain the $s=500$ largest coefficients, denoted by the $s$-sparse vector $w$. Then, we reconstruct the image based on these largest coefficients, denoted by $\widehat{x} = \Phi w$. Similar to the synthetic case, we generate a noise component in our superposition model based on 500 noiselet coefficients $z$. In addition, we consider a parameter which controls the strength of the noiselet component contributing to the superposition model. We set this parameter to 0.1. Therefore, our test image $x$ is given by $x = \Phi w + 0.1\Psi z$.

%\begin{figure}[!h]
%\begin{center}
%\begin{tabular}{cc}
%\includegraphics[height=1.5in]{Figs/X2d.eps} &
%\includegraphics[height=1.5in]{Figs/Phiw2d.eps} \\
%\large$x$ &\large$\Phi w$ \\
%\includegraphics[height=1.5in]{Figs//Xhat2d.eps} &
%\includegraphics[height=1.5in]{Figs/Phiwhat2d.eps} \\
%\large$\widehat{x}$ -- \textsc{Oneshot} &\large$\Phi\widehat{w}$ -- \textsc{Oneshot}  \\
%\includegraphics[height=1.5in]{Figs/XhatL2d.eps} &
%\includegraphics[height=1.5in]{Figs/PhiwhatL2d.eps} \\
%\large$\widehat{x}$ -- \textsc{NlcdLASSO} &\large$\Phi\widehat{w}$ -- \textsc{NlcdLASSO}  \\
%\end{tabular}
%\end{center}
%\caption{Comparison of \textsc{Oneshot} and \textsc{NlcdLASSO} for demixing problem in real 2-dimensional case from nonlinear under-sampled observations. Parameters: $n = 256 \times 256, s = 500, m = 35000, g(x) = sign(x)$.}\label{real2d}
%\end{figure}
\vskip -.55in
\begin{figure}[!h]
\begin{center}
\begin{tabular}{ccc}
\includegraphics[trim = 27mm 83mm 35mm 22mm, clip, width=0.282\linewidth]{X2d.pdf} &
%\large$x$ &\large$\Phi w$ \\
\includegraphics[trim = 27mm 83mm 35mm 22mm, clip, width=0.282\linewidth]{Xhat2d.pdf} &
%\large$\widehat{x}$ -- \textsc{Oneshot} &\large$\Phi\widehat{w}$ -- \textsc{Oneshot}  \\
\hskip .21in\includegraphics[trim = 2mm 0mm 1mm 1.5mm, clip, width=0.228\linewidth]{XhatL2d.pdf} \\
$x$ & $\widehat{x}~(\textsc{OneShot})$ &  $\widehat{x}~(\textsc{NlcdLasso})$  
%\includegraphics[height=1.5in]{Figs/Phiw2d.eps} \\
%\includegraphics[height=1.5in]{Figs/Phiwhat2d.eps} \\
%\includegraphics[height=1.5in]{Figs/PhiwhatL2d.eps} \\
% \large$\widehat{x}$ -- \textsc{NlcdLASSO} &\large$\Phi\widehat{w}$ -- \textsc{NlcdLASSO}  \\
\end{tabular}
\end{center}
\vskip -.2in\caption{\emph{Comparison of \textsc{Oneshot} and \textsc{NlcdLASSO} for real 2D image data from nonlinear under-sampled observations. Parameters: $n = 256 \times 256, s = 500, m = 35000, g(x) = sign(x)$.}}\label{real2d}
\end{figure}

Figure~\ref{real2d} illustrates both the true and the reconstructed images $x$ and $\widehat{x}$ using \textsc{Oneshot} and \textsc{NlcdLASSO}. The number of measurements is set to $35000$ (using subsampled Fourier matrix with $m=35000$ rows). From visual inspection we see that the reconstructed image, $\widehat{x}$, using \textsc{Oneshot} is better than the reconstructed image by \textsc{NlcdLASSO}. Quantitatively, we also calculate Peak signal-to-noise-ratio (PSNR) of the reconstructed images using both algorithms relative to the test image, $x$. We obtain
%\begin{align*}
PSNR of 19.8335 dB using \textsc{OneShot}, and a PSNR of 17.9092 dB using {\textsc{NlcdLASSO}},
%\end{align*
again illustrating the better performance of \textsc{Oneshot} compared to \textsc{NlcdLASSO}.

Next, we show our results using a differentiable link function. For this experiment, we consider an astronomical image illustrated in Fig.~\ref{fig:StarGla}. This image includes two components; the ``stars" component, which can be considered to be sparse in the identity basis ($\Phi$), and the ``galaxy" component which are sparse when they are expressed in the discrete cosine transform basis ($\Psi$). The superposition image $x = \Phi w + \Psi z$ is observed using a subsampled Fourier matrix with $m=15000$ rows multiplied with a diagonal matrix with random $\pm 1$ entries~\cite{krahmer2011new}. Further, each measurement is nonlinearly transformed by applying the (shifted) logistic function $g(x) = \frac{1}{2}\frac{1-e^{-x}}{1+e^{-x}}$ as the link function. In the recovery procedure using DHT, we set the number of iterations to $1000$ and step size $\eta'$ to $150000$. As is visually evident, our proposed DHT method is able to reliably recover the component signals.

\begin{figure}
\begin{center}
\begingroup
\setlength{\tabcolsep}{.1pt} 
\renewcommand{\arraystretch}{.1} 
\begin{tabular}{ccc}      
\includegraphics[trim = 27mm 5mm 35mm 5mm, clip, width=0.27\linewidth]{sT.png}&
\includegraphics[trim = 27mm 5mm 35mm 5mm, clip, width=0.27\linewidth]{phiwHTM.png}&
\includegraphics[trim = 27mm 5mm 35mm 5mm, clip, width=0.27\linewidth]{psizHTM.png}\\
(a) Original {\small$x$} & (b) {\small$\Phi(\widehat{w})$} & {\small$\Psi(\widehat{z})$}
\end{tabular}
\endgroup
\end{center}
\caption{\emph{Demixing a real 2-dimensional image from nonlinear observations with \textsc{DHT}. Parameters: $n = 512 \times 512, s = 1000, m = 15000, g(x) = \frac{1}{2}\frac{1-e^{-x}}{1+e^{-x}}$. Image credits: NASA, \cite{mccoy2014convexity}.}}
\label{fig:StarGla}
\end{figure}