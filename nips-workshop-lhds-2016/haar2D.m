function y = haar2D(x,flg)

% assume that length(x) is a power of 2
n = sqrt(numel(x)); 
levels = log2(n);

[~,lvs] = wavedec2(randn(n),levels,'haar');

switch flg
    case 'forward'
        X = reshape(x,[],n);
        [c,~] = wavedec2(X,levels,'haar');
        y = c(:);
    case 'inverse'
        x = x';
        Y = waverec2(x,lvs,'haar');
        y = Y(:);
end
