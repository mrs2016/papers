\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
% \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2016
%
% to avoid loading the natbib package, add option nonatbib:
% \usepackage[nonatbib]{nips_2016}

%\usepackage{nips_2016}

% to compile a camera-ready version, add the [final] option, e.g.:
\usepackage[final]{nips_2016}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{amsmath,epsfig}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{fullpage}
\usepackage{tikz}
\usepackage{tabu}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{epstopdf}
\usepackage{epsfig}
\usetikzlibrary{tikzmark,calc}
\usepackage{caption}
\usepackage{float}
\usepackage{multirow}
\numberwithin{equation}{section} 
\newtheorem{theorem}{Theorem}[section]                   %[section]   %numberes automatically
\newtheorem{lemma}[theorem]{Lemma}         %[theorem]  in the middle
\newtheorem{corollary}[theorem]{Corollary}     %{corollary}{Corollary}[theorem]   
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{ass}[theorem]{Assumption}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{heuristic}[theorem]{Heuristic}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{exmp}[theorem]{Example}    
\usepackage{microtype}
\usepackage{titlesec}

\titlespacing*{\section}
{0pt}{0\baselineskip}{0\baselineskip}
\setlength{\intextsep}{-3ex}
\setlength{\belowcaptionskip}{-5ex}


\newcommand{\new}[1]{\textcolor{red}{#1}}



%\newcommand\R{\mathbb{R}}

\title{Iterative Thresholding for Demixing \\ Structured Superpositions in High Dimensions}

% The \author macro works with any number of authors. There are two
% commands used to separate the names and addresses of multiple
% authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to
% break the lines. Using \AND forces a line break at that point. So,
% if LaTeX puts 3 of 4 authors names on the first line, and the last
% on the second line, try using \AND instead of \And before the third
% author name.

\author{
  Mohammadreza Soltani \\
  %ECE Department\\
  Iowa State University\\
 % Ames, IA, USA 50010 \\
  %\texttt{msoltani@iastate.edu} \And \texttt{msoltani@iastate.edu} \\
  %% examples of more authors
 \And
 Chinmay Hegde \\
 Iowa State University
  %% \texttt{email} \\
  %% \AND
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
}

\begin{document}

\maketitle

\section{Setup}

The \emph{demixing} problem involves disentangling two (or more) high-dimensional vectors from their linear superposition. In statistical learning applications involving parameter estimation, such superpositions can be used to model situations when there is some ambiguity in the parameters (e.g., the true parameters can be treated as ``ground truth'' + ``outliers'') or when there is some existing prior knowledge that the true parameter vector is a superposition of two components. Mathematically, suppose that the parameter vector is given by $\beta =\Phi\theta_1 + \Psi \theta_2$ where $\beta,\theta_1, \theta_2\in\mathbb{R}^p$ and $\Phi, \Psi$ are orthonormal bases. 
%Now assume that our goal is to fit
%To be more precise, we are interested in fitting the linear model for our observations as follows:
%\begin{align}\label{lindex}
% y = X\beta =X(\Phi w + \Psi z) + e,
% \end{align}
%where $X\in\mathbb{R}^{m\times n}$ denotes the design matrix and $e$ is representing additive error. In other words, our fitting model is give by $f(\widehat{w},\widehat{z}) = \Phi \widehat{w} + \Psi \widehat{z}$ with $\widehat{w}$ and $\widehat{z}$ denoting the estimation of the underlying components $w$ and $z$. Under model~\eqref{lindex}, we are interested in the cases we have limited number of observations where $m \ll n$. In this scenario, the demixing problem~\eqref{lindex} is more challenging due to the fact that $X$ possesses a nontrivial null space and again we have identifiability problem. 
If a linear observation model is assumed, then given samples $y\in\mathbb{R}^n$ and a design matrix $X \in \mathbb{R}^{n \times p}$, the goal is to recover the parameter vector $\beta$ that minimizes a loss function $\mathcal{L}(X,y; \beta)$. We focus on the sample-poor regime where the dimension far exceeds the number of samples; this regime has received significant attention from the machine learning and signal processing communities in recent years~\cite{negahban2009unified,CandesCS}.  

However, fitting the observations according to a linear model can be restrictive. One way to ease this restriction is to assume a \emph{nonlinear} observation model:
\begin{align}\label{nonlindex}
 y = g(X\beta) + e =g(X(\Phi\theta_1 + \Psi \theta_2) )+ e,
 \end{align}
where $g$ denotes a nonlinear \textit{link} function and $e$ denotes observation noise. This is akin to the \emph{Generalized Linear Model} (GLM) commonly used in statistics~\cite{kakade2011}. Here, the problem is to reliably estimate components $w$ and $z$ from the observations $y$ with as few samples as possible.

The above estimation problem is challenging in several different aspects: (i) there is a basic identifiability of issue of obtaining $\theta_1$ and $\theta_2$ even with perfect knowledge of $\beta$; (ii) there is a second identifiability issue arising from the nontrivial null-space of the design matrix (since $n \ll p$); and (iii) the nonlinear nature of $g$, as well as the presence of noise $e$ can further confound recovery.

Standard techniques to overcome each of these challenges are well-known. By and large, these techniques all make some type of \emph{sparseness} assumption on the components $\theta_1$ and $\theta_2$ \cite{CandesCS}; some type of \emph{incoherence} assumption on the bases $\Phi$ and $\Psi$ \cite{elad2005simultaneous,donoho2006stable}; some type of \emph{restricted strong convexity} (RSC) for the design matrix $X$~\cite{negahban2009unified}; and some type of \emph{Lipschitz} assumptions on the link function $g$~\cite{yang2015sparse}. In the longer version of this abstract, we will explain these in greater detail.


\section{Summary of contributions}

In this short paper, we demonstrate an algorithm that stably estimate the components $\theta_1$ and $\theta_2$ under general \emph{structured sparsity} assumptions on these components. Structured sparsity assumptions are useful in applications where the support patterns (i.e., the coordinates of the nonzero entries) belong to certain restricted families (for example, the support is assumed to be \emph{group-sparse} \cite{huang2010benefit}). It is known that such assumptions can significantly reduce the required number of samples for estimating the parameter vectors, compared to generic sparsity assumptions~\cite{baraniuk2010model,SPINIT,approxIT}. 

We note that demixing approaches in high dimensions with structured sparsity assumptions have appeared before in the literature~\cite{mccoyTropp2014,mccoy2014convexity,rao2014forward}. However, our method differs from these earlier works in a few different aspects. The majority of these methods involve solving a convex relaxation problem; in contrast, our algorithm is manifestly \emph{non-convex}. Despite this feature, for certain types of structured superposition models our method provably recovers the components given merely $n = \mathcal{O}(s)$ samples; moreover, our methods achieve a fast (linear) convergence rate, and also exhibits fast (near-linear) per-iteration complexity for certain types of structured models. Moreover, these earlier methods have not explicitly addressed the nonlinear observation model (with the exception of \cite{plan2014high}). We show that under certain smoothness assumptions on $g$, the performance of our method matches (in terms of asymptotics) the best possible sample-complexity.


\begin{figure}
\begin{center}
\begingroup
\setlength{\tabcolsep}{.1pt} % Default value: 6pt
\renewcommand{\arraystretch}{.1} % Default value: 1
\begin{tabular}{cc}      %{c@{\hskip .1pt}c@{\hskip .1pt}c}
\includegraphics[trim = 8mm 58mm 15mm 30mm, clip, width=0.4\linewidth]{../Figs/ProbRecRE.pdf}&
\includegraphics[trim = 8mm 58mm 15mm 30mm, clip, width=0.4\linewidth]{../Figs/ReErr.pdf}\\
(a) & (b) 
\end{tabular}
\endgroup
\end{center}
\caption{\small{\emph{Comparison of \textsc{DHT} with structured sparsity with other algorithms. (a) Probability of recovery in terms of normalized error. (b) Normalized error between $\widehat{\beta} =  \Phi \widehat{\theta_1} + \Psi \widehat{\theta_2}$ and true $\beta$.}}}
\label{fig:ComparisonSyn}
\end{figure}

\section{Details}

We call our demixing algorithm \emph{Structured Demixing with Hard Thresholding}, or \textsc{Struct-DHT} in short. This is an adaptation of the \textsc{DHT} algorithm, proposed in~\cite{soltani2016fast,SoltaniHegde_Asilomar} for demixing components $\theta_1$ and $\theta_2$ that obey generic sparseness assumptions. At a high level, \textsc{DHT} tries to minimize a specific loss function (tailored to $g$) between the observed samples $y$ and the predicted responses $X\Gamma \widehat{t}$, where $\Gamma = [\Phi \  \Psi]$ and $\widehat{t} = [\widehat{\theta}_1; \ \widehat{\theta}_2]$ is the current estimate of the component vectors. The algorithm proceeds by iteratively updating the current estimate of $t$ based on a gradient update rule followed by (myopic) \emph{hard thresholding} of the residual onto the set of $s$-sparse vectors in the span of $\Phi$ and $\Psi$. Here, we consider a version of \textsc{DHT} which is applicable for the case that components $\theta_1$ and $\theta_2$ have block sparsity. For this setting, we replace the hard thresholding step by block-hard thresholding~\cite{baraniuk2010model}. (Analogous approaches can apply for other structured sparsity models.)

Following~\cite{soltani2016fast}, we can prove that in the noiseless scenario to achieve $\varepsilon$-accuracy in estimating the parameter vector $\widehat{t} = [\widehat{\theta}_1; \ \widehat{\theta}_2]$, \textsc{Struct-DHT} only requires $\log\left(\frac{1}{\varepsilon}\right)$ iterations. In addition, if the rows of $X$ are independent subgaussian random vectors~\cite{vershynin2010introduction}, then the required number of samples for successful estimation of the components is given by $\mathcal{O}\left(\frac{s}{b}\log\frac{p}{s}\right)$, where $b$ denotes the block-length, $s$ is the total sparsity, and $p$ represents the ambient dimension. The big-Oh constant hides dependencies on various coherence parameters, including the mutual coherence of $\Gamma$ as well as the cross-coherence between $X$ and $\Gamma$.
If $b = {\Omega}\left(\log\frac{p}{s}\right)$, then the sample complexity of our proposed algorithm is given by $m = \mathcal{O}(s)$, which is asymptotically optimal.


To show the efficacy of \textsc{Struct-DHT} for demixing components with structured sparsity, we numerically compare \textsc{Struct-DHT} with ordinary \textsc{DHT} (which does \emph{not} leverage structured sparsity), and also with an adaptation of a convex formulation described in~\cite{yang2015sparse} that we call \emph{Demixing with Soft Thresholding} (\textsc{DST}). We first generate true components $\theta_1$ and $\theta_2$ with length $p = 2^{16}$ with nonzeros grouped in blocks with length $b = 16$ and total sparsity $s = 656$. The nonzero (active) blocks are randomly chosen from a uniform distribution over all possible blocks. %The design matrix $X$ is implemented as a partial DFT matrix multiplied with a diagonal matrix with random $\pm1$ entries
We construct a design (observation) matrix following the construction of~\cite{krahmer2011new}. Finally, we use a (shifted) sigmoid link function given by $g = \frac{1-e^{-x}}{1 + e^{-x}}$ to generate the observations $y$.  Fig~\ref{fig:ComparisonSyn} shows the the performance of the three algorithms with different number of samples averaged over $10$ Monte Carlo trials. In Fig~\ref{fig:ComparisonSyn}(a), we plot the probability of successful recovery, defined as the fraction of trials where the normalized error is less than 0.05. Fig~\ref{fig:ComparisonSyn}(b) just shows the normalized estimation error for these algorithms. As we can see, \textsc{Struct-DHT} shows much better sample complexity (the required number of samples for obtaining small relative error) as compared to \textsc{DHT} and \textsc{DST}. 

%\subsubsection*{Acknowledgments}
%
%Use unnumbered third level headings for the acknowledgments. All
%acknowledgments go at the end of the paper. Do not include
%acknowledgments in the anonymized submission, only in the final paper.

\bibliographystyle{unsrt}
\bibliography{../Common/chinbiblio,../Common/csbib,../Common/mrsbiblio}

\end{document}