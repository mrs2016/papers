function [xhat,w_hatm,z_hatm,xo,wo,zo,iter,err] = multishotMO(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts)

m = length(y);
th = recopts.th;
eta = recopts.eta;
T = recopts.it;
s1 = recopts.s1;
s2 = recopts.s2;

switch recopts.init
    case 'oneshot'
        [x,w,z] = oneshot(y,A,AT,Phi,PhiT,Psi,PsiT,recopts);
    case 'random'
        kk = recopts.n;
        w = 0*(1:kk)'; z = 0*w;
        suppw = randperm(kk); suppw = suppw(1:s1);
        w(suppw) = rand(s1,1); w = w/norm(w);
        suppz = randperm(kk); suppz = suppz(1:s2);
        z(suppz) = rand(s2,1); z = z/norm(z);
        x = Phi(w) + Psi(z);
        x = x/norm(x);
    case 'zeros'
        kk = recopts.n;
        w = zeros(kk,1);
        z = zeros(kk,1);
        x = zeros(kk,1);
        
end

tt = [w;z];
xo = x;
wo = w;
zo = z;
nn = length(tt);

for iter = 1:T
    tt1 = PhiT(AT((g(A(x))-y)));               %- 0.00001*PhiT(AT(A(x) - y)) ;
    tt2 = PsiT(AT((g(A(x))-y)));               % - 0.00001*PsiT(AT(A(x) - y)) ;
    Gr = [tt1;tt2];
    ttn = tt - (eta/m)*Gr;
    
    %*********************************
    switch recopts.recflag
        case 'individually_sparse'
            w = ttn(1:n/2);
            w = thresh(w,s1);
            z = ttn(n/2+1:end);
            z = thresh(z,s2);
        case 'totally_sparse'
            temp = thresh(ttn,s1+s2);
            w = temp(1:nn/2);
            z = temp(nn/2+1:end);
            tt = temp;
    end
    %**************************************
    
    x = Phi(w) +  Psi(z);
    
    err(iter) = norm(abs(y - g(A(x))), 2)/norm(y);
    fprintf('Iter: %d error %f\n',iter, err(iter));
    if err(iter) <= th
        fprintf('Terminating...\n');
        break;
    end
end

w_hatm = w;
z_hatm = z;
xhat = Phi(w_hatm) + Psi(z_hatm);

end


function xhat = thresh(x,s)
xhat = 0*x;
[~,idx] = sort(abs(x),'descend');
idx = idx(1:s);
xhat(idx) = x(idx);
end
