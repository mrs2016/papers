function [xhat,w_hatm,z_hatm,xo,wo,zo,err_trace] = htdemix(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts)

%%%%%%%%%%%
% [x,w,z] = oneshot(y,A,AT,Phi,PhiT,Psi,PsiT,recopts);
x = zeros(recopts.n,1);
w = zeros(recopts.n,1);
z = zeros(recopts.n,1);

xo = x;
wo = w;
zo = z;


Afun = @(x) Afun_PhiandPsi(x,1,A,AT,Phi,PhiT,Psi,PsiT,recopts);
AfunT = @(x) Afun_PhiandPsi(x,2,A,AT,Phi,PhiT,Psi,PsiT,recopts);

n2 = 2*recopts.n;
err = 1;
t_est = [w;z];  %0*randn(n2,1);
iterCnt = 0;

while ((err>recopts.th)&&(iterCnt<=recopts.it))
    iterCnt = iterCnt + 1;
    err_trace(iterCnt) = err;
    t_prev = t_est;
    resid = y - g(Afun(t_prev));
    proxy = AfunT(resid);
    
%     %------Head approximation
%     switch recopts.recflag
%         case 'totally_sparse'
%             t_est = thresh(tupdate,2*(recopts.s1+recopts.s2));
%             w_hatm = t_est(1:recopts.n);
%             z_hatm = (1/recopts.lambda)*t_est(recopts.n+1:end);
%             xhat = Phi(w_hatm) + Psi(z_hatm);
%         case 'individually_sparse'
%             w_hatm = tupdate(1:recopts.n);
%             z_hatm = (1/recopts.lambda)*tupdate(recopts.n+1:end);
%             w_hatm = thresh(w_hatm,2*recopts.s1);
%             z_hatm = thresh(z_hatm,2*recopts.s2);
%             t_est = [w_hatm; z_hatm];
%             xhat = Phi(w_hatm) + Psi(z_hatm);
%     end
    
    %----NoHead
    t_est = recopts.eta*proxy;
    
    %-------Tail approximation
    tupdate = t_prev + t_est;
    switch recopts.model_Sparsity
        case 'arbit_saprsity'
            switch recopts.recflag
                case 'totally_sparse'
                    t_est = thresh(tupdate,(recopts.s1+recopts.s2));
                    w_hatm = t_est(1:recopts.n);
                    z_hatm = (1/recopts.lambda)*t_est(recopts.n+1:end);
                    xhat = Phi(w_hatm) + Psi(z_hatm);
                case 'individually_sparse'
                    w_hatm = tupdate(1:recopts.n);
                    z_hatm = (1/recopts.lambda)*tupdate(recopts.n+1:end);
                    w_hatm = thresh(w_hatm,recopts.s1);
                    z_hatm = thresh(z_hatm,recopts.s2);
                    t_est = [w_hatm; z_hatm];
                    xhat = Phi(w_hatm) + Psi(z_hatm);
            end
        case 'struc_sparsity'
            w_hatm = tupdate(1:recopts.n);
            z_hatm = (1/recopts.lambda)*tupdate(recopts.n+1:end);
            w_hatm = block_thresh(w_hatm,recopts.numblocks,recopts.bl,recopts.K);
            z_hatm = block_thresh(z_hatm,recopts.numblocks,recopts.bl,recopts.K);
            t_est = [w_hatm; z_hatm];
            xhat = Phi(w_hatm) + Psi(z_hatm);
    end
    
    errTemp = norm(g(Afun(t_est))-y)/norm(y);
    if (recopts.verbose)
        fprintf('Iter: %d Relative err: %f Norm: %f\n',iterCnt,errTemp,norm(t_est(:)));
    end
    
    if (errTemp <= recopts.th) %Early termination condition
        if (recopts.verbose)
            fprintf('Terminating.\n');
        end
        break
    end
    err = errTemp;
    
end


