function [xhat,w_hatm,z_hatm,xo,wo,zo,iter,err] = multishotsoft(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts)

m = length(y);
landa = recopts.landa;
th = recopts.th;
eta = recopts.eta;
T = recopts.it;
s1 = recopts.s;
s2 = recopts.s;

switch recopts.init
    case 'oneshot'
        [x,w,z] = oneshot1D(y,A,AT,Phi,PhiT,Psi,PsiT,recopts);
    case 'random'
        kk = recopts.n;
        w = 0*(1:kk)'; z = 0*w;
        suppw = randperm(kk); suppw = suppw(1:s1);
        w(suppw) = rand(s1,1); w = w/norm(w);
        suppz = randperm(kk); suppz = suppz(1:s2);
        z(suppz) = rand(s2,1); z = z/norm(z);
        x = Phi(w) + Psi(z);
        x = x/norm(x);
    case 'zeros'
        kk = recopts.n;
        w = zeros(kk,1);
        z = zeros(kk,1);
        x = zeros(kk,1);
end

tt = [w;z];
xo = x;
wo = w;
zo = z;
nn = length(tt);
la = landa*eta;

for iter = 1:T
    tt1 = PhiT(AT((g(A(x))-y)));
    tt2 = PsiT(AT((g(A(x))-y)));
    Gr = [tt1;tt2];
    ttn = tt - (eta)*Gr;
    
    temp = S_f(ttn,la);
    w = temp(1:nn/2);
    z = temp(nn/2+1:end);
    tt = temp;
    
    x = Phi(w) + Psi(z);
    err(iter) = norm(abs(y - g(A(x))), 2)/norm(y);
    if (recopts.verbose)
        fprintf('Iter: %d error %f\n',iter, err(iter));
    end
    if err(iter) <= th
        if (recopts.verbose)
            fprintf('Terminating.\n');
        end
        break;
    end
end

w_hatm = w;
z_hatm = z;

xhat = Phi(w_hatm) + Psi(z_hatm);

end
