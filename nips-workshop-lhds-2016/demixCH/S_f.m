function y = S_f(x,landa)
for i = 1:length(x)
    if x(i) > landa
        y(i) = x(i) - landa;
    elseif (x(i) >= -landa && x(i) <= landa)
        y(i) = 0;
    elseif x(i) < -landa
        y(i) = x(i) + landa;
    end
end
y = y';