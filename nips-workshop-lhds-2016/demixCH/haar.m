function y = haar(x,flg)

n = length(x); 
levels = log2(n);

[~,lvs] = wavedec(randn(n,1),levels,'haar');

switch flg
    case 'forward'
        [c,~] = wavedec(x,levels,'haar');
        y = c(:);
    case 'inverse'
        x = x';
        y = waverec(x,lvs,'haar');
        y = y(:);
end
