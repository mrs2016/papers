%%%%% main script to test demixing algorithms (Modified))
%************ Mohammadreza Soltani ***************** June 3, 2016  ********
clear;
close all
clc
%%%%%%%%%%%%%%%%%%%%%%

% parameters
E = 1;    %Monte-Carlo 20
n = 2^16;  %ambient diemnsions
svec = 500;%[5 15 25:25:500];  %sparsity
mvec = [50 500:3000:5000];  %measurements
opts.basis = 'haar_noiselet';  %bases for constituent signal
opts.link = 'sin';       %nonlinear function
opts.meas = 'fourier';    % 'gauss'  measurement matrix
opts.recovery = 'HTM';   % oneshot nlcdlasso STM HTM all


% link function
switch opts.link
    case 'sign'
        g = @(x) sign(x);
    case 'sigmoid'
        g = @(x) 1./(1 + exp(-x));
    case 'lin'
        g = @(x) x;
    case 'sin'
        g = @(x) 2*x + sin(x);
end

Cos_sim_oneshot = zeros(length(svec),length(mvec),E);
Cos_sim_nlcdLasso = zeros(length(svec),length(mvec),E);
Cos_sim_HTM = zeros(length(svec),length(mvec),E);
Cos_sim_STM = zeros(length(svec),length(mvec),E);

for ee=1:E
    disp(ee)
    for ss = 1:length(svec)
        
        % generate signal
        w = 0*(1:n)'; z = 0*w;
        suppw = randperm(n); suppw = suppw(1:svec(ss));
        w(suppw) = ones(svec(ss),1); w = w/norm(w);
        suppz = randperm(n); suppz = suppz(1:svec(ss));
        z(suppz) = ones(svec(ss),1); z = z/norm(z);
        
        % basis operators
        switch opts.basis
            case 'id_dct'
                Phi = @(w) w; PhiT = @(w) w;
                Psi = @(z) dct(z); PsiT = @(z) idct(z);
            case 'haar_noiselet'
                Phi = @(w) haar(w,'forward');
                PhiT = @(w) haar(w, 'inverse');
                Psi = @(z) 1/sqrt(length(z))*realnoiselet(z);
                PsiT = @(z) 1/sqrt(length(z))*realnoiselet(z);
        end
        
        x = Phi(w) + Psi(z);
        x = x/norm(x);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        for mm=1:length(mvec)
            
            % measurement operator
            switch opts.meas
                case 'gauss'
                    Amat = randn(mvec(mm),n);
                    A = @(x) Amat*x; AT = @(x) Amat'*x;
                case 'fourier'
                    p = (1:n)';
                    q = randperm(n/2-1)+1; % this keeps a random set of FFT coefficients
                    omega = q(1:mvec(mm)/2)';
                    A = @(z) A_f(z, omega, p);
                    AT = @(z) At_f(z, n, omega, p);
                    
                    
%                     
%                     zeta = sign(randn(n,1));
%                     p = randperm(n)';
%                     q = randperm(n/2-1)+1; % this keeps a random set of FFT coefficients
%                     omega = q(1:mvec(mm)/2)';
%                     A = @(z) A_f(zeta.*z, omega, p);
%                     AT = @(z) zeta.*At_f(z, n, omega, p);
                    
            end
            
            %%%% measurements
            nvarb = 0;   %for noise before g
            nvara = 0; %for noise after g
            y = g(A(x) + nvarb*randn(mvec(mm),1)) + nvara*randn(mvec(mm),1);
            %%%% Recovery algorithm
            switch opts.recovery
                case             'oneshot'
                    recopts.s = svec(ss);
                    recopts.m = mvec(mm);
                    [xhatOne,w_hatOne,z_hatOne] = oneshot1D(y,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    Cos_sim_oneshot(ss,mm,ee) = xhatOne'*x/(norm(x)*(norm(xhatOne)));
                case            'HTM'
                    recopts.s = svec(ss);
                    recopts.m = mvec(mm);
                    recopts.n = n;
                    recopts.eta = 100;           %Step Size
                    recopts.th = 0.001;           %for faster terminating
                    recopts.init = 'oneshot';  % 'zeros', 'random'  'oneshot'
                    recopts.recflag = 'totally_sparse';  %individually_sparse
                    recopts.it = 500;       %100:200:700;  %iteration for multishot algorithm
%                     [xhatHT,w_hatHT,z_hatHT,xone,wone,zone,iter,err] = multishotMO(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    [xhat,w_hatm,z_hatm,xo,wo,zo,err_trace] = htdemix(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    Cos_sim_HTM(ss,mm,ee) = xhatHT'*x/(norm(x)*(norm(xhatHT)));
                    
                case 'nlcdlasso'
                    recopts.s = svec(ss);
                    recopts.n = n;
                    [xhatNL,w_hatNL,z_hatNL] = nlcdLasso(y,AT,Phi,PhiT,Psi,PsiT,recopts);
                    Cos_sim_nlcdLasso(ss,mm,ee) = xhatNL'*x/(norm(x)*(norm(xhatNL)));
                    
                case 'STM'
                    recopts.s = svec(ss);
                    recopts.m = mvec(mm);
                    recopts.n = n;
                    recopts.landa = 1e-4;      %tuning parameter
                    recopts.eta = .1;           %Step Size
                    recopts.th = 0.001;           %for faster terminating
                    recopts.init = 'oneshot';  % 'zeros', 'random'   'oneshot'
                    recopts.it = 1000;       %100:200:700;  %iteration for multishot algorithm
                    [xhatST,w_hatST,z_hatST,xoo,woo,zoo,iter,err] = multishotsoft(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    Cos_sim_STM(ss,mm,ee) = xhatST'*x/(norm(x)*(norm(xhatST)));
                    
                case 'all'
                    recopts.s = svec(ss);         %sparsity
                    recopts.m = mvec(mm);         %measurements
                    recopts.n = n;
                    recopts.landa = 1e-4;      %tuning parameter
                    recopts.eta = 100;           %Step Size
                    recopts.th = 0.001;           %for faster terminating
                    recopts.init = 'oneshot';  % 'zeros', 'random'
                    recopts.recflag = 'totally_sparse';  %individually_sparse
                    recopts.it = 500;       %100:200:700;  %iteration for multishot algorithm
                    
                    [xhatHT,w_hatHT,z_hatHT,xhatOne,w_hatOne,z_hatOne,iterHT,errHT] = multishotMO(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    
                    recopts.eta = 1e-4;           %Step Size
                    recopts.it = 100;       %100:200:700;  %iteration for multishot algorithm
                    [xhatST,w_hatST,z_hatST,xoo,woo,zoo,iterST,errST]  = multishotsoft(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    
                    [xhatNL,w_hatNL,z_hatNL] = nlcdLasso(y,AT,Phi,PhiT,Psi,PsiT,recopts);
                    
                    Cos_sim_HTM(ss,mm,ee) = xhatHT'*x/(norm(x)*(norm(xhatHT)));
                    Cos_sim_STM(ss,mm,ee) = xhatST'*x/(norm(x)*(norm(xhatST)));
                    Cos_sim_nlcdLasso(ss,mm,ee) = xhatNL'*x/(norm(x)*(norm(xhatNL)));
                    Cos_sim_oneshot(ss,mm,ee) = xhatOne'*x/(norm(x)*(norm(xhatOne)));
                    
            end
            
        end
    end
end


switch opts.recovery
    case 'HTM'
%         figure, plot(mvec,Cos_sim_HTM(1,:,1),'LineWidth',2);
            case 'STM'
                figure, plot(mvec,Cos_sim_STM(1,:,1),'LineWidth',2);
        %     case 'nlcdlasso'
        %         figure, plot(mvec,Cos_sim_nlcdLasso(1,:,1),'LineWidth',2);
        %     case 'oneshot'
        %         figure, plot(mvec,Cos_sim_oneshot(1,:,1),'LineWidth',2);
        %     case 'all'
        %         figure,
        %         plot(mvec,Cos_sim_HTM(1,:,1),'LineWidth',2);
        %         hold on
        %         plot(mvec,Cos_sim_STM(1,:,1),'r','LineWidth',2);
        %         hold on
        %         plot(mvec,Cos_sim_nlcdLasso(1,:,1),'g','LineWidth',2);
        %         hold on
        %         plot(mvec,Cos_sim_oneshot(1,:,1),'k','LineWidth',2);
end
