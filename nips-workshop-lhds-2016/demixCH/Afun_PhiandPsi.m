function y = Afun_PhiandPsi(t,mode,A,AT,Phi,PhiT,Psi,PsiT,recopts)

switch mode
    case 1
        n = recopts.n;
        x1 = t(1:n);
        x2 = t(n+1:end);
        x = Phi(x1) + (1/recopts.lambda)*Psi(x2);
        y = A(x);
    case 2
        y = [PhiT(AT(t)); (1/recopts.lambda)*PsiT(AT(t))];
end

end
