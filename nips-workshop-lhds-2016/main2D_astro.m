%%%%% 2D nonlinear demixing algorithms
% CH< May 2016
% specific to SQUARE astronomical images; need to modify for other sizes

clear
close all
clc
addpath /Users/msoltani/Documents/MATLAB/DemixingVector/Utils


opts.basis = 'id_dct';
opts.link = 'logit';
opts.meas = 'fourier';
opts.image_type = 'real_data';
s1 = 1000;
s2 = s1;
E = 1;
mvec = 15000;

% basis operators
switch opts.basis
    case 'id_dct'
        Phi = @(w) w(:);
        PhiT = @(w) w(:);
        Psi = @(z) myvec(idct2(mymat(z)));
        PsiT = @(z) myvec(dct2(mymat(z)));
end

% link function
switch opts.link
    case 'sign'
        g = @(x) sign(x);
    case 'logit'
        g = @(x) 1./(1 + exp(-x))-.5;
    case 'lin'
        g = @(x) 2*x + sin(x);
end

switch opts.image_type
    case 'real_data'
        load astro_image
        n = numel(im_superposition);
        comp1_coeff = PhiT(im_sparse(:));
        comp1_coeff = thresh(comp1_coeff,s1);
        comp2_coeff = PsiT(im_smooth(:));
        comp2_coeff = thresh(comp2_coeff,s2);
end

x = normc(Phi(comp1_coeff) + Psi(comp2_coeff));

%keyboard;

for mm=1:length(mvec)
    
    m = 2*round(mvec(mm)/2);
    % measurement operator
    switch opts.meas
        case 'full'
            A = @(x) x;
            AT = @(x) x;
        case 'gauss'
            Amat = randn(m,n);
            A = @(x) Amat*x;
            AT = @(x) Amat'*x;
        case 'fourier'
            zeta = sign(randn(n,1));
            p = randperm(n)';
            q = randperm(n/2-1)+1; % this keeps a random set of FFT coefficients
            omega = q(1:m/2)';
            A = @(z) A_f(zeta.*z, omega, p);
            AT = @(z) zeta.*At_f(z, n, omega, p);
    end
    
    %%%% measurements
    y = g(A(x));
    
    %%%% recovery
    recopts.s1 = s1;
    recopts.s2 = s2;
    recopts.m = m;
    recopts.th = 0.001;
    recopts.it = 1500;
    recopts.init = 'oneshot';  % 'zeros', 'random'
    recopts.n = n;
    
    recopts.rec = 'iht';
    recopts.lambda = 1;
    
    switch recopts.rec
        case 'multishot'
            recopts.recflag = 'totally_sparse'; recopts.eta = 150000;
            %recopts.recflag = 'individually_sparse'; recopts.eta = 5;
            [xhat,w_hatm,z_hatm,xo,wo,zo,iter,err] = multishotMO(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
        case 'lasso'
            [xhat,w_hatm,z_hatm] = lassodemix(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
        case 'iht'
            recopts.recflag = 'totally_sparse'; recopts.eta = 10;
            %recopts.recflag = 'individually_sparse'; recopts.eta = 5;
            recopts.verbose = 1;
            [xhat,w_hatm,z_hatm,xo,wo,zo,err_trace] = htdemix(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
    end
    
end

%figure, plot(mvec,mean(sim_multishot,1))
% X = reshape(x,[],sqrt(n));
% Xhat = reshape(xhat,[],sqrt(n));
% w_hat = reshape(Phi(w_hatm),[],sqrt(n));
% z_hat = reshape(Psi(z_hatm),[],sqrt(n));
% 
% figure(2), clf
% subplot(2,2,1), imshow(X,[])
% subplot(2,2,2), imshow(Xhat,[])
% subplot(2,2,3), imshow(w_hat,[])
% subplot(2,2,4), imshow(z_hat,[])
% set(gcf, 'position', [500, 500, 1000, 1000])
plotimgesSep
