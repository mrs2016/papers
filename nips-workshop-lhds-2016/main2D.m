%%%%% main script to test demixing algorithms

%%%%%%%%%%%%%%%%%%%%%%
% generate data

clear
close all
clc
addpath ../Utils/

% parameters
E = 1;
n = 2^16; % power of 2
s1 = 500;
s2 = 50;
mvec = 10000:5000:15000;
opts.basis = 'haar_noiselet';
opts.link = 'logit';
opts.meas = 'fourier';
opts.recovery = 'oneshot';
opts.image_type = 'real';
opts.noiselet_strength = 0.01;

% basis operators
switch opts.basis
    case 'id_dct'
        Phi = @(w) w; PhiT = @(w) w;
        Psi = @(z) dct(z); PsiT = @(z) idct(z);
    case 'haar_noiselet'
        Phi =  @(w) haar2D(w, 'inverse');
        PhiT = @(w) haar2D(w, 'forward');
        Psi = @(z) 1/sqrt(length(z))*realnoiselet(z);
        PsiT = @(z) 1/sqrt(length(z))*realnoiselet(z);
end


% link function
switch opts.link
    case 'sign'
        g = @(x) sign(x);
    case 'logit'
        g = @(x) 1./(1 + exp(-x)) - 0.5;
    case 'lin'
        g = @(x) x;
end

switch opts.image_type
    case 'synthetic'
        w = 0*(1:n)';
        suppw = randperm(n); suppw = suppw(1:s1);
        w(suppw) = randn(s1,1); w = w/norm(w);
    case 'real'
        Im = imread('mandrill-grayscale.jpg');
        Im = imresize(Im,[sqrt(n) sqrt(n)],'bicubic');
        Im = im2double(rgb2gray(Im));
        wavelet_coeff = PhiT(Im(:));
        w = thresh(wavelet_coeff,s1);
        w = w/norm(w);
end

z = 0*w;
suppz = randperm(n); suppz = suppz(1:s2);
z(suppz) = randn(s2,1); z = z/norm(z);

x = Phi(w) + (opts.noiselet_strength)*Psi(z);
x = x/norm(x);

% test algorithms
sim_oneshot = zeros(E,length(mvec));
sim_multishot = zeros(E,length(mvec));


%supperr = 0*sim;
for ee=1:E
    disp(ee)
    for mm=1:length(mvec)
        
        % measurement operator
        switch opts.meas
            case 'gauss'
                Amat = randn(mvec(mm),n);
                A = @(x) Amat*x; 
                AT = @(x) Amat'*x;
            case 'fourier'
                zeta = sign(randn(n,1));
                p = randperm(n)';
                q = randperm(n/2-1)+1; % this keeps a random set of FFT coefficients
                omega = q(1:mvec(mm)/2)';
                A = @(z) A_f(zeta.*z, omega, p);
                AT = @(z) zeta.*At_f(z, n, omega, p);
        end
        
        %%%% measurements
        nvar = 0;
        y = g(A(x) + nvar*randn(mvec(mm),1));
        
        %%%% recover signal using oneshot
        recopts.s1 = s1;
        recopts.s2 = s2;
        recopts.alpha = opts.noiselet_strength;
        recopts.m = mvec(mm);
        recopts.eta = 5000;
        recopts.th = 1e-3;
        recopts.it = 200;
        recopts.init = 'random';
        recopts.n = n;
        %[xhat,w_hat,z_hat] = oneshot(y,A,AT,Phi,PhiT,Psi,PsiT,recopts);
        
        [xhat,w_hatm,z_hatm,xo,wo,zo,iter,err] = multishot(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
        
        %%% record estimation error
        sim_oneshot(ee,mm) = xo'*x/(norm(x)*(norm(xo)));
        sim_multishot(ee,mm) = xhat'*x/(norm(x)*(norm(xhat)));
        %supperr(ee,mm) = numel(setdiff(find(w_hat),suppw));
        
        
        
        
    end
end

figure, plot(mvec,mean(sim_multishot,1))

X = reshape(x,[],sqrt(n));
Xhat = reshape(xhat,[],sqrt(n));
w_hat = reshape(Phi(w_hatm),[],sqrt(n));



figure 
subplot(1,3,1), imshow(X,[])
subplot(1,3,2), imshow(Xhat,[])
subplot(1,3,3), imshow(w_hat,[])
