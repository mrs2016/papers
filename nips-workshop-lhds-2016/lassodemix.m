function [xhat,w_hatm,z_hatm] = lassodemix(y,~,A,AT,Phi,PhiT,Psi,PsiT,recopts)

addpath ../Utils/spgl1-1.9/

Afun = @(x,mode) Afun_PhiandPsi(x,mode,A,AT,Phi,PhiT,Psi,PsiT,recopts);
opts_spg = spgSetParms('optTol',1e-5,'iterations',recopts.it,'verbosity',1);
t_hatspg = spg_bp(Afun,y,opts_spg);
w_hatm = t_hatspg(1:recopts.n);
z_hatm = (1/recopts.lambda)*t_hatspg(recopts.n+1:end);

xhat = Phi(w_hatm) + Psi(z_hatm);

end

