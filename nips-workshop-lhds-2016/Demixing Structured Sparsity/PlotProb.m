clc;close all;
clear;
load 'matlab41me.mat';
RE_th = 0.05;
CS_th = 0.99;
[u,v,w] = size(Cos_sim_DHT);

Succ_Norm_err_DHT = zeros(1,v);
Succ_Norm_err_DHT_block_spa = zeros(1,v);
Succ_Norm_err_DST = zeros(1,v);


Succ_Cos_sim_DHT = zeros(1,v);
Succ_Cos_sim_DHT_block_spa = zeros(1,v);
Succ_Cos_sim_DST = zeros(1,v);


for m = 1:v
    for e = 1:w
        if Norm_err_DHT(1,m,e) <= RE_th
            Succ_Norm_err_DHT(1,m) =  Succ_Norm_err_DHT(1,m) + 1;
        end
        if Norm_err_DHT_block_spa(1,m,e) <= RE_th
            Succ_Norm_err_DHT_block_spa(1,m) =  Succ_Norm_err_DHT_block_spa(1,m) + 1;
        end
        if Norm_err_DST(1,m,e) <= RE_th
            Succ_Norm_err_DST(1,m) =  Succ_Norm_err_DST(1,m) + 1;
        end
        
        
        
        
        if Cos_sim_DHT(1,m,e) >= CS_th
            Succ_Cos_sim_DHT(1,m) =  Succ_Cos_sim_DHT(1,m) + 1;
        end
        if Cos_sim_DHT_block_spa(1,m,e) >= CS_th
            Succ_Cos_sim_DHT_block_spa(1,m) =  Succ_Cos_sim_DHT_block_spa(1,m) + 1;
        end
        if Cos_sim_DST(1,m,e) >= CS_th
            Succ_Cos_sim_DST(1,m) =  Succ_Cos_sim_DST(1,m) + 1;
        end
        

    end
    
end

Succ_Norm_err_DHT = Succ_Norm_err_DHT/w;
Succ_Norm_err_DHT_block_spa = Succ_Norm_err_DHT_block_spa/w;
Succ_Norm_err_DST = Succ_Norm_err_DST/w;


Succ_Cos_sim_DHT = Succ_Cos_sim_DHT/w;
Succ_Cos_sim_DHT_block_spa = Succ_Cos_sim_DHT_block_spa/w;
Succ_Cos_sim_DST = Succ_Cos_sim_DST/w;


figure;
plot(mvec,Succ_Norm_err_DHT_block_spa,'-bs','LineWidth',2);
hold on
plot(mvec,Succ_Norm_err_DHT,'--bo','LineWidth',2);
plot(mvec,Succ_Norm_err_DST,':b^','LineWidth',2);
xlim([2000 12000])
axisfortex('','Number of measurements, m','Probability of recovery');
grid on
legend('DHT with structured sparsity','DHT with arbitrary sparsity', 'DST with arbitrary sparsity','location','southeast');

% print(gcf,'-r300','-dpdf','ProbRecRE.pdf');
% movefile('ProbRecRE.pdf','~/Documents/Figs');


figure,
plot(mvec,Succ_Cos_sim_DHT_block_spa,'-rs','LineWidth',2);
hold on
plot(mvec,Succ_Cos_sim_DHT,'--ro','LineWidth',2);
plot(mvec,Succ_Cos_sim_DST,':r^','LineWidth',2);
xlim([2000 12000])
axisfortex('','Number of measurements, m','Probability of recovery');
grid on
legend('DHT with structured sparsity','DHT with arbitrary sparsity', 'DST with arbitrary sparsity','location','southeast');


% set(h_legend,'FontSize',16);

% print(gcf,'-r300','-dpdf','ProbRecCoS.pdf');
% movefile('ProbRecCoS.pdf','~/Documents/Figs');

% figure;
% plot(kvec,Succ_DMF_CS_1,'-bo','LineWidth',2);
% hold on
% plot(kvec,Succ_DMF_CS_15,'--bs','LineWidth',2);
% plot(kvec,Succ_DMF_CS_30,':b^','LineWidth',2);
% plot(kvec,Succ_GHT_CS_1,'-ro','LineWidth',2);
% plot(kvec,Succ_GHT_CS_15,'--rs','LineWidth',2);
% plot(kvec,Succ_GHT_CS_30,':r^','LineWidth',2);
% xlim([1 8])
% axisfortex('Sparsity level = 100, q = 700','Number of diagonal blocks k = m/q','Probability of success (Cosine Similarity)');
% grid on
% h_legend = legend('DMF, ||x|| = 1','DMF, ||x|| = 15','DMF, ||x|| = 30', 'GHT, ||x|| = 1','GHT, ||x|| = 15','GHT, ||x|| = 30' );
