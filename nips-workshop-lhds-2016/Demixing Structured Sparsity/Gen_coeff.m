function  x = Gen_coeff(b,num_blocks,K)     % suppx = Gen_Supp(n,b,svec,g_ba)

% suppx = [];
% nb = svec/b;   % Number of the blocks
% sp = zeros(1,nb);
% for i = 1:nb
%     sp(i) = randi(n);
%     if (i > 1)
%         while (abs(sp(i) - sp(i-1)) < g_ba)
%             sp(i) = randi(n);
%         end
%     end
%     suppx = [sp(i):sp(i) + b-1 suppx];
% end


v = randperm(num_blocks);
hi_col = v(1:K);
smat = zeros(b, num_blocks);
suppx = smat(:,hi_col);
smat_hi = randn(size(suppx));
smat(:,hi_col) = smat_hi;
x = smat(:);