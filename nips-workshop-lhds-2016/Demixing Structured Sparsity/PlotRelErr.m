clc;close all;
clear;
load('matlab41me.mat')
figure;
plot(mvec,mean(Norm_err_DHT_block_spa,3),'-bs','LineWidth',2);
hold on
plot(mvec,mean(Norm_err_DHT,3),'--ro','LineWidth',2);
plot(mvec,mean(Norm_err_DST,3),':gs','LineWidth',2);
xlim([2000 12000])
axisfortex('','Number of measurements, m','Relative Error');
grid on
legend('DHT with structured sparsity','DHT with arbitrary sparsity', 'DST with arbitrary sparsity','location','northeast');

% print(gcf,'-r300','-dpdf','ReErr.pdf');
% movefile('ReErr.pdf','~/Documents/Figs');