%%%%% main script to test demixing of synthetic signals with structured saprsity
%************ Mohammadreza Soltani ***************** Oct 2nd, 2016  ********
clear;
close all
clc
addpath /Users/msoltani/Documents/MATLAB/DemixingVector/Utils
addpath /Users/msoltani/Documents/MATLAB/DemixingVector/demixCH
%%%%%%%%%%%%%%%%%%%%%%

% parameters
E = 10;    %Monte-Carlo
n = 2^16;  %ambient diemnsions
b = 2^4;    %Length of Blocks
num_blocks = n/b;  %Number of Blocks
K = round(0.01*num_blocks); % number of active blocks
svec = K*b; %Total sparsity
% g_ba = 50;  %Gourd band
mvec = 2000:250:12000;           % [800:100:1000];%50:500:5050];  %measurements
opts.basis = 'haar_noiselet';  %bases for constituent signal
opts.link = 'sigmoid';       %nonlinear function
opts.meas = 'fourier';    % 'gauss'  measurement matrix
opts.recovery = 'all';   % oneshot nlcdlasso DST HTM(htdemix) all


% link function
switch opts.link
    case 'sign'
        g = @(x) abs(x+1) - abs(x-1) + 1; %sign(x);
    case 'sigmoid'
        g = @(x) 1./(1 + exp(-x))-.5;
    case 'lin'
        g = @(x) x;
    case 'sin'
        g = @(x) sin(x);
end

% Cos_sim_oneshot = zeros(length(svec),length(mvec),E);
% Cos_sim_nlcdLasso = zeros(length(svec),length(mvec),E);
Cos_sim_DHT = zeros(length(svec),length(mvec),E);
Cos_sim_DHT_block_spa = zeros(length(svec),length(mvec),E);

% Cos_sim_HTMinv = zeros(length(svec),length(mvec),E);
% Cos_sim_HTMCOS = zeros(length(svec),length(mvec),E);
Cos_sim_DST = zeros(length(svec),length(mvec),E);

Norm_err_DHT = zeros(length(svec),length(mvec),E);
Norm_err_DHT_block_spa = zeros(length(svec),length(mvec),E);
% Norm_err_HTMinv = zeros(length(svec),length(mvec),E);
Norm_err_DST = zeros(length(svec),length(mvec),E);
% Norm_err_nlcdLasso = zeros(length(svec),length(mvec),E);
% Norm_err_oneshot = zeros(length(svec),length(mvec),E);

for ee=1:E
    disp(ee)
    for ss = 1:length(svec)
        
        % generate signal
        w = Gen_coeff(b,num_blocks,K);
        z = Gen_coeff(b,num_blocks,K);
        
        % basis operators
        switch opts.basis
            case 'id_dct'
                Phi = @(w) w; PhiT = @(w) w;
                Psi = @(z) dct(z); PsiT = @(z) idct(z);
            case 'haar_noiselet'
                Phi = @(w) haar(w,'forward');
                PhiT = @(w) haar(w, 'inverse');
                Psi = @(z) 1/sqrt(length(z))*realnoiselet(z);
                PsiT = @(z) 1/sqrt(length(z))*realnoiselet(z);
        end
        
        x = Phi(w) + Psi(z);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        for mm=1:length(mvec)
            
            % measurement operator
            switch opts.meas
                case 'gauss'
                    Amat = randn(mvec(mm),n);
                    A = @(x) Amat*x; AT = @(x) Amat'*x;
                case 'fourier'
                    zeta = sign(randn(n,1));
                    p = randperm(n)';
                    q = randperm(n/2-1)+1; % this keeps a random set of FFT coefficients
                    omega = q(1:mvec(mm)/2)';
                    A = @(z) A_f(zeta.*z, omega, p);
                    AT = @(z) zeta.*At_f(z, n, omega, p);
                    
            end
            
            %%%% measurements
            nvarb = 0;   %for noise before g
            nvara = 0; %for noise after g
            y = g(A(x) + nvarb*randn(mvec(mm),1)) + nvara*randn(mvec(mm),1);
            
            %             u = cos(A(x) + nvarb*randn(mvec(mm),1)) + nvara*randn(mvec(mm),1);
            %%%% Recovery algorithm
            switch opts.recovery
                case             'oneshot'
                    recopts.s = svec(ss);
                    recopts.m = mvec(mm);
                    [xhatOne,w_hatOne,z_hatOne] = oneshot1D(y,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    Cos_sim_oneshot(ss,mm,ee) = xhatOne'*x/(norm(x)*(norm(xhatOne)));
                    Norm_err_oneshot(ss,mm,ee) = norm(xhatOne-x)/norm(x);
                    
                case            'DHT'
                    recopts.s1 = svec(ss);
                    recopts.s2 = svec(ss);
                    recopts.bl = b;
                    recopts.numblocks = num_blocks;
                    recopts.K = K;
                    recopts.m = mvec(mm);
                    recopts.n = n;
                    recopts.eta = 15;           %Step Size  0.1 ch   100 mrs
                    recopts.th = 0.001;           %for faster terminating
                    recopts.init = 'oneshot';  % 'zeros', 'random'  'oneshot'
                    recopts.model_Sparsity = 'arbit_saprsity'; %'arbit_saprsity' 'struc_sparsity'
                    recopts.recflag = 'totally_sparse';  %individually_sparse
                    recopts.it = 800;       %100:200:700;  %iteration for multishot algorithm
                    recopts.lambda = 1;
                    recopts.verbose = 1;
                    
                    %                     [xhatHT,w_hatHT,z_hatHT,xhatOne,w_hatOne,z_hatOne,iter,err] = multishotMO(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    [xhatHT,w_hatm,z_hatm,xhatOne,w_hatOne,z_hatOne,err_trace] = htdemix(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    Cos_sim_DHT(ss,mm,ee) = xhatHT'*x/(norm(x)*(norm(xhatHT)));
                    
                    %                     [xhatHTCOS,w_hatmCOS,z_hatmCOS,xhatOneCOS,w_hatOneCOS,z_hatOneCOS,err_traceCOS] = htdemixCOS(u,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    %                     Cos_sim_HTMCOS(ss,mm,ee) = xhatHTCOS'*x/(norm(x)*(norm(xhatHTCOS)));
                    %                     if Cos_sim_HTMCOS(ss,mm,ee) >=0
                    %                         Cos_sim_HTM(ss,mm,ee) = xhatHT'*x/(norm(x)*(norm(xhatHT)));
                    %                     else
                    %                         Cos_sim_HTM(ss,mm,ee) = -xhatHT'*x/(norm(x)*(norm(xhatHT)));
                    %                     end
                    Norm_err_DHT(ss,mm,ee) = norm(xhatHT-x)/norm(x);
                    %***********************************************************************
                    %                     y = log(y+.5)-log(.5-y); %inverse of shiftted sigmoid (odd)
                    %                     %                     y = 0.5*(y-3); %inverse of affine function
                    % %                     y = binaryserchsol(y,min(y),max(y),0.0001);
                    %                     recopts.eta = 1;
                    %                     [xhatHTinv,w_hatminv,z_hatminv,xoinv,w_oinv,z_oneinv,err_traceinv] = htdemixinv(y,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    %                     Cos_sim_HTMinv(ss,mm,ee) = xhatHTinv'*x/(norm(x)*(norm(xhatHTinv)));
                    %                     Norm_err_HTMinv(ss,mm,ee) = norm(xhatHTinv-x)/norm(x);
                    
                case 'nlcdlasso'
                    recopts.s = svec(ss);
                    recopts.n = n;
                    [xhatNL,w_hatNL,z_hatNL] = nlcdLasso(y,AT,Phi,PhiT,Psi,PsiT,recopts);
                    Cos_sim_nlcdLasso(ss,mm,ee) = xhatNL'*x/(norm(x)*(norm(xhatNL)));
                    
                case 'DST'
                    recopts.s = svec(ss);
                    recopts.m = mvec(mm);
                    recopts.n = n;
                    recopts.landa = 1e-3;      %tuning parameter
                    recopts.eta = 4;           %Step Size
                    recopts.th = 0.001;           %for faster terminating
                    recopts.verbose = 1;
                    recopts.init = 'zeros';  % 'zeros', 'random'   'oneshot'
                    recopts.it = 800;       %100:200:700;  %iteration for multishot algorithm
                    [xhatST,w_hatST,z_hatST,xoo,woo,zoo,iter,err] = multishotsoft(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);
                    Cos_sim_DST(ss,mm,ee) = xhatST'*x/(norm(x)*(norm(xhatST)));
                    Norm_err_DST(ss,mm,ee) = norm(xhatST-x)/norm(x);
                    
                case 'all'
                    recopts.s1 = svec(ss);
                    recopts.s2 = svec(ss);
                    recopts.s = svec(ss);         %sparsity
                    recopts.bl = b;
                    recopts.numblocks = num_blocks;
                    recopts.K = K;
                    recopts.m = mvec(mm);         %measurements
                    recopts.n = n;
                    recopts.landa = 1e-3;      %tuning parameter
                    recopts.eta = 15;           %Step Size
                    recopts.th = 0.001;           %for faster terminating
                    recopts.init = 'zeros';  % 'zeros', 'random' 'oneshot'
                    recopts.model_Sparsity = 'arbit_saprsity'; %'arbit_saprsity'
                    recopts.recflag = 'totally_sparse';  %individually_sparse
                    recopts.it = 500;       %100:200:700;  %iteration for multishot algorithm
                    recopts.lambda = 1;
                    recopts.verbose = 0;
                    
                    [xhatHT,w_hatm,z_hatm,xhatOne,w_hatOne,z_hatOne,err_trace] = htdemix(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);  %Random DHT
                    
                    recopts.model_Sparsity = 'struc_sparsity'; %'arbit_saprsity'
                    recopts.eta = 25;           %Step Size
                    [xhatHTStuc,w_hatmStruc,z_hatmStuc,~,~,~,err_traceStruc] = htdemix(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);    %Structured DHT
                    
                    
                    recopts.eta = 4;           %Step Size
                    recopts.it = 800;       
                    [xhatST,w_hatST,z_hatST,~,~,~,~,~]  = multishotsoft(y,g,A,AT,Phi,PhiT,Psi,PsiT,recopts);        %Random DST
                    
                    %                     [xhatNL,w_hatNL,z_hatNL] = nlcdLasso(y,AT,Phi,PhiT,Psi,PsiT,recopts);
                    
                    Cos_sim_DHT(ss,mm,ee) = xhatHT'*x/(norm(x)*(norm(xhatHT)));
                    Cos_sim_DHT_block_spa(ss,mm,ee) = xhatHTStuc'*x/(norm(x)*(norm(xhatHTStuc)));
                    Cos_sim_DST(ss,mm,ee) = xhatST'*x/(norm(x)*(norm(xhatST)));
                    
                    %                     Cos_sim_nlcdLasso(ss,mm,ee) = xhatNL'*x/(norm(x)*(norm(xhatNL)));
                    %                     Cos_sim_oneshot(ss,mm,ee) = xhatOne'*x/(norm(x)*(norm(xhatOne)));
                    
                    Norm_err_DHT(ss,mm,ee) = norm(xhatHT-x)/norm(x);
                    Norm_err_DHT_block_spa(ss,mm,ee) = norm(xhatHTStuc-x)/norm(x);
                    Norm_err_DST(ss,mm,ee) = norm(xhatST-x)/norm(x);
                    %                     Norm_err_nlcdLasso(ss,mm,ee) = norm(xhatNL-x)/norm(x);
                    %                     Norm_err_oneshot(ss,mm,ee) = norm(xhatOne-x)/norm(x);
            end
            
        end
    end
end

clock
switch opts.recovery
    %     case 'HTM'
    %         figure, plot(mvec,Cos_sim_HTM(1,:,1),'LineWidth',2);
    %         hold on
    %         plot(mvec,Cos_sim_HTMinv(1,:,1),'r','LineWidth',2);
    %         grid on
    %         axisfortex('f(x) = logistic','m','Cosine Simalrity')
    %         legend('HTM','Linear Inv')
    %         figure, plot(mvec,Norm_err_HTM(1,:,1),'LineWidth',2);
    %         hold on
    %         plot(mvec,Norm_err_HTMinv(1,:,1),'r','LineWidth',2);
    %         grid on
    %         axisfortex('f(x) = logistic','m','Norm error')
    %         legend('HTM','Linear Inv')
    %             case 'STM'
    %                 figure, plot(mvec,Cos_sim_STM(1,:,1),'LineWidth',2);
    %             case 'nlcdlasso'
    %                 figure, plot(mvec,Cos_sim_nlcdLasso(1,:,1),'LineWidth',2);
    %                     case 'oneshot'
    %                         figure, plot(mvec,Cos_sim_oneshot(1,:,1),'LineWidth',2);
    %                         figure, plot(mvec,Norm_err_oneshot(1,:,1),'LineWidth',2);
    %             case 'all'
    %                 figure,
    %                 plot(mvec,Cos_sim_HTM(1,:,1),'LineWidth',2);
    %                 hold on
    %                 plot(mvec,Cos_sim_STM(1,:,1),'r','LineWidth',2);
    %                 hold on
    %                 plot(mvec,Cos_sim_nlcdLasso(1,:,1),'g','LineWidth',2);
    %                 hold on
    %                 plot(mvec,Cos_sim_oneshot(1,:,1),'k','LineWidth',2);
end
