function y = block_thresh(x,num_blocks,b,K)

y = 0*x;
N = length(y);

proxy__block = reshape(x, b, num_blocks);
[~,blockww] = sort(sum(proxy__block.^2,1),'descend');
newsupp = zeros(b,num_blocks);
newsupp(:,blockww(1:K)) = 1;
newsupp = reshape(newsupp, N, 1);

ind = find(newsupp==1);
y(ind) = x(ind);