\section{Numerical Results}
\label{sec:Results}

In this section, we provide some representative numerical experiments for our proposed algorithm based on synthetic and real data. We also compare its performance with a LASSO-type technique for demixing. This method, first proposed in~\cite{plan2014high}, was not explicitly developed in the demixing context, but is suitable for our problem. We call this method the \textit{Nonlinear convex demixing LASSO}, or the  \textsc{NlcdLASSO} for short. Using our notation from Section \ref{sec:Mathematicalsetup} and \ref{sec:AlgTheorm}, \textsc{NlcdLASSO} solves the following convex problem:
\begin{equation} \label{prob 6.1}
\begin{aligned}
& \underset{z,w}{\text{min}}
& & \|\widehat{x}_{\lin} - (\Phi z + \Psi w)\|\\
& \text{subject to} 
& & \|w\|_1\leq \sqrt{s},\quad\|z\|_1\leq \sqrt{s} .
\end{aligned}
\end{equation}
Here, $\widehat{x}_{\lin}$ denotes the linear estimate of $x$ (equal to ) and $s$ denotes to the sparsity level of signals $w$ and $z$ in basis $\Phi$ and $\Psi$, respectively. The constraints in problem \ref{prob 6.1} are convex penalties reflecting the knowledge that $w$ and $z$ are $s$-sparse and have unit $\ell_2$-norm. The outputs of this algorithm are the estimates $\widehat{w}$, $\widehat{x}$, and $\widehat{x} = \Phi\widehat{w} + \Psi\widehat{z}.$
To solve the optimization problem in \ref{prob 6.1}, we have used SPGL1 \cite{Berg2008,spgl12007}. This solver can handle large scale problems, which is the scenario that we have used in our experimental evaluations. 

\subsection{Synthetic data}
We precisely describe the setup of our simulations for synthetic data. First, we generate $w$ and $z$ belonging to $\mathbb{R}^n$ with $n={2^{20}}$, whose support is randomly generated among all supports in $\R^n$ with $s$ nonzero elements. According to the discussion in the Introduction, for successful recover we require that the constituent signals to be incoherent enough. To achieve this, we consider that the signal $w$ to be $s$-sparse in the Haar wavelet basis, and $z$ to be $s$-sparse in the noiselet basis~\cite{coifman2001noiselets}. For the measurement operator $A$, we choose a partial DFT matrix. Such matrices are known to have similar recovery performance as random Gaussian matrices, but enable fast numerical operations~\cite{candes2006robust}.

Due to this measurement model, the scale (amplitude) of the underlying signal is irrevocably lost. To measure recovery performance in the absence of scale information, we use the criterion of \textit{Cosine Similarity} between $x$ and $\widehat{x}$ to compare the performance of different methods. More precisely, suppose \textsc{Oneshot} (or \textsc{NlcdLASSO}) outputs $\widehat{w}$ and $\widehat{z}$, such that $\widehat{x} = \Phi\widehat{w} + \Psi\widehat{z}$. Then, the Cosine Similarity criterion is defined as follows:
$$\cos(x, \widehat{x}) = \frac{x^T\hat{x}}{\|x\|\|\widehat{x}\|}. $$

% -------------------------------------------------------------------------
%\begin{figure}
%\centering
%\includegraphics[width=0.5\linewidth]{../Figs/all.eps}
%\caption{\emph{Performance of \textsc{Oneshot} and \textsc{NlcdLASSO} according to the \textsc{Cosine Similarity} for different choices of sparsity level $s$ for $sign(x)$ as the nonlinear function.}}
%\label{figCos}
%%
%\end{figure}

\begin{figure}[h]
\begin{center}
\begin{tabular}{cc}
\includegraphics[height=2.4in]{../Figs/all.eps} &
\includegraphics[height=2.4in]{../Figs/all2.eps} \\
(a) &(b) \\
\end{tabular}
\end{center}
\caption{\emph{Performance of \textsc{Oneshot} and \textsc{NlcdLASSO} according to the \textsc{Cosine Similarity} for different choices of sparsity level $s$ for $(a)$ $f(x) = sign(x)$, and $(b)$ $f(x) = \frac{e^x-1}{e^x+1}$ as the nonlinear functions.}}
\label{figCos}
\end{figure}


Figure \ref{figCos} illustrates the performance of \textsc{Oneshot} and \textsc{NlcdLASSO} according to the Cosine Similarity for different choices of sparsity level $s$ when nonlinear link functions are set to $f(x) = \text{sign(x)}$ (a), and $f(x) = \frac{e^x-1}{e^x+1}$ (b), respectively. The horizontal axis denotes an increasing number of measurements. Each data point in the plot is obtained by conducting a Monte Carlo experiment in which a new random measurement matrix $A$ is generated, and averaging over $100$ trials. 

As we can see from the plots, we obtain similar results for both choices of link functions. Notably, the performance of \textsc{NlcdLASSO} is worse than \textsc{OneShot} for any fixed choice of $m$ and $s$. Even when the number of measurements increases (for example, at $m = 4850$), we see that \textsc{OneShot} outperforms \textsc{NlcdLASSO} by a significant degree. In this case, \textsc{NlcdLASSO} has $30\%$ error in the estimation of true signal $x$ for the sign link function and $20\%$ error for the sigmoid link function, while \textsc{OneShot} achieves close to zero error. This result suggests the inefficiency of \textsc{NlcdLASSO} for the purpose of nonlinear demixing. 

Figure \ref{SignalComponent} shows the true signal and its components ($x$, $\Phi w$ and $\Phi z$) as well as the reconstructed signals using \textsc{Oneshot}. In this experiment, we have used a standard normal random matrix as the measurement matrix $A$, $f(x)=sign(x)$ as the nonlinear link function, and a test signal of length $2^{15}$. The sparsity parameter $s$ is set to 15, and the number of measurements is set to $5000$. From visual inspection, we can observe that both true and reconstructed signals as well as true constituent signals and their estimations are close to each other (up to a scaling factor). 
%We believe that if the size of signal increases, then this scaling problem is solved. But increasing the size of the signal causes computational challenges due to the storing large standard normal random matrix and matrix-vector multiplication, as well.

\begin{figure}[t]
\centering
\subfloat[]{\includegraphics[scale=0.24]{../Figs/true1.png}} 
\subfloat[]{\includegraphics[scale=0.24]{../Figs/oneshot1.png}} 
\caption{(a) Ground truth $x$, $\Phi w$, and $\Phi z$. (b) \textsc{Oneshot} estimates $\hat{x}$, $\Phi\hat{w}$, and $\Phi\hat{z}$.}
\label{SignalComponent}
\end{figure}

Finally, we contrast the running time of both algorithms, illustrated in Figure \ref{RunninTime}. In this experiment, we have measured the wall-clock running time of the two algorithms, by varying signal size $x$ from $n = 2^{10}$ to $n = 2^{20}$.  Here, we set the number of measurements to $m = 500$, sparsity level to $s=5$, and the number of Monte Carlo trials to $1000$. Also, the nonlinear link function is considered as $f(x)=sign(x)$. As we can see from the plot, \textsc{OneShot} is $12$ times faster than \textsc{NlcdLASSO} when the size of signal equals to $2^{20}$ which makes \textsc{OneShot} very efficient even for large-scale nonlinear demixing problems.  We mention that in the above setup, the main computational costs incurred in \textsc{OneShot} involve a matrix-vector multiplication followed by a thresholding step, both of which can be performed in time that is \emph{nearly-linear} in terms of the signal dimension $n$. In particular, we experimentally verified that varying the sparsity level does not have any effect in the running time.

\begin{figure}
\centering
\includegraphics[width=0.5\linewidth]{../Figs/RTs5log.eps}
\caption{\emph{Comparison of running times of \textsc{OneShot} with \textsc{NlcdLASSO}. Our proposed algorithm is non-iterative, and in contrast with LASSO-based techniques, has a running time that is \emph{nearly-linear} in the signal dimension $n$ for certain structured sparsifying bases. %Moreover, the running time is independent of the sparsity level $s$.
}}
\label{RunninTime}
%
\end{figure}
 
 \subsection{Real data}
In this section, we provide representative results on real-world 2-dimensional data using \textsc{Oneshot} and \textsc{NlcdLASSO}.

For the 2-dimensional scenario, we start with a $256\times 256$ test image. First, we obtain its 2D Haar wavelet decomposition and retain the $s=500$ largest coefficients, denoted by the $s$-sparse vector $w$. Then, we reconstruct the image based on these largest coefficients, denoted by $\widehat{x} = \Phi w$. Similar to the synthetic case, we generate a noise component in our superposition model based on 500 noiselet coefficients $z$. In addition, we consider a parameter which controls the strength of the noiselet component contributing to the superposition model. We set this parameter to 0.1. Therefore, our test image $x$ is given by $x = \Phi w + 0.1\Psi z$.

\begin{figure}[h]
\begin{center}
\begin{tabular}{cc}
\includegraphics[height=1.5in]{../Figs/X2d.eps} &
\includegraphics[height=1.5in]{../Figs/Phiw2d.eps} \\
\large$x$ &\large$\Phi w$ \\
\includegraphics[height=1.5in]{../Figs/Xhat2d.eps} &
\includegraphics[height=1.5in]{../Figs/Phiwhat2d.eps} \\
\large$\widehat{x}$ -- \textsc{Oneshot} &\large$\Phi\widehat{w}$ -- \textsc{Oneshot}  \\
\includegraphics[height=1.5in]{../Figs/XhatL2d.eps} &
\includegraphics[height=1.5in]{../Figs/PhiwhatL2d.eps} \\
\large$\widehat{x}$ -- \textsc{NlcdLASSO} &\large$\Phi\widehat{w}$ -- \textsc{NlcdLASSO}  \\
\end{tabular}
\end{center}
\caption{Comparison of \textsc{Oneshot} and \textsc{NlcdLASSO} for demixing problem in real 2-dimensional case. }\label{real2d}
\end{figure}

Figure~\ref{real2d} illustrates both the true and the reconstructed images $x$ and $\widehat{x}$ as well as both the true and the reconstructed wavelet-sparse component, denoting by $\Phi w$ and $\Psi\widehat{w}$, respectively. The number of measurements is set to $35000$. In this simulation, for measurement matrix $A$, we have used a partial (subsampled) DFT matrix due to the difficulties in large-scale computations with Gaussian measurement matrices. However, the main theoretical results obtained in this paper are applicable to random Gaussian measurement matrices. Hence, our theoretical results do not directly apply in this, and we defer a thorough investigation of random DFT measurement matrices for the nonlinear demixing problem as future research.  Nevertheless, the results are promising, and from visual inspection we see that the reconstructed image, $\widehat{x}$, using \textsc{Oneshot} is better than the reconstructed image by \textsc{NlcdLASSO}. Quantitatively, we also calculate Peak signal-to-noise-ratio (PSNR) of the reconstructed images using both algorithms relative to the test image, $x$. We have:
\begin{align*}
PSNR_{\textsc{Oneshot}} = 19.8335 \,dB \qquad\qquad PSNR_{\textsc{NlcdLASSO}} = 17.9092 \,dB
\end{align*}
therefore illustrating the superior performance of \textsc{Oneshot} compared to \textsc{NlcdLASSO}.