\section{A New Demixing Algorithm}
\label{sec:AlgTheorm}

Having defined the above quantities, we now present our proposed demixing algorithm.
Recall that we wish to recover components $w$ and $z$ (modulo a scale ambiguity), given the nonlinear measurements $y$ and the matrix $A$. Our proposed algorithm, that we call \textsc{OneShot}, is described in pseudocode form below.

The mechanism of \textsc{OneShot} is simple. At a high level, \textsc{OneShot} first constructs a \emph{linear estimator} of the target superposition signal, denoting by $\widehat{x}_\text{\lin} = \frac{1}{m} A^T y$, and then projects $\widehat{x}_\text{\lin}$ onto the constraint sets $K_1$ and $K_2$. Finally, it combines these two projections to obtain the final estimate of the target superposition signal.


\begin{algorithm}
\label{alg:oneshot}
\caption{\textsc{OneShot}}
\textbf{Inputs:} Basis matrices $\Phi$ and $\Psi$, measurement matrix $A$, measurements $y$, sparsity level $s$. \\
\textbf{Outputs:} Estimates  $\widehat{x}=\Phi\widehat{w} + \Psi\widehat{z}$, $\widehat{w}\in K_1$, $\widehat{z}\in K_2$

\setlength{\parskip}{1em}
$\widehat{x}_\text{\lin}\leftarrow\frac{1}{m}A^{T}y$\qquad\{form linear estimator\}\\
$b_1\leftarrow\Phi^*\widehat{x}_\lin$\qquad~\{forming first proxy\}\\
$\widehat{w}\leftarrow\mathcal{P}_s(b_1)$\qquad~~\{Projection on set $K_1$\}\\
$b_2\leftarrow\Psi^*\widehat{x}_\lin$\qquad~\{forming second proxy\}\\
$\widehat{z}\leftarrow\mathcal{P}_s(b_2)$\qquad~~~~\{Projection on set $K_2$\}\\
$\widehat{x}\leftarrow\Phi\widehat{w} + \Psi\widehat{z}$\quad~~~\{Estimating $\widehat{x}$\}
\end{algorithm}

Here and below, for simplicity we assume that the sparsity levels $s_1$ and $s_2$, specifying the sets $K_1$ and $K_2$, are equal, i.e., $s_1 = s_2 = s$. The algorithm (and analysis) effortlessly extends to the case of unequal sparsity levels. Also, we have used the following \emph{projection} operators:
$$\widehat{w} = \mathcal{P}_s(\Phi^*\widehat{x}_\text{\lin}),  \quad \hat{z} = \mathcal{P}_s(\Psi^*\widehat{x}_\text{\lin}).$$
Here, $\mathcal{P}_s$ denotes the projection onto the set of (canonical) $s$-sparse signals $K$, and can be implemented by hard thresholding. Observe that \textsc{OneShot} is \emph{not} an iterative algorithm; this property of the algorithm enables us to achieve a fast running time. 

We now provide a rigorous performance analysis of \textsc{OneShot}. Our proofs follow the geometric approach provided in~\cite{plan2014high}, specialized to the demixing problem. In particular, we derive an upper bound on the estimation error of the \emph{component} signals $w$ and $z$, modulo scaling factors.
In our proofs, we use the following result from~\cite{plan2014high}, restated here for completeness.
\begin{lemma}(Quality of linear estimator).\label{lemma 4.1}
Given the model in Equation \ref{eq 3.1}, the linear estimator, $\widehat{x}_\text{\lin}$, is an unbiased estimator of $\bar{x}$ up to constants.  That is, $\mathbb{E}(\widehat{x}_\textrm{lin})= \mu\bar{x}$ and:
$\mathbb{E}\|\widehat{x}_\textrm{lin} - \mu\bar{x}\|_2^2 = \frac{1}{m}[\sigma^2 +\eta^2(n-1)],$
where 
$\mu = \mathbb{E}(y_1\langle a_1, \bar{x}\rangle),~\sigma^2 = Var(y_1\langle a_1, \bar{x}\rangle),~\eta^2 = \mathbb{E}(y_1^2).
$
\end{lemma}
%
%\begin{proof}
%The proof is given in \cite{plan2014high} 
%\end{proof}

We now state the main theoretical result of this paper, characterizing the estimation error of \textsc{OneShot}, with proofs provided below in Section \ref{sec 4}.

\begin{theorem}(Main theorem.)
\label{thm:main}
Let $y\in\mathbb{R}^m$ be given the set of nonlinear measurements. Let $A\in \mathbb{R}^{m\times n}$ be a random matrix with i.i.d.\ standard normal entries. Also, let $\Phi, \Psi\in \mathbb{R}^{n\times n}$ are bases with incoherence $\varepsilon$, as defined in Definition \ref{def 3.2}. If we use \textsc{Oneshot} to recover $w$ and $z$ (up to a scaling) described in equations \eqref{eq 3.1} and \eqref{eq 3.2}, then the error estimation of the constituent signal, $w$ (similar bound satisfies for the constituent signal, $z$) satisfies the following upper bound $\forall t >0$:

\begin{align}\label{MT}
\mathbb{E}\|\widehat{w}-\mu\alpha\bar{w}\|\leq t + \frac{2\sqrt{2}\sigma}{\sqrt{m}}\left(\frac{1+\varepsilon}{\sqrt{1-\varepsilon}}\right) + 2\sqrt{2}\mu \left(\frac{\varepsilon}{\sqrt{1-\varepsilon}}\right) + \frac{2\eta}{t\sqrt{m}}W_t(K).
\end{align}
\end{theorem}

The authors of~\cite{planRmanrobust,plan2014high} provide upper bounds on the local mean width $W_t(K)$ of the set of $s$-sparse vectors. In particular, for any $t > 0$ they show that $W_t(K) \leq C t \sqrt{s \log (2n/s)}$ for some absolute constant $C$. Plugging in this bound and combining terms gives the following:
\begin{corollary}
\label{corr:estx}
With the same assumptions as Theorem~\ref{MT}, the error of nonlinear estimation incurred by the final output $\widehat{x}$ satisfies the upper bound:
\begin{align}
\mathbb{E}\|\widehat{x} - \mu\bar{x}\| &\leq \frac{4\sqrt{2}\sigma}{\sqrt{m}}\left(\frac{1+\varepsilon}{\sqrt{1-\varepsilon}}\right) \nonumber\\ 
&+4\sqrt{2}\mu \left(\frac{\varepsilon}{\sqrt{1-\varepsilon}}\right) +\frac{C\eta}{\sqrt{m}}\sqrt{s\log\frac{2n}{s}}. \label{eq:estx}
\end{align}
\end{corollary}



\begin{corollary}(Example quantitative result). The constants $\sigma, \eta, \mu$ depend on the nature of the nonlinear function $f$, and are often rather mild. For example, if $f(x) = \mathrm{sign}(x)$, then we may substitute
$$\mu = \sqrt{\frac{2}{\pi}}\approx 0.8,\qquad\sigma^2 = 1- \frac{2}{\pi}\approx 0.6,\qquad\eta^2=1,$$
in the above statement.
Hence, the bound in \eqref{eq:estx} becomes:
\begin{align}
\mathbb{E}\|\widehat{x} - \mu\bar{x}\|\leq&\frac{4}{\sqrt{m}}\left(\frac{1+\varepsilon}{\sqrt{1-\varepsilon}}\right) 
 + 4.53\left(\frac{\varepsilon}{\sqrt{1-\varepsilon}}\right) + \frac{C}{\sqrt{m}}\sqrt{s\log\frac{2n}{s}} \, .
\end{align}
\end{corollary}

\begin{proof}
Using Lemma \ref{lemma 4.1}, $\mu = \mathbb{E}(y_i\langle a_i, \bar{x}\rangle)$ where $y_i = \mathrm{sign}(\langle a_i, x\rangle)$. Since $a_i\sim\mathcal{N}(0,I)$ and $\bar{x}$ has unit norm, $\langle a_i, \bar{x}\rangle\sim\mathcal{N}(0,1)$. Thus, $\mu = \mathbb{E}|g| = \sqrt{\frac{2}{\pi}}$ where $g\sim\mathcal{N}(0,I)$. Moreover, we can write $\sigma^2 = \mathbb{E}(|g|^2)-\mu^2 = 1- \frac{2}{\pi}$. Here, we have used the fact that $|g|^2$ obeys the $\chi^2_1$ distribution with mean 1. Finally, $\eta^2 = \mathbb{E}(y_1^2) = 1$.
\end{proof}

%\begin{remark}(Implications).
In contrast with demixing algorithms for traditional (linear) observation models, our estimated signal $\widehat{x}$ can differ from the true signal $x$ by a scale factor. Next, suppose we fix $\delta > 0$ as a small constant, and suppose that the incoherence parameter $\varepsilon = c\delta$ for some constant $c$, and that the number of measurements scales as: 
\[
m = O\left(\frac{s}{\delta^2}\log\frac{n}{s}\right).
\]
Then, the (expected) estimation error $\| \widehat{x} - \mu \bar{x} \| \leq O(\delta)$. In other words, the \emph{sample complexity} of \textsc{OneShot} is given by $m = O(\frac{1}{\delta^2} s \log(n/s))$, which resembles results for the linear observation case~\cite{spinIT,plan2014high}\footnote{Here, we use the term ``sample-complexity" as the number of measurements required by a given algorithm to achieve an estimation error $\delta$. However, we must mention that algorithms for the linear observation model are able to achieve stronger sample complexity bounds that are independent of $\delta$.}.
%\end{remark}

We also observe that the estimation error in \eqref{eq:estx} is upper-bounded by $O(\varepsilon)$. This  is meaningful only when $\varepsilon \ll 1$, or when $s \gamma \ll 1$. Per the Welch Bound~\cite{foucart2013}, the mutual coherence $\gamma$ satisfies $\gamma \geq 1/\sqrt{n}$. Therefore, Theorem \ref{thm:main} provides non-trivial results only when $s = o(\sqrt{n})$. This is consistent with the \emph{square-root bottleneck} in sparse approximation~\cite{tropp07}.
%\end{remark}

The main theorem obtains a bound on the expected value of the estimation error. We can derive a similar upper bound that holds with high probability. In this theorem, we assume that measurements $y_i$ for $i=1,2,\ldots,m$ have a \emph{sub-gaussian} distribution. See \cite{vershynin2010introduction} for a comprehensive discussion about sub-gaussian random variables. We obtain the following result, with proofs deferred to Section \ref{sec 4}.

\begin{theorem}(High-probability version of main theorem.)
\label{thm:high}
Let $y\in\mathcal{R}^m$ be a set of measurements with a sub-gaussian distribution. Assume that $A\in \mathbb{R}^{m\times n}$ is a random matrix with $i.i.d$ standard normal entries. Also, assume that $\Phi, \Psi\in \mathbb{R}^{n\times n}$ are two bases with incoherence $\varepsilon$ as in Definition \ref{def 3.2}. Let $0\leq s\leq\sqrt{m}$. If we use \textsc{Oneshot} to recover $w$ and $z$ (up to a scaling) described in \eqref{eq 3.1} and \eqref{eq 3.2}, then the estimation error of the output of \textsc{Oneshot} satisfies the following:

\begin{align} 
\|\widehat{x} - \mu\bar{x}\| &\leq \frac{2\sqrt{2}\eta s}{\sqrt{m}}\left(\frac{1+\varepsilon}{\sqrt{1-\varepsilon}}\right) + 4\sqrt{2}\mu \left(\frac{\varepsilon}{\sqrt{1-\varepsilon}}\right) +
\frac{C\eta}{\sqrt{m}}\sqrt{s\log\frac{2n}{s}} + 4\frac{\eta s}{\sqrt{m}} ,
\end{align}
with probability at least $1-4\exp(-\frac{cs^2\eta^4}{\|y_1\|_{\psi_2}^4})$ where $C, c> 0$ are absolute constants. The coefficients $\mu, \sigma$, and $\eta$ are given in Lemma \ref{lemma 4.1}. Here, $\|y_1\|_{\psi_2}$ denotes the $\psi_2$-norm of the first measurement $y_1$. 

\end{theorem}
%
%\begin{proof}
%See Section \ref{sec 4}.
%\end{proof}

\begin{remark}
In Theorem~\ref{thm:high}, we stated the tail probability bound of the estimation error for the superposition signal, $x$. Similar to Theorem~\ref{thm:main}, we can derive a completely analogous tail probability bound in terms of the constituent signals $w$ and $z$.
\end{remark}