\section{Proofs}\label{sec 4}
\label{sec:proof}

In this section, we derive the proof of Theorem~\ref{thm:main}. The proof mostly follows the method of \cite{plan2014high}. As a precursor, we need the following lemma from geometric functional analysis, restated from~\cite{plan2014high}.

\begin{lemma}\label{lemma 5.1}
Assume $K$ is a closed star-shaped set. Then for $u\in K$, and $a\in \mathbb{R}^n$, one has the following result $\forall t > 0$:
\begin{equation}\label{eq 5.1}
\|\mathcal{P}_K(a) - u\|_2\leq\max (t, \frac{2}{t}\|a - u\|_{K_t^o}).
\end{equation}
\end{lemma}

%\begin{proof}
%
%Define $$ d\overset{\Delta}{=} \|\mathcal{P}_K(a) - u\|_2.$$ 
%This distance satisfies $$d^2\leq 2\|\mathcal{P}_K(a) - u\|_{k_t^o}.$$
%Also, $\frac{1}{t}\|x\|_{k_t^o}$ is a non-increasing function of $t$. Hence, if $d\leq t$, then \ref{eq 5.1} is clearly satisfied. If $d > t$. then using the fact that $\frac{1}{t}\|x\|_{k_t^o}$ is a non-increasing function of $t$, \eqref{eq 5.1} is resulted. The details can be found in \cite{plan2014high}.
%\end{proof}
We also use the following result of~\cite{plan2014high}.

\begin{claim}(Orthogonal decomposition of $a_i$.)\label{re 4.5}
Suppose we decompose the rows of $A$, $a_i$, as:
\begin{align}
 a_i =  \langle{a_i,\bar{x}}\rangle\bar{x} + b_i,
\end{align} where $b_i\in\mathbb{R}^n$ is orthogonal to $\bar{x}$. Then we have $b_i\sim\mathcal{N}(0,I_{x^{\perp}})$ since $a_i\sim\mathcal{N}(0,I)$. Also, $I_{x^{\perp}}=I-\bar{x}\bar{x}^T.$ Moreover, the measurements $y_i$ in equation \ref{eq 3.1} and the orthogonal component $b_i$ are statistically independent.
\end{claim}

\begin{proof}[Proof of Theorem~\ref{thm:main}]
Observe that the magnitude of the signal $x$ may be completely lost due to the action of the nonlinear measurement function $f$ (such as the $\mathrm{sign}(\cdot)$ function). Therefore, our recovered signal $\widehat{x}$ approximates the true signal modulo (possibly unknown) scaling factor. Indeed, for $\mu$ defined in Lemma~\ref{lemma 4.1}, we have:
\begin{align}
\|\widehat{x} - \mu\bar{x}\| &= \|\Phi\widehat{w}+\Psi\widehat{z}-\alpha\mu\Phi\bar{w}-\alpha\mu\Psi\bar{z}\| \nonumber\\
&\leq\|\Phi\|\|\widehat{w}-\mu\alpha\bar{w}\|+\|\Psi\|\|\widehat{z}-\mu\alpha\bar{z}\| \nonumber\\
&\leq (t+\frac{2}{t}\|\Phi^*\widehat{x}_{\lin}-\mu\alpha\bar{w}\|_{K_t^o}) + (t+\frac{2}{t}\|\Psi^*\widehat{x}_{\lin}-\mu\alpha\bar{z}\|_{K_t^o}) \, .\nonumber
\end{align}

The equality comes from the definition of $\bar{x}$. The first inequality results from an application of the triangle inequality and the definition of the operator norm of a matrix, while the second inequality follows from Lemma \ref{lemma 5.1}. 

It suffices to derive a bound on the first term in the above expression (since a similar bound will hold for the second term.) This proves the first part of Theorem~\ref{thm:main}. We have:

\begin{align}
\|\Phi^*\widehat{x}_{\lin}-\mu\alpha\bar{w}\|_{K_t^o} &= \|\Phi^*\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle\bar{x}+y_i b_i)-\mu\alpha\bar{w}\|_{K_t^o}\nonumber\\
&\leq\|\Phi^*\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle\bar{x})-\mu\alpha\bar{w}\|_{K_t^o}+\|\Phi^*\frac{1}{m}\Sigma_i y_i b_i\|_{K_t^o}\nonumber\\
&\leq\underbrace{\|\Phi^*\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle\bar{x})-\mu\Phi^*\bar{x}\|_{K_t^o}}_\text{$S_1$}
+ \underbrace{\|\mu\alpha\Phi^*\Psi\bar{z}\|_{K_t^o}}_\text{$S_2$}+\underbrace{\|\Phi^*\frac{1}{m}\Sigma_i y_i b_i\|_{K_t^o}}_\text{$S_3$} . \label{eq}
\end{align}

The first equality follows from the orthogonal decomposition of $a_i$, while the second and third inequalities result from the triangle inequality. We first bound $S_1$ as follows:
\begin{align*}
S_1 &= \|\Phi^*\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle\bar{x})-\mu\Phi^*\bar{x}\|_{K_t^o} \\
&= \|(\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle-\mu) )\Phi^*\bar{x}\|_{K_t^o}\\
&= \lvert\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle-\mu) \rvert\|\Phi^*\bar{x}\|_{K_t^o} .
\end{align*}
Therefore, 
\[
\mathbb{E}(S_1^2) = \mathbb{E}(|\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle-\mu) |^2\|\Phi^*\bar{x}\|_{K_t^o}^2) .
\]
Define $\gamma_i\overset{\Delta}{=} y_i\langle a_i,\bar{x}\rangle - \mu_i$. Then,
 \begin{align*}
\mathbb{E}(|\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle-\mu) |^2)&=\mathbb{E}(\frac{1}{m^2}(\Sigma_i \gamma_i)^2)\\
&= \mathbb{E}(\frac{1}{m^2}(\sum_{i=}^{m} \gamma_i^2 + \Sigma_{i\neq j}\gamma_i\gamma_j)) \\
&=\frac{1}{m^2}(\sum_{i=1}^m \mathbb{E}\gamma_i^2) = \frac{1}{m}\mathbb{E}\gamma_1^2 \\
&= \frac{\sigma^2}{m}
\end{align*}
where $\sigma^2$ has been defined in Lemma \ref{lemma 4.1}. The third and last equalities follow from the fact that the $y_i$'s are independent and identically distributed. 

Now, we bound $\|\Phi^*\bar{x}\|_{K_t^o}^2$ as follows::

\begin{align*}
\|\Phi^*\bar{x}\|_{K_t^o} &= \sup_{u\in (K-K)\cap tB_n^2}\langle\Phi^*\bar{x},u\rangle \\
&= t\sup_{\substack{v_1\in \frac{1}{t}K, v_2\in \frac{1}{t}K\\\|v_i\|\leq 1, i=1,2}}\langle\Phi^*\bar{x},v_1-v_2\rangle\\
&\leq 2t\sup_{\substack{\|a\|_0\leq s\\\|a\|\leq 1}}|\langle\Phi^*\bar{x},a\rangle| \\
&\leq 2t(\sup_{\substack{\|a\|_0\leq s\\\|a\|\leq 1}}|\langle\alpha\bar{w},a\rangle|+\sup_{\substack{\|a\|_0\leq s\\\|a\|\leq 1}}|\langle\alpha\Phi^*\Psi\bar{z},a\rangle|)\\
&\leq 2\alpha t(1+\sup_{\substack{\|a\|_0\leq s\\\|a\|\leq 1}}|\langle\alpha\Psi\bar{z},\Phi a\rangle|) \\
&=2\alpha t(1+\varepsilon).
\end{align*}

This implies that:
\begin{equation}\label{eq 5.3}
\Longrightarrow\mathbb{E}(S_1^2)\leq 4\frac{\alpha^2 t^2\sigma^2}{m}(1+\varepsilon)^2 .
\end{equation}

The second inequality follows from \eqref{eq 3.2} and the triangle inequality. The last inequality is results from an application of the Cauchy-Schwarz inequality and the definition of $\varepsilon$.

Similarly, we can bound $S_2$ as follows:

\begin{align}
\mathbb{E}(S_2) &= \mathbb{E}(\|\mu\alpha\Phi^*\Phi\bar{z}\|_{K_t^o}) \nonumber\\
&=\mathbb{E}(|\mu\alpha|\|\Phi^*\Phi\bar{z}\|_{K_t^o}) \nonumber\\
&=|\mu\alpha|\|\Phi^*\Phi\bar{z}\|_{K_t^o} \nonumber\\
&=|\mu\alpha|\sup_{u\in (K-K)\cap tB_n^2}\langle\Psi\bar{z},\Phi u\rangle \nonumber\\
&=|\mu\alpha|t\sup_{\substack{v_1\in \frac{1}{t}K, v_2\in \frac{1}{t}K\\\|v_i\|\leq 1, i=1,2}}\langle\Psi\bar{z},\Phi(v_1-v_2)\rangle\nonumber\\
&\leq 2\mu\alpha t\varepsilon .\label{eq 5.4}
\end{align}

Finally, we give the bound for $S_3$. Define $\quad L\overset{\Delta}{=} \frac{1}{m}\Sigma_i y_i b_i$. Then, we get:
\[
\mathbb{E}(S_3)=\mathbb{E}\|\Phi^*\frac{1}{m}\Sigma_i y_i b_i\|_{K_t^o} = \mathbb{E}\|\Phi^*L\|_{K_t^o} .
\]

Our goal is to bound $\mathbb{E}\|\Phi^*L\|_{K_t^o}$. Since $y_i$ and $b_i$ are independent random variables (as per Claim \ref{re 4.5}), we can use the law of conditional covariances and the law of iterated expectation. That is, we first condition on $y_i$, and then take expectation with respect to $b_i$.

By conditioning on $y_i$, we have $L\sim\mathcal{N}(0,\beta^2 I_{x^{\perp}})$ where $I_{x^{\perp}} = I - \bar{x}\bar{x}^T$ is the covariance of vector $b_i$ according to claim \ref{re 4.5} and $\beta^2 = \frac{1}{m^2}\Sigma_i y_i^{2}$. Define $g_{x^{\perp}}\sim\mathcal{N}(0,I_{x^{\perp}})$. Therefore, $L= \beta g_{x^{\perp}}$  Putting everything together, we get:
\begin{align*}
\mathbb{E}(S_3)&=\mathbb{E}\|\Phi^*L\|_{K_t^o} \\
&= \mathbb{E}\|\Phi^*\beta g_{x^{\perp}}\|_{K_t^o} \\
&= \beta\mathbb{E}\|\Phi^* g_{x^{\perp}}\|_{K_t^o} .
\end{align*}

We need to extend the support of distribution of $g_{x^{\perp}}$ and consequently $L$ from $x^{\perp}$ to $\mathbb{R}^n$. This is done by the following claim in \cite{plan2014high}:

\begin{claim}
Let $g_E$ be a random vector which is distributed as $\mathcal{N}(0,I_E)$. Also, assum that $\Gamma : \mathbb{R}^n\rightarrow\mathbb{R}$ is a convex function. Then, for any subspace $E$ of $\mathbb{R}^n$ such that $E\subseteq F$, we have:
$$\mathbb{E}(\Gamma(g_E))\leq\mathbb{E}(\Gamma(g_F)) .$$
\end{claim}

Hence, we can orthogonally decompose $\mathbb{R}^n$ as $\mathbb{R}^n=D\oplus C$ where $D$ is a subspace supporting $x^{\perp}$ and $C$ is the orthogonal subspace onto it. 
Thus, $g_{\mathbb{R}^n}=g_D + g_C$ in distribution such that $g_D\sim\mathcal{N}(0,I_D), \ g_C\sim\mathcal{N}(0,I_C)$. Also, $\|.\|_{K_t^o}$ is a convex function since it is a semi-norm. Hence,
\begin{align*}
\mathbb{E}_D\|\Phi^*g_D\|_{K_t^o} &= \mathbb{E}_D\|\Phi^*g_D+\mathbb{E}_C(g_C)\|_{K_t^o} \\
&=\mathbb{E}_D\|\mathbb{E}_{C|D}(\Phi^*g_D+g_c)\|_{K_t^o} \\
&\leq\mathbb{E}_D\mathbb{E}_{C|D}\|\Phi^*(g_D+g_C)\|_{K_t^o} \\ 
&= \mathbb{E}\|\Phi^*g_{\mathbb{R}^n}\|_{K_t^o} .
\end{align*}

The first inequality follows from Jensen's inequality, while the second inequality follows from the law of iterated expectation. Therefore, we get:
\begin{align*}
\mathbb{E}\|\Phi^*L\|_{K_t^o} &= \mathbb{E}\|\Phi^*\beta g_{x^{\perp}}\|_{K_t^o} \\
&=\beta\mathbb{E}\|\Phi^* g_{x^{\perp}} \|_{K_t^o} \\
&\leq \beta \mathbb{E}\|\Phi^*g_{\mathbb{R}^n}\|_{K_t^o}\\
&= \beta\sup_{u\in (K-K)\cap tB_n^2} \langle\Phi^*\beta g_{\mathbb{R}^n}, u\rangle \\
&= \beta W_t(K) .
\end{align*}

The last equality follows from the fact that $\Phi^*g_{\mathbb{R}^n}\thicksim\mathcal{N}(0,I)$. The final step is to take an expectation with respect to $y_i$, giving us a bound on $\mathbb{E}(S_3)$: 
\begin{align*}
\mathbb{E}(S_3) &= \mathbb{E}\|\Phi^*L\|_{K_t^o} \\
&\leq\mathbb{E}(\beta) W_t(K) \\
& \leq\sqrt{\mathbb{E}(\beta^2)} W_t(K) \, ,
\end{align*}
where $\beta^2 = \frac{1}{m^2}\sum_{i=1}^m y_i^2$. Hence,
\begin{equation}\label{eq 5.5}
\mathbb{E}(S_3) \leq \frac{\eta}{\sqrt{m}}W_t(K) \, .
\end{equation}

Putting together the results from \eqref{eq 5.3}, \eqref{eq 5.4}, and \eqref{eq 5.5}, we have:
\begin{align*}
\mathbb{E}(\|\Phi^*\widehat{x}_{\lin}-\mu\alpha\bar{w}\|_{K_t})&\leq \mathbb{E}(S_1)+\mathbb{E}(S_2)+\mathbb{E}(S_3) \\
&\leq \sqrt{\mathbb{E}(S_1)}+\mathbb{E}(S_2)+\mathbb{E}(S_3)\\ 
&\leq \frac{2\alpha t\sigma}{\sqrt{m}}(1+\varepsilon) + 2\mu\alpha t\varepsilon + \frac{\eta}{\sqrt{m}}W_t(K) .
\end{align*}

Therefore, we obtain:
\begin{align}\label{1MT}
 \mathbb{E}\|\widehat{w}-\mu\alpha\bar{w}\|\leq t +\frac{2}{t}\mathbb{E}(\|\Phi^*\widehat{x}_{lin}-\mu\alpha\bar{w}\|_{K_t})\leq t + \frac{4\alpha \sigma}{\sqrt{m}}(1+\varepsilon) + 4\mu\alpha \varepsilon + \frac{2\eta}{t\sqrt{m}}W_t(K).
\end{align}

Moreover, we can bound $\alpha = \frac{1}{\|\Phi \bar{w} + \Psi \bar{z}\|}$ as follows:
\begin{align*}
\|\Phi \bar{w} + \Psi \bar{z}\|_2^2 &\geq \|\Phi \bar{w}\|_2^2 + \|\Psi \bar{z}\|_2^2 - 2|\langle\Phi\bar{w},\Psi{z}\rangle| \\
&\geq 2-2\varepsilon\nonumber,~~\text{or,}\\
\alpha&\leq\frac{1}{\sqrt{2}\sqrt{1-\varepsilon}}\nonumber
\end{align*}

By plugging $\alpha$ in~\eqref{1MT}, we obtain the desired result in Theorem~\ref{thm:main}. However, $K$ is a closed star-shaped set (the set of $s$-sparse signals), thus $W_t(K)=tW_1(K)$~\cite{plan2014high}. Now using~\eqref{eq}, we can conclude the Corollary~\ref{corr:estx} (bound on the estimation error for superposition signal):
\begin{align}
\mathbb{E}(\|\widehat{x} - \mu\bar{x}\|)\leq 2t + \frac{8\alpha \sigma}{\sqrt{m}}(1+ \varepsilon) + 8\mu\alpha \varepsilon + \frac{4\eta}{t\sqrt{m}}W_t(K) . \nonumber
\end{align}
We can use lemma $(2.3)$ in \cite{planRmanrobust} to bound $W_t(K)\leq C t\sqrt{s \log (2n/s)}$. Hence (Using bound on $\alpha$ and by letting $t\rightarrow 0$),
\begin{align}
\mathbb{E}\|\widehat{x} - \mu\bar{x}\|\leq \frac{4\sqrt{2}\sigma}{\sqrt{m}}\left(\frac{1+\varepsilon}{\sqrt{1-\varepsilon}}\right) + 4\sqrt{2}\mu \left(\frac{\varepsilon}{\sqrt{1-\varepsilon}}\right) + \frac{C\eta}{\sqrt{m}}\sqrt{s\log\frac{2n}{s}} \, ,
\end{align}
where $C> 0$ is an absolute constant. This completes the proof of Corollary~\ref{corr:estx}.
\end{proof}

As a precursor to the high probability version of the main theorem, we need a few more definitions and preliminary lemmas:
\begin{definition}(Sub-gaussian random variable.)\label{subgau}
A random variable $X$ is called sub-gaussian if it satisfies the following:
\begin{align*}
\mathbb{E}\exp\left(\frac{c X^2}{\|X\|_{\psi_2}^2}\right)\leq 2,
\end{align*}
where $c >0$ is an absolute constant and $\|X\|_{\psi_2}$ denotes the $\psi_2$-norm which is defined as follows:
\begin{align*}
\|X\|_{\psi_2}=\sup_{p\geq 1}\frac{1}{\sqrt{p}}(\mathbb{E}|X|^p)^{\frac{1}{p}}.
\end{align*}
\end{definition}

\begin{definition}(Sub-exponential random variable.)\label{subexp}
A random variable $X$ is sub-exponential if it satisfies the following relation:
\begin{align*}
\mathbb{E}\exp\left(\frac{c X}{\|X\|_{\psi_1}}\right)\leq 2,
\end{align*}
where $c >0$ is an absolute constant. Here, $\|X\|_{\psi_1}$ denotes the $\psi_1$-norm, defined as follows:
\begin{align*}
\|X\|_{\psi_1}=\sup_{p\geq 1}\frac{1}{p}(\mathbb{E}|X|^p)^{\frac{1}{p}}.
\end{align*}
\end{definition}

We should mention that there are other definitions for sub-gaussian and sub-exponential random variables. Please see~\cite{vershynin2010introduction} for a detailed treatment.

\begin{lemma}\label{SubgaSubexp}
Let $X$ and $Y$ be two sub-gaussian random variables. Then, $XY$ is a sub-exponential random variable.
\end{lemma}

\begin{proof}
According to the definition of the $\psi_2$-norm, we have:
\begin{align}
(\mathbb{E}|XY|^p)^{\frac{1}{p}}=\left(\mathbb{E}|X|^p|Y|^p\right)^{\frac{1}{p}}\leq\left(\left(\mathbb{E}|X|^{2p}\right)^{\frac{1}{2p}}\left(\mathbb{E}|Y|^{2p}\right)^{\frac{1}{2p}}\right)\leq \sqrt{2}p\|X\|_{\psi_2}\|Y\|_{\psi_2},
\end{align}
where the first inequality results from Cauchy-schwarz inequality, and the last inequality is followed by the sub-gaussian assumption on $X$ and $Y$. This shows that the random variable $XY$ is sub-exponential random variable according to Definition \ref{subexp}.
\end{proof}
 
\begin{lemma}(Gaussian concentration inequality) See \cite{vershynin2010introduction}.\label{GaussConcen}
Let $(G_x)_{x\in T}$ be a centered gaussian process indexed by a finite set $T$. Then $\forall t>0$:
\begin{align*}
\mathbb{P}(\sup_{x\in T}G_x\geq \mathbb{E}\sup_{x\in T}G_x+t))\leq \exp\left(-\frac{t^2}{2\sigma^2}\right),
\end{align*} 
where $\sigma^2 = \sup_{x\in T}\mathbb{E}G_x^2 < \infty$.
\end{lemma} 
 
\begin{lemma}(Bernstein-type inequality for random variables)~\cite{vershynin2010introduction}.\label{bern}
Let $X_1,X_2,\ldots,X_n$ be independent sub-exponential random variables with zero-mean. Also, assume that $K = \max_{i}\|X_i\|_{\psi_1}$. Then, for any vector $a\in\mathbb{R}^n$ and every $t\geq 0$, we have:
\begin{align*}
\mathbb{P}(|\Sigma_{i}a_iX_i|\geq t)\leq 2\exp\left(-c\min\left\{\frac{t^2}{K^2\|a\|_2^2},\frac{t}{K\|a\|_\infty}\right\}\right).
\end{align*} 
where $c>0$ is an absolute constant.
\end{lemma}

\begin{proof}[Proof of Theorem~\ref{thm:high}]
We follow the proof given in~\cite{plan2014high}. Let $\beta=\frac{s}{2\sqrt{m}}$ for $0<s<\sqrt{m}$ where $m$ denotes the number of measurements. In \eqref{eq}, we saw that 
\begin{align}\label{mainprob}
\|\widehat{x} - \mu\bar{x}\|\leq 2(t+\frac{2}{t}(S_1+S_2+S_3))
\end{align}
We attempt to bound the tail probability separately for each term $S_1, S_2,$ and $S_3$ and then use a union bound to obtain the desired result.

For $S_1$, we have:
\begin{align*}\label{mainprob1}
S_1\leq\lvert\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle-\mu) \rvert\|\Phi^*\bar{x}\|_{K_t^o} .
\end{align*}
We note that $y_i$ is a sub-gaussian random variable (by assumption) and $\langle a_i, \bar{x}\rangle$ is a standard normal random variable. Hence, by Lemma~\ref{SubgaSubexp}, $y_i\langle a_i, \bar{x}\rangle$ is a sub-exponential random variable. Also, $y_i\langle a_i, \bar{x}\rangle$ for $i=1,2,\ldots,m$ are independent sub-exponential random variables that can be centered by subtracting their mean $\mu$. Now, we can apply Lemma~\ref{bern} on $\lvert\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle-\mu)\rvert$. Therefore, We have:
\begin{align*}
\mathbb{P}(\lvert\frac{1}{m}\Sigma_i(y_i\langle a_i, \bar{x}\rangle-\mu))\rvert\geq \eta\beta)\leq2\exp\left(-\frac{c\beta^2\eta^2m}{\|y_1\|_{\psi_2}^2}\right) .
\end{align*}
Here, $\eta$ and $\mu$ are as defined in~\ref{lemma 4.1}. Using the bound on $\|\Phi^*\bar{x}\|_{K_t^o}$, we have:
\begin{align}
S_1\leq\sqrt{2}\eta\beta t\frac{1+\varepsilon}{\sqrt{1-\varepsilon}} ,
\end{align}
with probability at least $1-2\exp(-\frac{c\beta^2\eta^2m}{\|y_1\|_{\psi_2}^2})$ where $c>0$ is some constant.

For $S_2$ we have:
\begin{align}\label{mainprob2}
S_2\leq \sqrt{2}\mu\alpha t\frac{\varepsilon}{\sqrt{1-\varepsilon}} ,
\end{align}
with probability $1$ since $S_2$ is a deterministic quantity. 

For $S_3$ we have:
\begin{align*}
S_3\leq\|\Phi^*\frac{1}{m}\Sigma_i y_i b_i\|_{K_t^o} .
\end{align*}
To obtain a tail bound for $S_3$, We are using the following:
\begin{align*}
S_3\leq\frac{1}{m}(\Sigma_{i}y_i^2)^{1/2}\|\Phi^*g\|_{K_t^o}
\end{align*}
We need to invoke the Bernstein Inequality (Lemma~\ref{bern}) for sub-exponential random variables $(y_i^2-\eta^2)$ for $i=1,2,\ldots,m$ which are zero mean subexponential random variables in order to bound $\frac{1}{m}(\Sigma_{i}y_i^2)^{1/2}$. we have:
\begin{align*}
\Big|\frac{1}{m}\Sigma_i(y_i^2-\eta^2)\Big|\leq 3\eta^2
\end{align*}
with high probability $1- 2\exp(-\frac{cm\eta^4}{\|y_1\|_{\psi_2}^4})$.

Next, we upper-bound $\|\Phi^*g\|$ (where $g\sim\mathcal{N}(0,I)$) with high probability. Since $\Phi$ is an orthogonal matrix, we have that $\Phi^*g\sim\mathcal{N}(0,I)$. Hence, we can use the Gaussian concentration inequality to bound $\Phi^*g$ as mentioned in Lemma~\ref{GaussConcen}. Putting these pieces together, we have:
\begin{align}\label{mainprob3}
S_3\leq\frac{2\eta}{\sqrt{m}}\left(W_t(K) + t\beta\sqrt{m}\right) ,
\end{align}     
with probability at least $1- 2\exp(-\frac{cm\eta^4}{\|y_1\|_{\psi_2}^4})-\exp(c\beta^2m)$.
Here, $W_t(K)$ denotes the local mean width for the set $K_1$ defining in Section \ref{sec:Mathematicalsetup}.

Now, combining~\eqref{mainprob},~\eqref{mainprob1},~\eqref{mainprob2}, and~\eqref{mainprob3} together with the union bound, we obtain:
\begin{align}\label{mainprobfinal}
\|\widehat{x} - \mu\bar{x}\| &\leq \frac{2\sqrt{2}\eta s}{\sqrt{m}}\left(\frac{1+\varepsilon}{\sqrt{1-\varepsilon}}\right) \nonumber + 4\sqrt{2}\mu \left(\frac{\varepsilon}{\sqrt{1-\varepsilon}}\right) +
\frac{C\eta}{\sqrt{m}}\sqrt{s\log\frac{2n}{s}} + 4\frac{\eta s}{\sqrt{m}}.
\end{align}
with probability at least $1-4\exp(-\frac{cs^2\eta^4}{\|y_1\|_{\psi_2}^4})$ where $C, c> 0$ are absolute constants. Here, we have again used the well-known bound on the local mean width of the set of sparse vectors (for example, see Lemma 2.3 of \cite{planRmanrobust}). This completes the proof.
\end{proof}
