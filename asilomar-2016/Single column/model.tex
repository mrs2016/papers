\section{Preliminaries}
\label{sec:Mathematicalsetup}

In this section, we establish the formal mathematical model and introduce some definitions. Unless otherwise mentioned, the symbol $\| \cdot \|$ refers to the $\ell_2$-norm. Consider a signal $x\in \mathbb{R}^n$ that is the superposition of a pair of sparse vectors in different bases, i.e.,:
\begin{equation}\label{eq 3.2}
x = \Phi w + \Psi z \, ,
\end{equation}
where $\Phi, \Psi\in \mathbb{R}^{n\times n}$ are orthonormal bases, and $w, z\in \mathbb{R}^n$ such that $\|w\|_0\leq s_1$, and  $\|z\|_0\leq s_2$.  Consider the \emph{Gaussian} observation model:
\begin{equation}\label{eq 3.1}
y= f(Ax) \, ,
\end{equation}
where $A\in \mathbb{R}^{m\times n}$ is a random matrix with i.i.d.\ standard normal entries, and $f$ denotes a (possibly unknown) link function which is not necessarily smooth, invertible, or continuous. 
We define the following quantities:
\begin{align}\label{sp}
\bar{x} = \frac{\Phi \bar{w} + \Psi \bar{z}}{\|\Phi \bar{w} + \Psi \bar{z}\|} = \alpha(\Phi \bar{w} + \Psi \bar{z}) ,
\end{align}
where 
$\alpha = \frac{1}{\|\Phi \bar{w} + \Psi \bar{z}\|},~\bar{w} = \frac{w}{\|w\|},~\bar{z} = \frac{z}{\|z\|}.$
Also, we define the set of sparse vectors in the bases $\Phi$ and $\Psi$ as follows:
\begin{align*}
K_1 &= \{\Phi a\ | \ \|a\|_0\leq s_1\}, \\ 
K_2 &= \{\Psi a\ | \ \|a\|_0\leq s_2\},
\end{align*}
and we define $K = \{a\ | \ \|a\|_0\leq s\}.$

%\begin{definition}(Linear estimate of $x$).\label{def 3.1}
%The linear estimate of $x$ is given by $\hat{x}_{lin} = \frac{1}{m}A^{T}y = \frac{1}{m}\Sigma_{i = 1}^{m}y_i a_i$ where $a_i\in \mathbb{R}^n$ is the $i$\textsl{-th} row of $A$ and we have  $a_i\sim\mathcal{N}(0,I)$.
%\end{definition}

A fundamental assumption made by several demixing algorithms is that the sparsifying bases are sufficiently \emph{incoherent} with respect to each other. We quantify the incoherence assumption as follows:

\begin{definition}($\varepsilon$-incoherence).\label{def 3.2}
The set of $s$-sparse vectors in basis $\Phi$ and $\Psi$ are said to be $\varepsilon$-incoherent where:  
$$\varepsilon = \sup_{\substack{\|u\|_0\leq s,\ \|v\|_0\leq s  \\ \|u\|_2 = 1,\ \|v\|_2 = 1}}|\langle{\Phi u, \Psi v}\rangle|.$$
\end{definition}

We note that the parameter $\varepsilon$ is related to the more well-known \emph{mutual coherence} of a matrix. Indeed, if we consider the matrix $\Gamma = [\Phi \, \Psi]$, then the mutual coherence of $\Gamma$ is given by $\gamma = \max_{i \neq j} | (\Gamma^T \Gamma)_{ij} |$, and one can show 
that $\varepsilon \leq s \gamma$  \cite{foucart2013}.


We also introduce some concepts from high-dimensional geometry. First, we define a statistical measure of complexity of a set of signals, following the approach of~\cite{plan2014high}.

\begin{definition}(Local gaussian mean width).\label{def 3.3}
For a given set $K \in\mathbb{R}^n$, the local gaussian mean width (or simply, the local mean width) is defined as follows $\forall~t>0$:
$$W_t(K) = \mathbb{E}\sup_{x, y\in K, \|x-y\|\leq t}\langle g, x-y\rangle.$$
where $g\sim\mathcal{N}(0,I_{n \times n})$.
\end{definition}

Also following \cite{plan2014high}, we define the notion of a \emph{polar norm} with respect to a given subset $Q$ of the signal space:
\begin{definition}(Polar norm)
For a given $x\in \mathbb{R}^n$ and a subset of $Q\in \mathbb{R}^n$, the polar norm with respect to $Q$ is defined as follows:
$$\|x\|_{Q^o} = \sup_{u\in Q}\langle x, u\rangle.$$
\end{definition} 
Furthermore, for a given subset of $Q\in \mathbb{R}^n$, we define $Q_t = (Q-Q)\cap t B_2^n$. Since $Q_t$ is a symmetric set, one can show that the polar norm with respect to $Q_t$ defines a semi-norm. 


%By these definitions, we are ready to move on to our algorithm and main theorem.