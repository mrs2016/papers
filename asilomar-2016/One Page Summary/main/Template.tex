% Template for SSP-2016 paper; to be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{spconf,amsmath,epsfig}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{fullpage}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{epstopdf}
\usepackage{epsfig}
\usetikzlibrary{tikzmark,calc}
\usepackage{caption}
\usepackage{float}
\numberwithin{equation}{section} 
\newtheorem{theorem}{Theorem}[section]                   %[section]   %numberes automatically
\newtheorem{lemma}[theorem]{Lemma}         %[theorem]  in the middle
\newtheorem{corollary}[theorem]{Corollary}     %{corollary}{Corollary}[theorem]   
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{ass}[theorem]{Assumption}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{heuristic}[theorem]{Heuristic}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{exmp}[theorem]{Example} 
% Example definitions.
% --------------------
\def\x{{\mathbf x}}
\def\R{{\mathbb{R}}}
\def\L{{\cal L}}


\ninept
% Title.
% ------
\title{Demixing Sparse Signals from Nonlinear Observations}
%
% Single address.
% ---------------
\name{Mohammadreza Soltani, Chinmay Hegde}
\address{ECpE Department, Iowa State University, Ames, IA, 50010}
%
% For example:
% ------------
%\address{School\\
%	Department\\
%	Address}
%
% Two addresses (uncomment and modify for two-address case).
% ----------------------------------------------------------
%\twoauthors
%  {A. Author-one, B. Author-two\sthanks{Thanks to XYZ agency for funding.}}
%	{School A-B\\
%	Department A-B\\
%	Address A-B}
%  {C. Author-three, D. Author-four\sthanks{The fourth author performed the work
%	while at ...}}
%	{School C-D\\
%	Department C-D\\
%	Address C-D}
%
\begin{document}
%\ninept
%
\maketitle
%
%\begin{abstract}
%%Signal demixing is of special importance in several applications ranging from astronomy to computer vision. 
%The goal in demixing is to recover a set of signals from their linear superposition. In this paper, we study the more challenging scenario where only a limited number of nonlinear measurements of the signal superposition are available. Our contribution is a simple, fast algorithm that recovers the component signals from
%the nonlinear measurements. We support our algorithm with a rigorous theoretical analysis, 
%and provide simulation result to show the efficiency of our algorithm.
%%and provide upper bounds on the sample complexity of demixing the component signals (up to a scalar ambiguity). We also provide a range of simulation results, and observe that the method outperforms a previously existing algorithm based on convex relaxation.
%\end{abstract}
%

%\section{Introduction}
In several applications in signal processing, data analysis, and statistics, the problem of  \emph{demixing} assumes special importance. In the simplest case, the goal is to recover a pair of signals from their linear superposition. Mathematically, consider the observation model:
\begin{equation}
\nonumber
x = \Phi w + \Psi z \, ,
\end{equation}
where $x\in\mathbb{R}^n$ represents the observations, $\Phi, \Psi \in \R^{n \times n}$ are \emph{incoherent} orthonormal bases, and $w, z \in \mathbb{R}^n$ are the coefficients of the constituent signals.  The demixing problem involves reliably recovering $w$ and $z$ from the observations $x$. This problem has been studied in several applications; see \cite{mccoyTropp2014,mccoy2014convexity}. 

In this paper, we focus on a challenging scenario where the observations $y$ are \emph{nonlinear} functions of the signal superposition. Specifically, we consider the observation model:
$$y = f(Ax) \, ,$$
where $A\in\mathbb{R}^{m\times n}$ is a random matrix with $m \ll n$ and $x = \Phi w + \Psi z$. As part of our structural assumptions, we suppose that the signal coefficient vectors $w$ and $z$ are both $s$-sparse  (i.e., $w$ and $z$ contain no more than $s$ nonzero entries). Furthermore, $f$ may be non-smooth, non-invertible, or even unknown. Then, the goal is to recover an estimate of $w$ and $z$ (and therefore the superposition $x$), given nonlinear measurements $\{ y_i \}_{i=1}^m$, the measurement matrix $A$, and the bases $\Phi$ and $\Psi$.

In this paper, we provide a simple, \emph{non-iterative} algorithm (that we call \textsc{OneShot}) to demix the constituent signals $w$ and $z$, given the observations $y$ and the measurement operator $A$. Our algorithm is non-iterative, does not require explicit knowledge of the link function $f$, and works even in the highly under-determined case where $m \ll n$. %, provided the bases $\Phi$ and $\Psi$ are incoherent enough.
Please refer to the full (five-page) version of the paper for the algorithm description.

We support our algorithm with a rigorous theoretical analysis. Our analysis leads us to prove upper bounds for the \emph{sample complexity} of demixing with nonlinear observations. (Here, sample complexity refers to a sufficient number of observations for reliable recovery of $w,z,x$ modulo a scaling factor). Specifically, we prove that the estimation error incurred by the final output ($\widehat{x}$) of \textsc{OneShot} satisfies the upper bound:
\begin{align}
\mathbb{E}\|\widehat{x} - \mu\bar{x}\| &\leq \frac{4\sqrt{2}\sigma}{\sqrt{m}}\left(\frac{1+\varepsilon}{\sqrt{1-\varepsilon}}\right) \nonumber\\ 
&+4\sqrt{2}\mu \left(\frac{\varepsilon}{\sqrt{1-\varepsilon}}\right) +\frac{C\eta}{\sqrt{m}}\sqrt{s\log\frac{2n}{s}} .  \nonumber
\end{align}
%\end{theorem}
Here, $\bar{x}$ is a normalized version of $x$, and $\varepsilon$ is a parameter that controls the incoherence between the bases $\Phi$ and $\Psi$. The constants $\sigma, \eta, \mu$ depend on the nonlinear link function $f$. 
%For example, if $f(x) = \mathrm{sign}(x)$, then explicit calculations reveal that:
%$$\mu = \sqrt{\frac{2}{\pi}}\approx 0.8,~\sigma^2 = 1- \frac{2}{\pi}\approx 0.6,~\eta^2=1$$.

Our analysis of \textsc{OneShot} is based on a geometric argument that follows the pioneering approach of \cite{plan2014high}. Our contribution is to extend the work of~\cite{plan2014high} for the (more general) nonlinear demixing problem, and to characterize the role of incoherence and how it affects the estimation. %Our technique is based on a geometric argument. %, and leverages the \emph{Gaussian mean width} for the set of sparse vectors, which is a statistical measure of complexity of a set of points in a given space. %Due to page-limit constraints, we merely state our theoretical claims and some representative experiments, and 
%Please refer to the extended version for details.

Moreover, we provide numerical evidence for the efficiency of our method. In particular, we compare the performance of \textsc{OneShot} with a previous method proposed in~\cite{plan2014high} that was based on convex optimization. (We call this method \textsc{NlcdLASSO}.)
%The original idea of this method \cite{plan2014high} is not directly related to the nonlinear demixing problem, but it was proposed for nonlinear signal recovery when the underlying signal $x$ belongs to the set of approximately $s$-sparse signals. To make comparison with \textsc{Oneshot}, we use this idea in our framework.
Our results show that \textsc{OneShot} outperforms this convex method significantly in both demixing efficiency as well as running time, and consequently makes it an attractive choice in large-scale problems. Figure~\ref{figCos} displays a representative result. Suppose we set $f(x)=\text{sign(x)}$, $\Phi$ and $\Psi$ as the Haar wavelet and the noiselet bases respectively, and $w$ and $z$ as randomly chosen $s$-sparse vectors.  We vary $s$ and $m$, and estimate the underlying components (as well as their superposition) using the two methods. We use the \textit{Cosine Similarity} criterion between the original signal $x$ and the reconstructed superposition $\widehat{x}$ to compare algorithm performance. From Fig.\ \ref{figCos}, we observe the performance of \textsc{Oneshot} exceeds \textsc{NlcdLASSO} for any fixed choice of $m$ and $s$. 

\begin{figure}
\centering
\includegraphics[width=0.65\linewidth]{../Figs/all.eps}
\caption{Performance of \textsc{Oneshot} vs.\ \textsc{NlcdLASSO}. 
%for different choices of sparsity level $s$ and for different number of measurements m for $f(x) = sign(x)$ as nonlinear link function.
}
\label{figCos}
%
\end{figure}
%\section{Conclusions}
%We proposed a simple (and fast) algorithm for demixing sparse signals from nonlinear observations. We support our algorithm with a rigorous theoretical analysis, and provide upper bounds on the sample complexity of demixing the component signals (up to a scalar ambiguity).

%\newpage
{
\footnotesize
\bibliographystyle{IEEEbib}
\bibliography{../../Common/chinbiblio,../../Common/csbib,../../Common/mrsbiblo}
}

\end{document}
