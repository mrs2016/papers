\section{Algorithm and main theorem}
\label{sec::alg}

Having described the mathematical model for our demixing problem and the notations in the previous section, we are ready to state the optimization problem for recovery of the components $w$ and $z$ in model~\eqref{MainModell} and our iterative algorithm for solving this optimization problem along with the main theorem to support our algorithm.

\subsection{Optimization Problem} 

According to the notations introduced in section~\ref{sec::model}, we formulate our demixing problem as minimization of the loss function $F(t)$:

\begin{equation} \label{optprob}
\begin{aligned}
& \underset{t}{\text{min}}
\ \ F(t) = \frac{1}{m}\sum_{i=1}^m \Theta(a_i^T\Gamma t) - y_i a_i^T\Gamma t \\
& \text{subject to}  \quad \|t\|_0\leq 2s.
\end{aligned}
\end{equation}

In problem~\eqref{optprob}, once the solution (constituent vector $t$) was found, the components $w$ and $z$ are followed by selecting the first half entries of $t$ as signal $w$ and the second half as $z$. We note that the loss function $F(t)$ in problem~\eqref{optprob} is not squared loss function which is mostly used in statistics and signal processing when the link function, $g(t)$ is linear function. Function $F(t)$ is made by the integral of the link function and a linear term. By assumption on $g$ ($0<l_1\leq\ g^{\prime}(x)$), $F(t)$ would be a convex function. This kind of loss functions are typically used in GLM and SIM in statistics~\cite{negahban2011unified}. Also, we can justify the use of the objective function in~\eqref{optprob} according to the nonlinear model in~\eqref{MainModell} by noting that $\mathbb{E}(e_i|a_i)= 0$ where $e_i$  denotes the additive noise corresponding to measurement $i$. Hence, model~\eqref{MainModell} implies that $\mathbb{E}(y_i|a_i) = g(a_i^T\Gamma t)$ which is the typical assumption in SIM. But this satisfies the optimal solution of the population version of problem~\eqref{optprob},   $i.e.$, $\min_t \  \mathbb{E}(\Theta(a_i^T\Gamma t) - y_i a_i^T\Gamma t)$~\cite{ganti2015learning}. This shows that using objective function in~\eqref{optprob} is meaningful according to the fundamental assumption, $\mathbb{E}(y_i|a_i) = g(a_i^T\Gamma t)$, in GLM and SIM in statistics. 

%\begin{algorithm}
%\label{alg:oneshot}
%\caption{\textsc{OneShot}}
%\textbf{Inputs:} Basis matrices $\Phi$ and $\Psi$, measurement matrix $A$, measurements $y$, sparsity level $s$. \\
%\textbf{Outputs:} Estimates  $\widehat{x}=\Phi\widehat{w} + \Psi\widehat{z}$, $\widehat{w}\in K_1$, $\widehat{z}\in K_2$
%
%\setlength{\parskip}{1em}
%$\widehat{x}_\text{lin}\leftarrow\frac{1}{m}A^{T}y$\qquad\{form linear estimator\}\\
%$b_1\leftarrow\Phi^*\widehat{x}_{lin}$\qquad~~\{forming first proxy\}\\
%$\widehat{w}\leftarrow\mathcal{P}_s(b_1)$\qquad~~~\{Projection on set $K_1$\}\\
%$b_2\leftarrow\Psi^*\widehat{x}_{lin}$\qquad~~\{forming second proxy\}\\
%$\widehat{z}\leftarrow\mathcal{P}_s(b_2)$\qquad~~~~\{Projection on set $K_2$\}\\
%$\widehat{x}\leftarrow\Phi\widehat{w} + \Psi\widehat{z}$\quad~~~\{Estimating $\widehat{x}$\}
%\end{algorithm}

\subsection{\textsc{Hard Thresholding Multishot (HTM)}}

Now, we state the iterative algorithm for solving~\eqref{optprob} which we call it \textsc{Hard Thresholding Multishot (HTM)}. This algorithm is given in Algorithm \ref{alg:HTM}.

\begin{algorithm}[t]
\caption{\textsc{HTM}}
\begin{algorithmic}
\label{alg:HTM}
\STATE \textbf{Inputs:} Basis matrices $\Phi$ and $\Psi$, measurement matrix $A$, link function, $g(x)$, measurements $y$, sparsity level $s$, step size, $\eta'$. 
\STATE \textbf{Outputs:} Estimates  $\widehat{x}=\Phi\widehat{w} + \Psi\widehat{z}$, $\widehat{w}\in K_1$, $\widehat{z}\in K_2$
\STATE\textbf{Initialization:}\\
$\left(x^0, w^0, z^0\right)\leftarrow\textsc{random initialization}$\\
%or,\\
%$\left(x^0, w^0, z^0\right)\leftarrow\textsc{OneShot}(\Phi,\Psi,A,y,s)$\\
$k =0$
\WHILE{$k\leq N$}
\STATE $t^k \leftarrow [ w^k ; z^k ]$~~~~~~\qquad\qquad\{Forming constituent vector\}
\STATE $t_1^k\leftarrow\frac{1}{m}\Phi^TA^T(g(Ax^k) - y)$ 
\STATE$t_2^k\leftarrow\frac{1}{m}\Psi^TA^T(g(Ax^k) - y)$\\
\STATE$\nabla F^k \leftarrow[ t_1^k ; t_2^k ]$
\qquad~~~~~~~~~\{Forming Gradient\}\\
\STATE${\tilde{t}}^k = t^k - \eta'\nabla F^k$
\qquad~~~~\{Gradient Update\}\\
\STATE$[ w^k ; z^k ]\leftarrow\mathcal{P}_{s;s}\left(\tilde{t}^k\right)$  
\qquad~~~\{Projection on sets $K_1$ and $K_2$\}\\
\STATE$x^k\leftarrow\Phi w^k + \Psi z^k$\quad~~~~~~~\{Estimating $\widehat{x}$\}
\STATE$k\leftarrow k+1$
\ENDWHILE
\STATE\textbf{Return:} $\left(\widehat{x}, \widehat{w}, \widehat{z}\right)\leftarrow \left(x^N,  w^N, z^N\right)$
\end{algorithmic}
\end{algorithm}

In Algorithm \ref{alg:HTM}, the gradient of the objective function is given by:
\begin{align}\label{GradientofOb}
\nabla F(t) = \frac{1}{m}\sum_{i=1}^{m}\Gamma^T a_i g(a_i^T\Gamma t) - y_i\Gamma^Ta_i.
\end{align}

Also, we assume that both components $w$ and $z$ are $s$-sparse in the basis $\Phi$ and $\Psi$, respectively. One can easily consider two different level of sparsity for both of these two components. In addition, in Algorithm \ref{alg:HTM}, $\mathcal{P}_{s;s}$ denotes the projection of vector $\tilde{t}^k\in\mathbb{R}^{2n}$ on the sets $K_1$ and $K_2$ which is implemented through simple hard thresholding by maintaining of the $2s$ largest entries of constituent vector, $t$ and set the others to zero.

At a high level, \textsc{HTM} first initializes the constituent signals, $w, z$ and superposition signal $x$. Then, it constructs the constituent vector, $t$ and forming the gradient step using~\eqref{GradientofOb}. Next, it updates the current estimate according to the gradient update being determined in Algorithm \ref{alg:HTM}. Then, it projects the updated estimation on sets $K_1$ and $K_2$ using projection operator $\mathcal{P}_{s;s}$ to obtain the new estimation of components $w$ and $z$. Finally, it estimates the superposition signal $x$ and repeat this procedure again until stopping criterion is met (see section~\ref{sec::result} for stopping criterion and the other details in implementation).

\subsection{Main theorem}
In this sub-section, we provide our main theorem supporting the convergence analysis of \textsc{HTM}. Please see section~\ref{sec::proof} for all proofs. In particular, we derive an upper bound on the estimation error of the constituent signals, $w,z$ and the constituent vector, $t$.

Before stating the main theorem, we should mention that the main theorem heavily depends on the fact that the objective function, $F(t)$ in~\eqref{optprob} satisfy Restricted Strong Convexity/Smoothness (RSC/RSS) conditions. This helps us to prove the linear convergence of \textsc{HTM}. Here, we briefly express the RSC/RSS conditions and refer to section~\ref{sec::proof} and~\cite{negahban2011unified} for more detailed discussion and related proofs.

\begin{definition}(RSC/RSS)
A function $f$ satisfies RSC and \textit{RSS} conditions if
\begin{align*}
\frac{m_{4s}}{2}\|t_2-t_1\|^2\leq f(t_2) - f(t_1) - \langle\nabla f(t_1) , t_2-t_1\rangle\leq\frac{M_{4s}}{2}\|t_2-t_1\|^2,
\end{align*}
where $\|t_i\|_0\leq 4s$ for $i=1,2$ and $m_{4s}$, and $M_{4s}$ are called RSC and RSS constants, respectively.
\end{definition}

\begin{theorem}(Main theorem)
\label{mainThConvergence}
Let $y\in\mathbb{R}^m$ be given nonlinear measurements according to model~\eqref{MainModell}. Also, assume that $A\in\mathbb{R}^{m\times n}$ be a measurement matrix which its rows are isotropic random vector which independently drawn from a known distribution and we have $\big{\|}a_i^T\Gamma_{\xi}\big{\|}_{\infty}\leq\vartheta$, $i=1\dots m$ and $\vartheta$ denotes the mutual coherence between $A$ and $\Gamma_{\xi}$. Also, let $\Phi, \Psi$ be two  $\varepsilon$-incoherence bases and $g$ be a known link function which is a smooth function such that $0<l_1\leq g^{\prime}\leq l_2$ for some $l_2\geq l_1 >0$. Furthermore, let $e_i$ denote the additive subgaussian noise corresponding to the measurement $i$ with $\|e_i\|_{\psi_2}\leq\tau$ for $i=1\dots m$. If we use \textsc{HTM} to solve optimization problem in~\eqref{optprob}, and consequently recover constituent vector, $t$ with step size $\frac{0.5}{M_J}<\eta^{\prime}<\frac{1.5}{m_J}$ such that $1\leq\frac{M_J}{m_J}\leq\frac{2}{\sqrt{3}}$ ($M_J$ and $m_J$ denote the RSC and RSS constants on set $J$ with $\|J\|_0\leq 6s$), then \textsc{HTM} outputs $w^N\in K_1, z^N\in K_2$ in no more than $N = \mathcal{O}(\log\frac{\mathbb{E}\|t^0-t^*\|_2}{\kappa})$ such that the error estimation of the constituent vector satisfies the following upper bound for any $k\geq 1$:
\begin{align}
\mathbb{E}\|t^{k+1} - t^*\|_2\leq\left(2q\right)^k\mathbb{E}\|t^0-t^*\|_2 + C\tau\sqrt{\frac{s}{m}}, 
\end{align}
where $q =\sqrt{1+{\eta^{\prime}}^2M_J^2-2\eta^{\prime} m_J}$, $C>0$ is a constant that only depends on the step size, $\eta^{\prime}$ and $q$. Also, $\kappa$ denotes the desired accuracy for solving optimization problem~\ref{optprob}, $s$ denotes the sparsity of the constituent signals, $t^*$ denotes the solution of problem~\eqref{optprob}, and $t^0$ represents the initial value of the constituent vector. 
\end{theorem}

\begin{theorem}(Sample complexity without any specific distribution on $A$)
\label{SampleComplexityNonormal}
Under the assumptions in Theorem~\ref{mainThConvergence},  the sample complexity or the required number of measurements to reliably recover constituent signals $w$ and $z$ is given by $m=\mathcal{O}(s\log n\log^2s\log(s\log n))$ with the optimization error equals to $\mathcal{O}(1)$ provided that two bases are incoherent enough. Here, $n$ represents the ambient dimension of the constituent signals.
\end{theorem}

\begin{theorem}(Sample complexity with specific distribution on $A$)
\label{SampleComplexitysubg}
Assume that all assumptions and definitions in Theorem~\ref{mainThConvergence} holds except that the measurement matrix $A$ is now more structured. That is, the rows of matrix $A$ are independent subgaussian isotropic random vectors. Then sample complexity or the required number of measurements to reliably recover constituent signals $w$ and $z$ is given by $m=\mathcal{O}(s\log\frac{n}{s})$ with the optimization error equals to $\mathcal{O}(1)$ provided that two bases are incoherent enough. Here, $n$ represents the ambient dimension of the constituent signals.
\end{theorem}

\new{\textbf{I am not sure to mention theorem \ref{mainThConvergenceNormal} and corollary \ref{SampleComplexityNormal}} 
\begin{theorem}(Linear convergence of \textsc{HTM} for standard normal measurement matrix)
\label{mainThConvergenceNormal}
Assume that all assumptions and definitions in Theorem~\ref{mainThConvergence} holds except that the measurement matrix $A$ is now more structured. That is, the entries of matrix $A$ are independent standard normal random variables. Then, \textsc{HTM} outputs $w^N\in K_1, z^N\in K_2$ in no more than $N = \left\lceil{\log\frac{\mathcal{O}(\varepsilon)+\sqrt{\frac{s\log\frac{n}{s}}{m} }}{\epsilon}}\right\rceil$ such that the error estimation of the constituent vector satisfies the following upper bound:
\begin{align*}
\mathbb{E}\|t^{k+1} - t^*\|_2\leq\left(2q\right)^k\left(\mathcal{O}(\varepsilon)+\sqrt{\frac{s\log\frac{n}{s}}{m}}\right) + C\tau\sqrt{\frac{s}{m}},
\end{align*}
\end{theorem}
\begin{corollary}(Sample complexity for standard normal measurement matrix)
\label{SampleComplexityNormal}
Under the assumptions in Theorem~\ref{mainThConvergenceNormal}, the sample complexity or the required number of measurements to reliably recover constituent signals $w$ and $z$ is given by $m=\mathcal{O}(s\log\frac{n}{s})$ with the optimization error which is given by $\mathcal{O}(\epsilon)$. Here, $n$represents the ambient dimension of the constituent signals.
\end{corollary}
}

\begin{remark}
In theorem~\ref{mainThConvergence}, 
%and~\ref{mainThConvergenceNormal}, 
we express the error bound based on constituent vector, $t$. It is easy to directly state these results in terms of components $w$ and $z$ using the following:
\begin{align*}
\max\{\|w^0-w^*\|_2,\|z^0-z^*\|_2\}\leq\|t^0-t^*\|_2\leq\|w^0-w^*\| + \|z^0-z^*\|_2,
\end{align*}
see section~\ref{sec::proof} for more details.
\end{remark}

\begin{remark}
In theorem~\ref{mainThConvergence},
%and~\ref{mainThConvergenceNormal}, 
we mentioned the expected value of the estimation error of the constituent vector, $t$. It is also possible to state these results in terms of tail probability. 
\end{remark}




