\section{Mathematical Model and Preliminaries}
\label{sec::model}

In this section, we state our mathematical model, notations and some definitions that we use in this paper. 
The underlying assumption for all demixing algorithm is that the bases in which the constituent signals are expressed are sufficiently \textit{incoherent} with respect to each other. This notion of incoherence is quantitatively expressed in the following definition:
\begin{definition}($\varepsilon$-incoherence).\label{incoherence}
The bases $\Phi$ and $\Psi$ are said to be $\varepsilon$-incoherent if:  
$$\varepsilon = \sup_{\substack{\|u\|_0\leq s,\ \|v\|_0\leq s  \\ \|u\|_2 = 1,\ \|v\|_2 = 1}}|\langle{\Phi u, \Psi v}\rangle|.$$
\end{definition}

We mention that the parameter $\varepsilon$ is related to the more well-known \emph{mutual coherence} of a matrix. Indeed, if we consider the matrix $\Gamma = [\Phi \, \Psi]$, then the mutual coherence of $\Gamma$ is given by $\gamma = \max_{i \neq j} | (\Gamma^T \Gamma)_{ij} |$, and one can show that $\varepsilon \leq s \gamma$  \cite{foucart2013}. In addition to have some incoherence between bases, we also need that the measurement matrix, $A$ and the dictionary, $\Gamma$ to be incoherent enough. In other words, in order that the recovery of the constituent signals is possible, one needs to require that the rows of $A$ and the columns of $\Gamma$ to be dissimilar or incoherent enough. This notion of incoherence was originally introduced in compressive sensing literature~\cite{candes2007sparsity}. Now, we give the formal definition of this incoherence:

\begin{definition}(mutual coherence between the measurement matrix and the dictionary)\label{mutualcoherence}
The mutual coherence between the measurement matrix $A$ and the dictionary $\Gamma=[\Phi \ \Psi]$ is defined as follows:
\begin{align}
\vartheta = \max_{i,j}\frac{a_i^T\Gamma_j}{\|a_i\|_2},
\end{align}
where $a_i$ and $\Gamma_j$ denote the $i$-th row of  the measurement matrix $A$ and the $j$-th column of the dictionary $\Gamma$, respectively. Since the matrix $\Gamma$ is the concatenation of the two orthonormal bases. its columns have unit $\ell_2$ norm.   
\end{definition}

Throughout this paper, $\|.\|_2$ denotes the $\ell_2$-norm of a vector in $\mathbb{R}^n$, and $\|A\|$ means the spectral norm of the matrix $A\in\mathbb{R}^{m\times n}$. Also, $\|.\|_0$ which is called $\ell_0$- norm counts the the number of nonzero entries in a vector given in $\mathbb{R}^n$. As mentioned, we use $\Gamma = \left[\Phi \ \Psi\right]_{n\times 2n}$ as the concatenation of the two bases and we call it dictionary. We define the constituent vector as $\  t =  [w \ z ]^T\in\mathbb{R}^{2n}$ which is a stacked vector of two components, $w,z$. Also, we set $\Theta(x) = \int_{-\infty}^{x} g(t)dt$ with $g(x)$ as the link function in our main model~\eqref{MainModell}. Finally, we assume the following set of $s$ sparse vectors in bases $\Phi$ and $\Psi$:
\begin{align*}
K_1 &= \{\Phi a\ | \ \|a\|_0\leq s_1\}, \\ 
K_2 &= \{\Psi a\ | \ \|a\|_0\leq s_2\},
\end{align*}

Now we state our model. Consider the nonlinear observation model as follows:
\begin{align}\label{MainModell}
y_i = g(a_i^Tx) + e_i, \ i=1\dots m,
\end{align}
where $x\in\mathbb{R}^n$ is the superposition signal and is given by $x = \Phi w+\Psi z$. Here, matrices $\Phi, \Psi\in\mathbb{R}^{n\times n}$ are two \textit{incoherent} orthonormal bases,  and $w, z\in\mathbb{R}^n$ denote the constituent $s$-sparse signals (signals with at most $s$ nonzero entries) which we are looking  to recover. In this model, we assume that the observation $y_i$ is corrupted by a subgaussian additive noise with $\|e_i\|_{\psi_2}\leq\tau$ for $i=1\dots m$. We also assume that the additive noise has zero mean conditioning on $a_i$, $i.e.$, $\mathbb{E}\left(e_i|a_i\right) = 0$, for $i=1\dots m$. Moreover, $g$ called \textit{link} function (which is known) represents a nonlinear smooth monotonically increasing function. We make the following, crucial assumption on the link function:

\begin{center}
\textit{"The derivative of the link function is strictly bounded either within a positive interval, or within a negative interval"}
\end{center}

Mathematically, this means that $\exists~l_1, l_2 >0 \ (l_1,l_2<0)\ s.t. \ 0<l_1\leq\ g^{\prime}(x)\leq l_2 \ (l_1\leq\ g^{\prime}(x)\leq l_2<0)$. From now, we just focus on the case $0<l_1\leq\ g^{\prime}(x)\leq l_2$ and give all the analysis just for this case. The analysis of the other case is similar. 

The lower bound in $0<l_1\leq\ g^{\prime}(x)\leq l_2$ guarantees that the function $g$ is strictly increasing function, $i.e.$, $if \ x_1 < x_2, \ then \ g(x_1)<g(x_2)$ and the upper bound states that the function $g$ is \textit{Lipschitz} with constant $l_2$. As we will see in the next section, the lower bound assumption makes the objective function convex (concave). Both of these assumptions are common in the literature of nonlinear recovery problems in signal processing and in statistics~\cite{negahban2011unified, yang2015sparse}. Also, bounding $g$ from below by a strictly positive number is an underlying assumption in the proof of Restricted Strong Convexity (RSC) (see section~\ref{sec::proof}) of our objective function.
It should be mentioned that without the lower bound assumption on $g$ ($0<l_1\leq g'(x)$), the proposed algorithm (see section~\ref{sec::alg}) is still working, but our analysis here is for simpler case where this assumption holds and we leave the analysis of the more challenging case (without this assumption) for future research.

In addition, in model~\eqref{MainModell}, $a_i$ denotes the $i$-th row of the random measurement matrix $A\in\mathbb{R}^{m\times n}$. In the context of model~\eqref{MainModell}, we consider two cases for the measurement matrix $A$; first, its rows are isotropic random vectors which are independently drawn from a known distribution, and the measurement matrix $A$ is enough incoherent with the bases, $\Phi$ and $\Psi$. More precisely, let $\Gamma = [\Phi \ \Psi]$ denotes the dictionary, then with this assumption, we have $\big{\|}a_i^T\Gamma_{\xi}\big{\|}_{\infty}\leq\vartheta$ for $i=1\dots m$ where $\vartheta $ denotes the mutual coherence between the measurement matrix and the dictionary (see definition~\ref{mutualcoherence}) and $\Gamma_{\xi}$ denotes the restriction of the columns of the dictionary to set $\xi$ with $\|\xi\|_0\leq 4s$. The second case, which we assume that the rows of matrix $A$ are independent subgaussian isotropic random vectors. 



