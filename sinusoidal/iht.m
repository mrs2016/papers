function [x_hat,err_trace]  = iht(y,A,AT,s,n,eta,tol,iter,opts)
log = opts.verbose;
err = 1;
x_est = zeros(n,1);
iterCnt = 0;

while ((err>tol) && (iterCnt<=iter))
    iterCnt = iterCnt + 1;
    err_trace(iterCnt) = err;
    
    x_prev = x_est;
    resid = A(x_prev) - y;
    x_update = x_prev - eta*AT(resid);
    x_est = thresh(x_update,s);
    x_hat = x_est;
    
    err_temp = norm(resid)/norm(y);
    
    if log == 1
        fprintf('Iteration = %d,  Relative error = %f,  Norm = %f\n',iterCnt, err_temp, norm(x_hat));
    end
    if err_temp <= tol
        if log == 1
            fprintf('Terminating...\n')
        end
        break;
    end
    
    err = err_temp;
end