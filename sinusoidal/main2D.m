
clc;clear;
close all
addpath Utils/
%%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = 2^18;                % Ambient diemnsions of the signal
s = 2000;                % Sparsity of the signal
sigma = 0.05;   %0:.02:0.25;  % Variance of the additive noise
kvec = 6;             % The number of diagonal blocks (An integer number)
q = 8*s;                 % Each diagonal block is a q*q matrix,  m = k . q
opts.link = 'com_exp';
opts.meas = 'fourier';
opts.basis = 'haar';
opts.cs_alg = 'cosampDIC';
opts.verbose = 0;   % = 1: print the errors,    = 0: no print for DMF and GHT
imageinput = 'cameraman.jpg';

switch opts.basis
    case 'id'
        Phi = @(w) w;
        PhiT = @(w) w;
    case 'haar'
        Phi =  @(w) haar2D(w, 'inverse');
        PhiT = @(w) haar2D(w, 'forward');
end

% link function
switch opts.link
    case 'com_exp'
        g = @(x) exp(1i*x);
        flg = 1;
    case 'sin'
        g = @(x) sin(x);
        flg = 0;
end

Im = imread(imageinput);
Im = imresize(Im,[sqrt(n) sqrt(n)],'bicubic');
Im = im2double(rgb2gray(Im));
wavelet_coeff = PhiT(Im(:));
w = thresh(wavelet_coeff,s);
x = Phi(w);  %Im(:);%

% measurement operator
switch opts.meas
    case 'gauss'
        Bmat = (1/sqrt(q))*randn(q,n);
        B = @(x) Bmat*x; BT = @(x) Bmat'*x;
    case 'fourier'
        zeta = sign(randn(n,1));
        p = randperm(n)';
        que = randperm(n/2-1)+1; % this keeps a random set of FFT coefficients
        omega = que(1:q/2)';
        B = @(z) A_f(zeta.*z, omega, p);
        BT = @(z) zeta.*At_f(z, n, omega, p);
end

x_hat = zeros(n,length(kvec));

for kk = 1:length(kvec)
    disp(kk);
    for sig = 1:length(sigma)
        D = [];
        for i = 1:kvec(kk)    %constructing random D
            temp = randn(q,1);
            % temp = ones(q,1)*randn(1,1);
            d = diag(temp);
            D = [D;d];
        end
        y1 = g(D*B(x)) + sigma(sig)*randn(size(D,1),1);
        
        %%%-------- DMF --- 1D Tone estimation -------------------------
        alpha = 1000; % Grid seatch resolution
        u1 = zeros(kvec(kk),1);
        v1 = zeros(kvec(kk),1);
        z_hatDMF = zeros(q,1);
        zz = B(x);
        
        z_min = -1.5;
        z_max = 1.5;
        
        for j =1:q
            u1 = y1(j:q:end);
            v1 = D(j:q:end,j);
            z_hatDMF(j) = recover_tone(u1,v1,g,z_min,z_max,alpha,flg);
        end
        %%%---------------- Sparse recovery ---------------------------
        switch opts.cs_alg
            case 'iht'
                tol = 0.01;
                iter = 300;
                eta = 0.2;
                [w_hatDMF,~] = iht(z_hatDMF,B,BT,svec(ss),n,eta,tol,iter,opts);
            case 'cosampDIC'
                opts.s = s;
                opts.selectAtom = 2;
                opts.tol=1e-4;
                opts.maxIter=50;
                opts.tol_early=0.005;
                opts.debias = 1;
                opts.hhs = 0;
                [w_hatDMF,~,~] = cosampDIC('sparsity', z_hatDMF, B, BT, Phi, PhiT, opts);
        end
    end
    x_hat(:,kk) = Phi(w_hatDMF);
end
% x_hat = Phi(w_hatDMF);

X = reshape(x,[],sqrt(n));
figure
imshow(X,[]);
for inde = 1:length(kvec)
    Xhat = reshape(x_hat(:,inde),[],sqrt(n));
    figure
    imshow(Xhat,[]);
    axisfortex('','','');
end

% Y = reshape(y1,[],sqrt(n));
% figure
% imshow(Y,[]);