function [z_hat] = recover_tone(y,v,g,omega_min,omega_max,alpha,flg)

v_min = min(v);
v_max = max(v);
T = v_max - v_min;

Omega = omega_min:2*pi/(alpha*T):omega_max;
l = length(Omega);
match_fil = zeros(1,l);

for i = 1:l
    phi_omega = g(v*Omega(i));
    
    if flg == 1
        match_fil(i) = abs(y'*phi_omega);
    else
        match_fil(i) = 2*y'*phi_omega- norm(phi_omega)^2;
    end
end
[~,in] = max(match_fil);

z_hat = Omega(in);