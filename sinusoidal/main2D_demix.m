%%%%% 2D nonlinear demixing algorithms
% specific to SQUARE astronomical images; need to modify for other sizes

clear
close all
clc
addpath Utils/

s1 = 1000;
s2 = s1;               % Sparsity of the signal
sigma = 0;           %0:.02:0.25;  % Variance of the additive noise
kvec = 3;             % The number of diagonal blocks (An integer number)
q = 16000;                 % Each diagonal block is a q*q matrix,  m = k . q

opts.basis = 'id_dct';
opts.sr_function = 'lin';
opts.link = 'sin';
opts.meas = 'fourier';


% basis operators
switch opts.basis
    case 'id_dct'
        Phi = @(w) w(:);
        PhiT = @(w) w(:);
        Psi = @(z) myvec(idct2(mymat(z)));
        PsiT = @(z) myvec(dct2(mymat(z)));
end

% function:  z = f(B(x))
switch opts.sr_function
    case 'sign'
        f = @(x) sign(x);
    case 'logit'
        f = @(x) 1./(1 + exp(-x))-.5;
    case 'lin'
        f = @(x) x;
end
% link function  y = g(Dz)
switch opts.link
    case 'com_exp'
        g = @(x) exp(1i*x);
        flg = 1;
    case 'sin'
        g = @(x) sin(x);
        flg = 0;
end
load astro_image
n = numel(im_superposition);
comp1_coeff = PhiT(im_sparse(:));
comp1_coeff = thresh(comp1_coeff,s1);
comp2_coeff = PsiT(im_smooth(:));
comp2_coeff = thresh(comp2_coeff,s2);

x = normc(Phi(comp1_coeff) + Psi(comp2_coeff));

switch opts.meas
    case 'gauss'
        Bmat = (1/sqrt(q))*randn(q,n);
        B = @(x) Bmat*x; BT = @(x) Bmat'*x;
    case 'fourier'
        zeta = sign(randn(n,1));
        p = randperm(n)';
        que = randperm(n/2-1)+1; % this keeps a random set of FFT coefficients
        omega = que(1:q/2)';
        B = @(z) A_f(zeta.*z, omega, p);
        BT = @(z) zeta.*At_f(z, n, omega, p);
end

%%%%*********************************************************
for kk = 1:length(kvec)
    disp(kk);
    for sig = 1:length(sigma)
        D = [];
        for i = 1:kvec(kk)    %constructing random D
            temp = randn(q,1);
            % temp = ones(q,1)*randn(1,1);
            d = diag(temp);
            D = [D;d];
        end
        y1 = g(D*B(x)) + sigma(sig)*randn(size(D,1),1);
        
        %%%-------- DMF --- 1D Tone estimation -------------------------
        alpha = 100000; % Grid seatch resolution
        u1 = zeros(kvec(kk),1);
        v1 = zeros(kvec(kk),1);
        z_hatDMF = zeros(q,1);
        zz = B(x);
        
        z_min = -.01;
        z_max = .01;
        
        for j =1:q
            u1 = y1(j:q:end);
            v1 = D(j:q:end,j);
            z_hatDMF(j) = recover_tone(u1,v1,g,z_min,z_max,alpha,flg);
        end
        %%%----------------DHT----Demixing recovery -----------------------
        recopts.s1 = s1;
        recopts.s2 = s2;
        recopts.m = q;
        recopts.th = 0.001;
        recopts.it = 2500;
        recopts.init = 'oneshot';  % 'zeros', 'random'
        recopts.n = n;
        recopts.lambda = 1;
        recopts.verbose = 1;
        recopts.eta = 1;
        
        recopts.recflag = 'totally_sparse'; 
%         recopts.recflag = 'individually_sparse'; 
        
        [xhat,w_hatm,z_hatm,~,~,~,~] = htdemix(z_hatDMF,f,B,BT,Phi,PhiT,Psi,PsiT,recopts);
    end
%     x_hat(:,kk) = Phi(w_hatDMF);
end
plotimgesdemixing
