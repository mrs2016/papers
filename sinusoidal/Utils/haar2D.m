function y = haar2D(x,flg)

dwtmode('per');
% assume that length(x) is a power of 2
n = sqrt(numel(x)); 
levels = log2(n);

[~,lvs] = wavedec2(randn(n),levels,'db8');

switch flg
    case 'forward'
        X = reshape(x,[],n);
        [c,~] = wavedec2(X,levels,'db8');
        y = c(:);
    case 'inverse'
        x = x';
        Y = waverec2(x,lvs,'db8');
        y = Y(:);
end
