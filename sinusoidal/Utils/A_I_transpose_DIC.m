function y = A_I_transpose_DIC(A,PhiT,x,I)
% assume consistency all around
% this function takes as input a function handle A and a vector x
% and computes y = A(:,I)'*x
z = PhiT(A(x));
y = z(I);