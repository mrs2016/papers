function y = mymat(x)
sqrtn = sqrt(numel(x));
siz = [sqrtn sqrtn];
y = reshape(x,siz);
