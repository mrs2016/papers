clc;clear;
close all

%%*********************************** test recovery_tone ******************
k = [10 2^12 214 2^16];   % The number of measurements
z = [-1e+6 -0.2 -2 3 1000 1e+6];  %Unknown frequency
alpha = 1000;
opts.link = 'sin';
switch opts.link
    case 'com_exp'
        g = @(x) exp(1i*x);
        flg = 1;
    case 'sin'
        g = @(x) sin(x);
        flg = 0;
end

for i = 3:3   %length(z)        %loop on the unknown frequency
    for j = 1:1%length(k)     %loop on the number of measurements
        
        %         v = randn(k(j),1);
        v = rand(k(j),1)  - 0.5;
        y = g(v*z(i));
        z_min = -4;
        z_max = 4;
        [z_hat] = recover_tone(y,v,g,z_min,z_max,alpha,flg)
        
        %%%%% MUSIC method %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        v = [0.5:5.5]';
%         y = g(v*z(i));
        y = (1/2*1i)*exp(1i*v*z(i)) - (1/2*1i)*exp(-1i*v*z(i));
        [~,R] = corrmtx(y,4,'mod');
        [Z_music,~] = rootmusic(R,2,'corr')
        
    end
end

%%********************************************* test IHT ******************
% q = 2^8;
% svec = 5;   % Sparsity of the signal
% n = 2^14;      %Ambient diemnsions of the signal
%
%
% for ss = 1:length(svec)
%     x = 0*(1:n)';
%     suppx = randperm(n); suppx = suppx(1:svec(ss));
%     x(suppx) = randn(svec(ss),1);
%     A = (1/sqrt(q))*randn(q,n);
%
%     y = A*x;
%
%     tol = 0.001;
%     iter = 1000;
%     eta = 0.1;
%     log = 1;   % = 1: print the errors,    = 0: no print
%     [x_hat,err_trace] = iht(y,A,svec(ss),eta,tol,iter,log);
%     err = norm(x - x_hat)/norm(x);
% end
