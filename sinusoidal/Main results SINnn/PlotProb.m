clc;close all;
clear;
load 'S100sinNN.mat';
RE_th = 0.05;
CS_th = 0.99;
[u,v,w] = size(Re_errDMF1);

Succ_DMF_RE_1 = zeros(1,v);
Succ_DMF_RE_15 = zeros(1,v);
Succ_DMF_RE_30 = zeros(1,v);
Succ_GHT_RE_1 = zeros(1,v);
Succ_GHT_RE_15 = zeros(1,v);
Succ_GHT_RE_30 = zeros(1,v);

Succ_DMF_CS_1 = zeros(1,v);
Succ_DMF_CS_15 = zeros(1,v);
Succ_DMF_CS_30 = zeros(1,v);
Succ_GHT_CS_1 = zeros(1,v);
Succ_GHT_CS_15 = zeros(1,v);
Succ_GHT_CS_30 = zeros(1,v);

for k = 1:v
    for e = 1:w
        if Re_errDMF1(1,k,e) <= RE_th
            Succ_DMF_RE_1(1,k) =  Succ_DMF_RE_1(1,k) + 1;
        end
        if Re_errDMF15(1,k,e) <= RE_th
            Succ_DMF_RE_15(1,k) =  Succ_DMF_RE_15(1,k) + 1;
        end
        if Re_errDMF30(1,k,e) <= RE_th
            Succ_DMF_RE_30(1,k) =  Succ_DMF_RE_30(1,k) + 1;
        end
        
        if Re_errGHT1(1,k,e) <= RE_th
            Succ_GHT_RE_1(1,k) =  Succ_GHT_RE_1(1,k) + 1;
        end
        if Re_errGHT15(1,k,e) <= RE_th
            Succ_GHT_RE_15(1,k) =  Succ_GHT_RE_15(1,k) + 1;
        end
        if Re_errGHT30(1,k,e) <= RE_th
            Succ_GHT_RE_30(1,k) =  Succ_GHT_RE_30(1,k) + 1;
        end
        
        
        
        if Cos_similarityDMF1(1,k,e) >= CS_th
            Succ_DMF_CS_1(1,k) =  Succ_DMF_CS_1(1,k) + 1;
        end
        if Cos_similarityDMF15(1,k,e) >= CS_th
            Succ_DMF_CS_15(1,k) =  Succ_DMF_CS_15(1,k) + 1;
        end
        if Cos_similarityDMF30(1,k,e) >= CS_th
            Succ_DMF_CS_30(1,k) =  Succ_DMF_CS_30(1,k) + 1;
        end
        
        if Cos_similarityGHT1(1,k,e) >= CS_th
            Succ_GHT_CS_1(1,k) =  Succ_GHT_CS_1(1,k) + 1;
        end
        if Cos_similarityGHT15(1,k,e) >= CS_th
            Succ_GHT_CS_15(1,k) =  Succ_GHT_CS_15(1,k) + 1;
        end
        if Cos_similarityGHT30(1,k,e) >= CS_th
            Succ_GHT_CS_30(1,k) =  Succ_GHT_CS_30(1,k) + 1;
        end
    end
    
end

Succ_DMF_RE_1 = Succ_DMF_RE_1/w;
Succ_DMF_RE_15 = Succ_DMF_RE_15/w;
Succ_DMF_RE_30 = Succ_DMF_RE_30/w;
Succ_GHT_RE_1 = Succ_GHT_RE_1/w;
Succ_GHT_RE_15 = Succ_GHT_RE_15/w;
Succ_GHT_RE_30 = Succ_GHT_RE_30/w;

Succ_DMF_CS_1 = Succ_DMF_CS_1/w;
Succ_DMF_CS_15 = Succ_DMF_CS_15/w;
Succ_DMF_CS_30 = Succ_DMF_CS_30/w;
Succ_GHT_CS_1 = Succ_GHT_CS_1/w;
Succ_GHT_CS_15 = Succ_GHT_CS_15/w;
Succ_GHT_CS_30 = Succ_GHT_CS_30/w;

figure;
plot(kvec,Succ_DMF_RE_1,'-bo','LineWidth',2);
hold on
plot(kvec,Succ_DMF_RE_15,'--bs','LineWidth',2);
plot(kvec,Succ_DMF_RE_30,':b^','LineWidth',2);
plot(kvec,Succ_GHT_RE_1,'-ro','LineWidth',2);
plot(kvec,Succ_GHT_RE_15,'--rs','LineWidth',2);
plot(kvec,Succ_GHT_RE_30,':r^','LineWidth',2);
xlim([1 8])
axisfortex('','Number of diagonal blocks, k = m/q','Probability of recovery');
grid on
h_legend = legend('MF-Sparse, ||x||_2 = 1','MF-Sparse, ||x||_2 = 15','MF-Sparse , ||x||_2 = 30', 'GHT, ||x||_2 = 1',...
    'GHT, ||x||_2 = 15','GHT, ||x||_2 = 30' ,'location','southeast');

% set(h_legend,'FontSize',16);

print(gcf,'-r300','-dpdf','sinProbRec.pdf');
movefile('sinProbRec.pdf','~/Documents/Figs');

% figure;
% plot(kvec,Succ_DMF_CS_1,'-bo','LineWidth',2);
% hold on
% plot(kvec,Succ_DMF_CS_15,'--bs','LineWidth',2);
% plot(kvec,Succ_DMF_CS_30,':b^','LineWidth',2);
% plot(kvec,Succ_GHT_CS_1,'-ro','LineWidth',2);
% plot(kvec,Succ_GHT_CS_15,'--rs','LineWidth',2);
% plot(kvec,Succ_GHT_CS_30,':r^','LineWidth',2);
% xlim([1 8])
% axisfortex('Sparsity level = 100, q = 700','Number of diagonal blocks k = m/q','Probability of success (Cosine Similarity)');
% grid on
% h_legend = legend('DMF, ||x|| = 1','DMF, ||x|| = 15','DMF, ||x|| = 30', 'GHT, ||x|| = 1','GHT, ||x|| = 15','GHT, ||x|| = 30' );
