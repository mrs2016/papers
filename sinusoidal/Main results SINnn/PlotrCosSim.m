clc; clear;
close all;
load('S100sinNN.mat');

figure,
plot(kvec,mean(Cos_similarityDMF1,3),'-bo','LineWidth',2);
hold on
plot(kvec,mean(Cos_similarityDMF15,3),'--bs','LineWidth',2);
plot(kvec,mean(Cos_similarityDMF30,3),':b^','LineWidth',2);

plot(kvec,mean(Cos_similarityGHT1,3),'-ro','LineWidth',2);
plot(kvec,mean(Cos_similarityGHT15,3),'--rs','LineWidth',2);
plot(kvec,mean(Cos_similarityGHT30,3),':r^','LineWidth',2);

plot(kvec,mean(Cos_similarityONESHOT1,3),'-ko','LineWidth',2);
plot(kvec,mean(Cos_similarityONESHOT15,3),'--ks','LineWidth',2);
plot(kvec,mean(Cos_similarityONESHOT30,3),':k^','LineWidth',2);

xlim([1 8])
axisfortex('','Number of diagonal blocks, k = m/q','Cosine Similariy');
grid on
h_legend = legend('MF-Sparse, ||x||_2 = 1','MF-Sparse, ||x||_2 = 15','MF-Sparse, ||x||_2 = 30', 'GHT, ||x||_2 = 1',...
    'GHT, ||x||_2 = 15','GHT, ||x||_2 = 30', '[Plan et al.], ||x||_2 = 1','[Plan et al.], ||x||_2 = 15','[Plan et al.], ||x||_2 = 30' );

% set(h_legend,'FontSize',12);

% print(gcf,'-r300','-dpdf','sinCosSimi.pdf');
% movefile('sinCosSimi.pdf','~/Documents/Figs');