function g = sawtoothApp(x,NoFS,T,amp)

w0 = 2*pi/T;
p = NoFS;
g = zeros(length(x),1);
for k = -p:p
    if k~=0
        g = g + (amp/T)*(1i/(k*w0))*exp(1i*k*w0*x);
    end
end
g = g + amp/2;
