function xhat = oneshot(y,D,BT,s)
% A = D*B  and AT = BT*DT
m = length(y);

xlin = 1/m*BT(D'*y); 
xhat = thresh(xlin,s);

end


function xhat = thresh(x,s)
xhat = 0*x;
[~,idx] = sort(abs(x),'descend');
idx = idx(1:s);
xhat(idx) = x(idx);
end
