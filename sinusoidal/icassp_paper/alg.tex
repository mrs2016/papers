\section{Mathematical model and Algorithm}
\label{sec:alg} 

In this section, we present our algorithm for solving problem~\eqref{eq:sine_obs} and provide the theory for sample complexity of the proposed algorithm. We first establish some notation. %$\|x\|_p$ denotes the $\ell_p$ norm of vector $x$. 
%Also, when we say that a signal $x$ is $s$-sparse, we mean that $\|x\|_0\leq s$. 
Let the $j^{\mathrm{th}}$ entry of the signal $x\in\mathbb{R}^n$ be denoted as $x_j$. For any $j \in [q]$, $x(j:q:(k-1)q+j)$ denotes the sub-vector in $\mathbb{R}^k$ formed by the entries of $x$ starting at index $j + qr$, where $r = 0, 1, \ldots, k-1$. Similarly, $A((j:q:(k-1)q,l)$ represents the sub-vector constructed by picking the $l^{\mathrm{th}}$ column of any matrix $A$ and selecting the entries of this column with the same procedure as mentioned. 

%Moreover, we need the following definitions from compressive sensing literature:
%\begin{definition}{Restricted Isometry Property (RIP)}
%A matrix $B$ satisfies RIP, if the following satisfies for all $s$-sparse signals:
%\begin{align*}
%(1-\delta)\|x\|_2\leq\|Ax\|_2\leq(1+\delta)\|x\|_2,
%\end{align*}
%,then smallest number $\delta > 0$ is denoted by $\delta_s$ and called RIP-constant.
%\end{definition}

As the key component in our approach, instead of using a random Gaussian linear projection as proposed in~\cite{rahimi2007random}, we construct a linear operator $A$ that can be \emph{factorized} as $A = D B$, where $D\in\mathbb{R}^{m\times q}$, and $B\in\mathbb{R}^{q\times n}$. We assume that $m$ is a multiple of $q$, and that $D$ is a concatenation of $k$ diagonal matrices of $q \times q$ such that the diagonal entries in the blocks of $D$ are i.i.d. random variables, drawn from a distribution that we specify later. The choice of $B$ depends on the model assumed on the data vector $x$; if the data is $s$-sparse, then $B$ can be any matrix that supports stable sparse recovery (more precisely, $B$ satisfies the null-space property~\cite{foucart2013}). Overall, our low-dimensional feature map can be written as:
%\begin{align}\label{RDMF}
%y = \exp(iDBx) + e = \exp\left(i
%\begin{bmatrix}
%d_{1}^1 & \ldots & 0 \\
% &  \ddots &  \\
%0 &  \ldots & d_{q}^1\\
%d_{1}^2 & \ldots & 0 \\
% &  \ddots &  \\
%0 &  \ldots & d_{q}^2\\
% & \vdots &  \\
%d_{1}^k & \ldots & 0 \\
% &  \ddots &  \\
%0 &  \ldots & d_{q}^k\\
%\end{bmatrix}
%Bx
%\right) + e,
%\end{align}
\begin{align}\label{RDMF}
y = \exp(iDBx) + e = \exp\left(i
\begin{bmatrix}
D^1 \\
D^2 \\
 \vdots  \\
D^k
\end{bmatrix}
Bx
\right) + e,
\end{align}
where $D^i$ are diagonal matrices and $e\in\mathbb{R}^m$ denotes additive noise such that $e\sim\mathcal{N}(0,\sigma^2I)$. The goal is to stably recover $x$ from the embedding $y$.
%As we can see, matrix $D$ has $k$ diagonal blocks with entries $d_l^r$ which are are i.i.d standard normal random variables for all $l=1\ldots q$ and $r = 1\ldots k$.  

By the block diagonal structure of the outer projection $D$, we can reduce the overall reconstruction problem to first obtaining a good enough estimate of each entry of $Bx$. We show below that this can be achieved using line spectral estimation methods. The output of the first stage is used as the input of the second stage, which involves estimating $x$ from a (possibly noisy) estimate of $Bx$. 

%We discuss these two stages in detail as follows.

\subsection{Line spectral estimation}\label{ToneEst}

Consider the observation model~\eqref{RDMF} and let $z = Bx \in \R^q$. Suppose we know \emph{a priori} that the entries of $z$ belong to some bounded set $\Omega \in R$. Fix $l \in [q]$, and let {$t = D(l:q:(k-1)q+l,l), u = y(l:q:(k-1)q+l), h = e(l:q:(k-1)q+l)$} which are vectors in $\mathbb{R}^k$. We observe that:
\[
u = \exp(i z_l t) + h \, .
\]
In other words, $u$ can be interpreted as a collection of time samples of a (single-tone, potentially off-grid) complex-valued signal with frequency $z_l \in \Omega$, measured at time locations $t$. Therefore, we can independently estimate $z_l$ for $l=1, \ldots, q$ by solving a least-squares problem~\cite{eftekhari2013matched}:
\begin{align}
\label{OptmExp}
\widehat{z_l} = \underset{v \in \Omega}{\argmin}\|u - \exp(i \, vt )\|_2^2 = \underset{v \in \Omega}{\argmax}\left|\langle u,\psi_{v}\rangle\right|,
 \end{align}
for all $l=1,\ldots,q$, where  $\psi_{v}\in\mathbb{R}^k$ denotes a \emph{template vector} given by $\psi_{v} = \exp(j t v)$ for any $v \in \Omega$. In essence, the solution of the least-squares problem can be interpreted as a \emph{matched filter}. Numerically, the optimization problem in~\eqref{OptmExp} can be solved using a grid search over the set $\Omega$, and the resolution of this grid search controls the running time of the  algorithm; for fine enough resolution, the estimation of $z_l$ is more accurate albeit with increased running time. This issue is also discussed in~\cite{eftekhari2013matched} and more sophisticated spectral estimation techniques have appeared in~\cite{eldarxampling,tangcsoffgrid,chioffgrid}. After obtaining all the estimates $\widehat{z_l}$'s, we stack them in a vector $\widehat{z}$. 

\subsection{Sparse recovery}
We now propose to recover the high-dimensional signal $x \in \R^m$ in problem~\eqref{RDMF} using our estimated $\widehat{z} \in \R^q$. According to our model, we also have assumed that the matrix $B$ supports {stable sparse recovery}, and the underlying signal $x$ is $s$-sparse. Hence, we can use any generic sparse recovery algorithm of our choice to obtain an estimate of $x$. In our simulations below, we use the CoSaMP algorithm~\cite{cosamp} due to its speed and ease of parameter tuning. Again, we stress that this stage depends on a model assumption on $x$, and other recovery algorithms (such as structured-sparse recovery~\cite{modelcs}, low-rank matrix recovery~\cite{rechtfazelparillo}, or demixing a pair of sparse (but incoherent) vectors~\cite{soltani2016fastIEEETSP17}) can equally well be used. Our overall algorithm, \emph{Matched Filtering+Sparse recovery} (MF-Sparse) is described in pseudocode form in Algorithm~\ref{alg:RDMF}.
We now provide a theoretical analysis of our proposed algorithm. Our result follows from a concatenation of the results of~\cite{eftekhari2013matched} and~\cite{cosamp}.

\begin{theorem}[Sample complexity of MF-Sparse]
Assume that the nonzero entries of $D$ are i.i.d.\ samples from a uniform distribution $[-T,T]$, and the entries of $B$ are i.i.d.\ samples from $\mathcal{N}(0,1/q)$. Also, assume $\|x\|_2 \leq R$ for some constant $R>0$. Set $m = kq$ where $k=c_1 \log\left(\frac{Rq}{\varepsilon}\frac{1}{\delta}\right)(1+\sigma^2)$ for some $\varepsilon>0$ and $q = c_2 \left(s\log\frac{n}{s}\right)$. Set $\omega = c_3 R$ and $\Omega = [-\omega,\omega]$. Then, MF-Sparse returns an estimate $\widehat{x}$, such that 
$$\|x - \widehat{x}\|_2 \leq C \varepsilon \, ,$$ 
with probability exceeding $1-\delta$. Here, $c_1, c_2, c_3, C$ are constants. 
\end{theorem}

\begin{proof}
Set $\varepsilon'>0$. Based on Corollary 8 of~\cite{eftekhari2013matched}, by setting $|T| = \mathcal{O}(\frac{1}{\varepsilon'})$, we need $k=\mathcal{O}\left((1+\sigma^2)\log\left(\frac{|\Omega|}{\varepsilon'}\frac{1}{\delta}\right)\right)$ to obtain $|\widehat{z_l} -z_l|\leq\varepsilon'$  with probability at least $1-\delta$ for each $l=1, \ldots, q$. Here, $|\Omega|$ denotes the radius of the feasible range for $z$, and appears in the maximization problem~\eqref{OptmExp}. Observe that $|\Omega| = \mathcal{O}\left(\|Bx\|_{\infty}\right)$. If the entries of matrix $B$ are chosen as stipulated in the theorem, then $\|Bx\|_{\infty} \leq  \|Bx\|_{2} \lesssim \mathcal{O}(R)$ which justifies the choice of $|\Omega|$ in the theorem. Thus, $k=\mathcal{O}\left((1+\sigma^2)\log\left(\frac{R}{\varepsilon'}\frac{1}{\delta}\right)\right)$. 
By a union bound, it follows that with probability at least $1- q\delta$, we have $\|\widehat{z} - z\|_{\infty}\leq\varepsilon'$. Now, we can write $\widehat{z} = z + e' = Bx + e'$ where $\|e'\|_{\infty}\leq\varepsilon'$. Since we have used CoSaMP in the sparse recovery stage, if the choice of $q=\mathcal{O}\left(s\log\frac{n}{s}\right)$ enables us to obtain $\|\widehat{x} - x\|_2\leq c\|e'\|_2\leq c\sqrt{q}\|e'\|_{\infty}\leq c\sqrt{q}\varepsilon'$~\cite{cosamp}. (In fact, a more general guarantee can be derived even for the case when $x$ is not exactly $s$-sparse.) Now, let $\varepsilon' = \mathcal{O}\left(\frac{\varepsilon}{\sqrt{q}}\right)$ to obtain the final bound on the estimation error $\|\widehat{x}-x\|_2\leq\mathcal{O}(\varepsilon)$ with $k=\mathcal{O}\left((1+\sigma^2)\log\left(\frac{Rq}{\varepsilon}\frac{1}{\delta}\right)\right)$.
\end{proof}

%\begin{remark}[Real random sinusoidal feature]
In model~\eqref{eq:sine_obs}, the features $y_j$ are modeled in terms of complex exponentials. With only a slight modification in the line spectral estimation stage, we can recover $x$ from real-valued random sine features. More precisely, these random features are given by:
\begin{align}
\label{eq:realsine_obs}
y = \sin(DBx) + e
\end{align}
where $e$ denotes the additive noise as defined before. If we follow an analogous approach for estimating $x$, then in lieu of the least squares estimator \eqref{OptmExp}, we have the following estimator:
\begin{align}
\label{OptmSin}
\widehat{z_l} &= \underset{v \in\Omega}{\argmin}\|u - \sin(v t)\|_2^2 \\
&= \underset{v \in \Omega}{\argmax}\left( 2\left|\langle u,\psi_{v}\rangle\right| - \|\psi_{v}\|_2^2\right),
\end{align}
for $l=1,\ldots,q$ and $u$ as defined above. {Also, $\psi_{v} = \sin(tv)$ for any $v \in \Omega$}. We only provide some numerical experiments for this estimator and leave a detailed theoretical analysis for future research.
%\end{remark}

\begin{algorithm}[t]
\caption{\textsc{MF-Sparse}}
\label{alg:RDMF}
\begin{algorithmic}
\State\textbf{Inputs:} $y$, $D$, $B$, $\Omega$, $s$
\State\textbf{Output:}  $\widehat{x}$
%\State $ k \leftarrow m/q$
\State \textbf{Stage 1: Tone estimation:}
\For {$l =1:q$}
\State $t \leftarrow D(l:q:(k-1)q+l,l)$
\State $u \leftarrow y(l:q:(k-1)q+l)$
%\State estimate $z_l$ from $u = \exp(jtz_l)$ using 
\State $\widehat{z_l} = \argmax_{\omega\in\Omega}|\langle y,\psi_{\omega}\rangle|$
\EndFor
\State $\widehat{z} \leftarrow [\widehat{z_1},\widehat{z_2}\ldots,\widehat{z_q}]^T$
\State \textbf{Stage 2: Sparse recovery}
%\State $\varepsilon \leftarrow \mathcal{O}(\frac{1}{|T|})$
\State $\widehat{x} \leftarrow \textsc{CoSaMP}(\widehat{z},B,s)$
\end{algorithmic}
\end{algorithm}
