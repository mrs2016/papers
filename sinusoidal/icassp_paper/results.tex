\section{Experimental Results}
\label{sec:res}

%In this section, we provide some numerical experiments for our proposed algorithm. 

We compare our algorithm with existing algorithms in various scenarios. In the first experiment, we assume that our random features are computed using a real sine link function. In other words, the features, $\{y_j\}$ is given by~\eqref{eq:realsine_obs} for all $j=1,\ldots,q$.

In Fig.~\ref{fig:sinNonoise}(a), we compare the probability of recovery of MF-Sparse with \textit{Gradient Hard Thresholding} (GHT), a projected-gradient descent type algorithm whose variants have proposed in~\cite{bahmani2011greedy,yuan2014gradient,jain2014iterative}. To recover $x$, GHT tries to minimize a specific loss function (typically, the squared loss) between the observed random feature vector, $y$, and $\sin(DBx)$ by iteratively updating the current estimate of $x$ based on a gradient update rule, and then projecting it into the set of $s$-sparse vectors via hard thresholding. 

The setup for the experiment illustrated in Fig.~\ref{fig:sinNonoise}(a) is as follows. First, we generate a synthetic signal of length $n = {2^{14}}$ with sparsity $s=100$ such that the support is random and the values of the signal in the support are drawn from a standard normal distribution. Then, the $\ell_2$-norm of $x$ is adjusted via a global scaling to coincide with three different values; $\|x\|_2 =1, \|x\|_2 = 15$, and $\|x\|_2 =30$. We generate a matrix $B\in\mathbb{R}^{q\times n}$ where $q =700$ and $n = 2^{14}$ such that the entries of it are i.i.d random variables with distribution $\mathcal{N}(0,\frac{1}{\sqrt{q}})$. All nonzero entries of $D$, $d_{l}^r$ for $l=1,\ldots, q$ and $r = 1, \ldots, k$ are assumed to be standard normal random variables for $k=1,\ldots, 8$. Next, we generate $y\in\mathbb{R}^m$ as $y = \sin(DBx)$ where $m = kq$. (There is no noise considered in this experiment.) By running MF-Sparse and GHT, we obtain the estimate of $x$, denoted by $\widehat{x}$, and calculate the (normalized) estimation error defined as $\frac{\|\widehat{x}-x\|_2}{\|x\|_2}$. We repeat this process for $60$ Monte Carlo trials, and define the empirical probability of {successful} recovery as the fraction of simulations in which the relative error is less than $0.05$. 

As we can see in Fig.~\ref{fig:sinNonoise}(a), MF-Sparse can successfully recover $x$ even with $k=4$ number of blocks with probability close to $1$ when the norm of $x$ is small. In this regime, i.e., $\|x\|_2$ being small, both GHT and MF-Sparse display similar performance. However, GHT shows extremely poor performance when $\|x\|_2$ has moderate or large value. The reason is that when the norm of $x$ is small, the entries of $Bx$ are also small (i.e., the entries of $DBx$ are close to the origin with high probability), and the sinusoidal nonlinearity can be assumed to be almost linear. Consequently, GHT can recover $x$ in this regime. However, this assumption breaks down for larger values of $\|x\|_2$.

\begin{figure}[t]
\begin{center}
\begingroup
\setlength{\tabcolsep}{.1pt} % Default value: 6pt
\renewcommand{\arraystretch}{.1} % Default value: 1
\begin{tabular}{cc}      %{c@{\hskip .1pt}c@{\hskip .1pt}c}
\includegraphics[trim = 8mm 58mm 15mm 60mm, clip, width=0.48\linewidth]{Figs/sinProbRec.pdf}&
\includegraphics[trim = 8mm 58mm 15mm 60mm, clip, width=0.48\linewidth]{Figs/sinCosSimi.pdf}\\
(a) & (b) 
\end{tabular}
\endgroup
\end{center}
\caption{\emph{Comparison of the proposed algorithm with other algorithms. Parameters: $n =2^{14}, q= 700$. (a) Probability of recovery in terms of normalized error. (b) Cosine similarity between recovered and true signal.}}
\label{fig:sinNonoise}
\end{figure}

In Fig.~\ref{fig:sinNonoise}(b), we repeat a similar experiment, but measure performance with a different recovery criterion. In this scenario, we measure the \textit{cosine similarity} of the estimated vector $\widehat{x}$ with $x$, defined as $\frac{\widehat{x}^Tx}{\|\widehat{x}\|_2\|x\|_2}$. In addition to GHT, we also compare the performance of MF-Sparse with the single-step thresholding approach proposed by~\cite{plan2014high}. The approach of~\cite{plan2014high} only recovers the vector modulo an (unknown) scaling factor, and does not need to possess knowledge of the nonlinearity. As illustrated in Fig.~\ref{fig:sinNonoise}(b), the approach of~\cite{plan2014high} has worse performance compared to two other methods. Also, GHT shows good performance only when $\|x\|_2 = 1$, as expected. In contrast, MF-Sparse shows the best performance.

\begin{figure}[t]
\begin{center}
\includegraphics[trim = 5mm 58mm 5mm 72mm, clip, width=0.55\linewidth]{Figs/expReErrNoise.pdf}
\end{center}
\caption{\emph{Comparison of matched filtering versus rootMUSIC in the first stage. Parameters:  $n =2^{14}, q= 800$, $k =6$. %RDMF is more robust to increasing of the noise compared to DDMUSIC.
}}
\label{fig:expWnoise}
\end{figure}

\begin{figure}[t]
\begin{center}
\begingroup
\setlength{\tabcolsep}{.1pt} % Default value: 6pt
\renewcommand{\arraystretch}{.1} % Default value: 1
\begin{tabular}{ccc}      %{c@{\hskip .1pt}c@{\hskip .1pt}c}
\includegraphics[trim = 47mm 68mm 15mm 55mm, clip, width=0.32\linewidth]{Figs/truepepper.pdf}&
\includegraphics[trim = 47mm 68mm 15mm 55mm, clip, width=0.32\linewidth]{Figs/2Dk2.pdf} &
\includegraphics[trim = 47mm 68mm 15mm 55mm, clip, width=0.32\linewidth]{Figs/2Dk3.pdf} \\
%\includegraphics[trim = 47mm 68mm 15mm 35mm, clip, width=0.32\linewidth]{Figs/2Dk5.pdf}\\
(a) & (b) & (c) 
\end{tabular}
\endgroup
\end{center}
\caption{\emph{Successful reconstruction on a real $2$-D image from random sinusoidal features. (a) Test image. (b) Reconstruction quality with $k=2$ diagonal blocks.
(c) Reconstruction with $k=3$.}}
\label{fig:pepper}
\end{figure}

\begin{figure}[t]
\begin{center}
\begingroup
\setlength{\tabcolsep}{.1pt} % Default value: 6pt
\renewcommand{\arraystretch}{.1} % Default value: 1
\begin{tabular}{ccc}      %{c@{\hskip .1pt}c@{\hskip .1pt}c}
\includegraphics[trim = 47mm 68mm 15mm 55mm, clip, width=0.32\linewidth]{Figs/GS.pdf}&
\includegraphics[trim = 47mm 68mm 15mm 55mm, clip, width=0.32\linewidth]{Figs/phiwHTM.pdf} &
\includegraphics[trim = 47mm 68mm 15mm 55mm, clip, width=0.32\linewidth]{Figs/psizHTM.pdf} \\
(a) & (b) & (c) 
\end{tabular}
\endgroup
\end{center}
\caption{\emph{Successful demixing on a real 2-dimensional image from random sinusoidal features. Parameters: $n = 512 \times 512, s = 1000, m = k\times q=48000, g(x) = \sin(x)$. Image credits:~\cite{mccoy2014convexity}.}}
\label{fig:GalStar}
\end{figure}

Next, we consider the experiment illustrated in Fig.~\ref{fig:expWnoise}. In this experiment, we assume a complex sinusoid as the nonlinearity generating the feature map. In this scenario, we fix $k= 6$ (number of diagonal blocks in matrix $D$), $q=800$, and add i.i.d.\ Gaussian noise to the embeddings with increasing variance. Our goal is to compare the matched filtering part of MF-Sparse with classical spectral estimation techniques. Indeed, a natural question is whether we can use other spectral estimation approaches instead of the matched filter, and whether there is any advantage in choosing the elements of $D$ randomly. We attempt to illustrate the benefits of randomness through the experiment in Fig.~\ref{fig:expWnoise}. Here, we compare the relative error of MF-Sparse with an analogous algorithm that we call \emph{RM-Sparse}, which uses the RootMUSIC spectral estimation technique~\cite{schmidt1986multiple}. For RM-Sparse, in the line spectral estimation stage, we replace random diagonal blocks in matrix $D$ with deterministic diagonal ones, and letting the diagonal entries of the $r^{\mathrm{th}}$ block being proportional to $r$. In other words, the time samples are chosen to lie on a uniform grid, as RootMUSIC expects. As we see in Fig.~\ref{fig:expWnoise}, while RM-Sparse does recover the original $x$ for very low values of noise variance, MF-Sparse outperforms RM-Sparse when the noise level increases; this verifies the robustness of MF-Sparse to noise in the observations.

In addition, we evaluate our proposed algorithm for a real $2$D image. We begin with a $512\times 512$ test image. First, we obtain its 2D Haar wavelet decomposition and sparsify it by retaining the $s = 2000$ largest coefficients. Then, we synthesize the test image based on these largest coefficients, and multiply it by a subsampled Fourier matrix with $q = 16000$ multiplied with a diagonal matrix with random $\pm 1$ entries~\cite{krahmer2011new}. Further, we multiply this result with a block diagonal matrix $D$ with number of blocks $k=2,3$. Now, we apply the $\sin(\cdot)$ function element-wise to the resulting vector. Figure~\ref{fig:pepper} shows the recovered image using MF-Sparse. As is visually evident, with $k=2$, the quality of the reconstructed image is poor, but $k=3$ already yields a solution close to the true image. 
%This experiment verifies good performance of RDMF even for real $2$-D images.

Finally, we present another experiment on a real $2$D image. In this case, we assume that our signal $x$ is the superposition of two constituent signals, i.e., $x = \Phi w + \Psi z$. Hence, the random feature map is given by $y = \sin(DB\left(\Phi w + \Psi z\right))$. In this setting, as illustrated in~Fig.~\ref{fig:GalStar}(a), the signal $x$ is the mixture of galaxy and star images, where the constituent coefficient vectors $w$ and $z$ are $s$-sparse signals in the DCT basis ($\Phi$) and the canonical basis ($\Psi$). In this experiment, we fix $s = 1000$ for both $w$ and $z$.  The matrices $B$ and $D$ are generated the same way as in the experiment expressed in~Fig.~\ref{fig:pepper} with $q=16000, n=2^{18}$, and $k=3$. As we can see in Figs.~\ref{fig:GalStar}(b) and~\ref{fig:GalStar}(c), running a variant of MF-Sparse along with the DHT algorithm~\cite{soltani2016fastIEEETSP17} for the second stage can successfully separate the galaxy from the stars, verifying the applicability of MF-Sparse for more generic structured inverse problems. 
