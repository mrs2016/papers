clc;close all;
clear;
load('S100expNorm1k6WN.mat')
figure;
plot(sigma,mean(Re_errDMF,3),'-bo','LineWidth',2);
hold on
plot(sigma,mean(Re_errMUSIC,3),'-rs','LineWidth',2);
xlim([0 sigma(length(sigma))])
axisfortex('','Standard deviation of the noise, \sigma','Relative Error');
grid on
legend('MF-Sparse','RM-Sparse','location','northwest')

print(gcf,'-r300','-dpdf','expReErrNoise.pdf');
movefile('expReErrNoise.pdf','~/Documents/Figs');