% % *****  Author: Mohamamdreza Soltani 09/06/2016
% % Nonlinear signal recovery --- nonlinear function g(x) = exp(jx) (complex exponential -- j = sqrt(-1))
% % Measurement model ---- y = g(jAx) + e. Here, y is a m*1 vector,  A is a m*n (m << n)
% % matrix and is decoupled as A = D*B where D is a m*q matrix which includes
% % k digonal matrix and B is a q*n matrix. Also, e denotes the noise.
% % The goal is to recover signal x from y asssuming that x is  s-sparse.

clc;clear;
close all
addpath Utils/
%%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
E = 60;                   %Monte Carlo trails
kvec = 6;             % The number of diagonal blocks (An integer number)
q = 800;                 % Each diagonal block is a q*q matrix,  m = k . q
n = 2^14;                % Ambient diemnsions of the signal
svec = 100;                % Sparsity of the signal
Ampl = 1;              %Norm of x (true Signal)
sigma = 0:.02:0.25;                % Variance of the additive noise
opts.link = 'com_exp';
opts.meas = 'gauss';
opts.cs_alg = 'cosamp';
opts.verbose = 0;   % = 1: print the errors,    = 0: no print for DMF and GHT
% link function
switch opts.link
    case 'com_exp'
        g = @(x) exp(1i*x);
        flg = 1;
    case 'sin'
        g = @(x) sin(x);
        flg = 0;
end
%%%%%%%%%%%%%% Measurement model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Re_errDMF = zeros(length(svec),length(sigma),E);
Cos_similarityDMF = zeros(length(svec),length(sigma),E);

Re_errMUSIC = zeros(length(svec),length(sigma),E);
Cos_similarityMUSIC = zeros(length(svec),length(sigma),E);

% Re_errONESHOT = zeros(length(svec),length(sigma),E);
% Cos_similarityONESHOT = zeros(length(svec),length(sigma),E);
%
% Re_errGHT = zeros(length(svec),length(sigma),E);
% Cos_similarityGHT = zeros(length(svec),length(sigma),E);

for ee = 1:E
    disp(ee)
    for ss = 1:length(svec)
        x = 0*(1:n)';
        suppx = randperm(n); suppx = suppx(1:svec(ss));
        x(suppx) = randn(svec(ss),1);
        x = Ampl*x/norm(x);
        
        % measurement operator
        switch opts.meas
            case 'gauss'
                Bmat = (1/sqrt(q))*randn(q,n);
                B = @(x) Bmat*x; BT = @(x) Bmat'*x;
            case 'fourier'
                zeta = sign(randn(n,1));
                p = randperm(n)';
                que = randperm(n/2-1)+1; % this keeps a random set of FFT coefficients
                omega = que(1:q/2)';
                B = @(z) A_f(zeta.*z, omega, p);
                BT = @(z) zeta.*At_f(z, n, omega, p);
        end
        
        for kk = 1:length(kvec)
            for sig = 1:length(sigma)
                D = [];
                for i = 1:kvec(kk)    %constructing random D
                    temp = randn(q,1);
                    % temp = ones(q,1)*randn(1,1);
                    d = diag(temp);
                    D = [D;d];
                end
                y1 = g(D*B(x)) + sigma(sig)*randn(size(D,1),1);
                
                %%%%%%%%%%%%%%%%%%%%%%%%% Recovery of the signal %%%%%%%%%%%%%%%%%%%%%%%%%
                %%-------- DMF --- 1D Tone estimation -------------------------
                alpha = 1000; % Grid seatch resolution
                u1 = zeros(kvec(kk),1);
                v1 = zeros(kvec(kk),1);
                z_hatDMF = zeros(q,1);
                zz = B(x);
                
                z_min = -1.5;
                z_max = 1.5;
                
                for j =1:q
                    u1 = y1(j:q:end);
                    v1 = D(j:q:end,j);
                    z_hatDMF(j) = recover_tone(u1,v1,g,z_min,z_max,alpha,flg);
                end
                %%%-------- MUSIC --- 1D Tone estimation ----------------------
                D = [];
                for i = 1:kvec(kk)    %constructing deterministic D
                    temp = i*ones(q,1);
                    d = diag(temp);
                    D = [D;d];
                end
                y2 = g(D*B(x)) + sigma(sig)*randn(size(D,1),1);
                
                Z_hatMUSIC = zeros(q,1);
                u2 = zeros(kvec(kk),1);  %Initialization of time samples
                for j =1:q
                    u2 = y2(j:q:end);  %time samples
                    [~,R] = corrmtx(u2,size(u2,1)-1,'mod');
                    [Z_hatMUSIC(j),~] = rootmusic(R,1,'corr');
                end
                %%%---------------- Sparse recovery ---------------------------
                switch opts.cs_alg
                    case 'iht'
                        tol = 0.01;
                        iter = 300;
                        eta = 0.2;
                        [x_hatDMF,~] = iht(z_hatDMF,B,BT,svec(ss),n,eta,tol,iter,opts);
                        [x_hatMUSIC,~] = iht(Z_hatMUSIC,B,BT,svec(ss),n,eta,tol,iter,log);
                    case 'cosamp'
                        opts.s = svec(ss);
                        opts.selectAtom = 2;
                        opts.tol=1e-4;
                        opts.maxIter=50;
                        opts.tol_early=0.005;
                        opts.debias = 1;
                        opts.hhs = 0;
                        [x_hatDMF,~,~] = cosamp('sparsity', z_hatDMF, B, BT, opts);
                        [x_hatMUSIC,~,~] = cosamp('sparsity', Z_hatMUSIC, B, BT, opts);
                end
                
                Re_errDMF(ss,sig,ee) = norm(x - x_hatDMF)/norm(x);
                Cos_similarityDMF(ss,sig,ee) = x'*x_hatDMF/(norm(x)*norm(x_hatDMF));
                
                Re_errMUSIC(ss,sig,ee) = norm(x - x_hatMUSIC)/norm(x);
                Cos_similarityMUSIC(ss,sig,ee) = x'*x_hatMUSIC/(norm(x)*norm(x_hatMUSIC));
                
                %------------ ONESHOT ---------------------------------------
                %             x_hatONESHOT = oneshot(y,D,BT,svec(ss));
                %             Re_errONESHOT(ss,kk,ee) = norm(x - x_hatONESHOT)/norm(x);
                %             Cos_similarityONESHOT(ss,kk,ee) = x'*x_hatONESHOT/(norm(x)*norm(x_hatONESHOT));
                %------------ GHT -------------------------------------------
                %             tol = 0.01;
                %             iter = 250;
                %             if kvec(kk) <= 10
                %                 eta = 0.04;
                %             else
                %                 eta = 0.01;
                %             end
                %             [x_hatGHT,~] = GHT(y,D,B,BT,g,svec(ss),n,eta,tol,iter,opts);
                %             Re_errGHT(ss,kk,ee) = norm(x - x_hatGHT)/norm(x);
                %             Cos_similarityGHT(ss,kk,ee) = x'*x_hatGHT/(norm(x)*norm(x_hatGHT));
                %%%-------------------------------------------------------------
            end
        end
    end
end

figure;
plot(sigma,mean(Re_errDMF,3),'-bo','LineWidth',2);
hold on
% % plot(sigma,mean(Re_errONESHOT,3),'-rs','LineWidth',2);
% plot(sigma,mean(Re_errGHT,3),'-g^','LineWidth',2);
plot(sigma,mean(Re_errMUSIC,3),'-k*','LineWidth',2);
xlim([0 sigma(sig)])
axisfortex('Sparsity level = 100, q = 800','Variance of the noise \sigma','Relative Error');
grid on
% legend('DMF','OneShot','GHT')
legend('DMF','MUSIC')

figure,
plot(sigma,mean(Cos_similarityDMF,3),'-bo','LineWidth',2);
hold on
% plot(sigma,mean(Cos_similarityONESHOT,3),'-rs','LineWidth',2);
% plot(sigma,mean(Cos_similarityGHT,3),'-g^','LineWidth',2);
plot(sigma,mean(Cos_similarityMUSIC,3),'-k*','LineWidth',2);
xlim([0 sigma(sig)])
axisfortex('Sparsity level = 100, q = 800','Variance of the noise \sigma','Cosine Simiarity');

grid on
% legend('DMF','OneShot','GHT')
legend('DMF','MUSIC')