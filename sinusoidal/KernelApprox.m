clc;clear;
close all;
%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = 1000;
s = 40;
kvec = 50;
q = 200;
Ampl = 1;
sigma = 10;
nor_factor = 1/500/20;
samples = 20;
opts.meas = 'gauss';
opts.diagblock = 'allrandom';
opts.link = 'sin';
%%%%%%%%%%%%% Constructing B %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch opts.meas
    case 'gauss'
        Bmat = (1/sqrt(q))*randn(q,n);
        B = @(x) Bmat*x; BT = @(x) Bmat'*x;
    case 'fourier'
        zeta = sign(randn(n,1));
        p = randperm(n)';
        que = randperm(n/2-1)+1; % this keeps a random set of FFT coefficients
        omega = que(1:q/2)';
        B = @(z) A_f(zeta.*z, omega, p);
        BT = @(z) zeta.*At_f(z, n, omega, p);
end
%%%%%%%%%%%%%%%%%%%%%%% link function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch opts.link
    case 'com_exp'
        g = @(x) exp(1i*x);
    case 'sin'
        g = @(x) sin(x);
end

%%%%%%%%%%%%% Constructing D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
D = [];
for j = 1:kvec
    switch  opts.diagblock
        case 'allrandom'
            temp = randn(q,1);
        case 'blockrandom'
            temp = ones(q,1)*randn(1);
        case 'blockidentity'
            temp = ones(q,1);
    end
    d = diag(temp);
    D = [D;d];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x = zeros(n,samples);
y = zeros(kvec*q,samples);

for i =1:samples
    suppx = randperm(n);
    suppx = suppx(1:s);
    x(suppx,i) = randn(s,1);
    x(:,i) = Ampl*x(:,i)/norm(x(:,i));
    
    y(:,i) = g(D*B(x(:,i)));
end
Y = nor_factor*(y'*y);

M = L2_distance(x,x);
K = exp(-M/sigma);


Y
K
Re_Error = norm(K - Y,'fro')/norm(K,'fro')
Cos_sim = cos(K(:)'*Y(:)/(norm(K(:))*norm(Y(:))))