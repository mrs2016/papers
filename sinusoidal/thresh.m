function [xhat,idx] = thresh(x,s)
xhat = 0*x;
[~,idx] = sort(abs(x),'descend');
idx = idx(1:s);
xhat(idx) = x(idx);
end
