close all
clc
addpath Utils/

n = 2^14;
s = 100;
m = 8*s;
m = round(m);

x0 = randn(n,1);
x = thresh(x0,s);


Amat = 1/sqrt(m)*randn(m,n);

A = @(x) Amat*x;
At = @(x) Amat'*x;

y = A(x);

%%%%%%%%%% Recover using cosamp

opts.s = s; % Target sparsity
opts.selectAtom = 2;
opts.tol=1e-4;
opts.maxIter=50;
opts.verbose=0;
opts.tol_early=0.005;
opts.debias = 1;
opts.hhs = 0;

[xhat_sparse,tcgs_sparse,tsparse] = cosamp('sparsity', y, A, At, opts);
err_sparse = norm(x-xhat_sparse)/norm(x)
times_sparse = tsparse;
