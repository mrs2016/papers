function [xhat,tcgs,tproj] = cosamp(recflag, y, A, At,opts)

%%%%%%%%%%%
temp = At(y);
n = length(temp);
err = 1;
x_est = zeros(n,1);
s_cosamp = zeros(n,1);
alph = opts.selectAtom;

iterCnt = 0; tcgs = 0; tproj = 0;

while ((err>opts.tol)&&(iterCnt<=opts.maxIter))
    iterCnt = iterCnt + 1;
    
    %disp(iterCnt);
    
    resid = y - A(x_est);
    proxy = At(resid);
    
    %------Estimation
    switch recflag
        case 'sparsity'
            s = opts.s;
            tstart_proj = tic;
            [~,idx] = thresh(proxy,round(alph*s));
            supp = 0*proxy; supp(idx) = 1;
            tproj = tproj + toc(tstart_proj);
    end
    
    if opts.hhs
        tt = find(supp);
        samples = resid;
    else
        tt = union(find(ne(s_cosamp,0)),find(supp));
        samples = y;
    end
    
    %------Least-squares
    PP_tt = @(z) A_I(A,z,tt,n);
    PP_transpose_tt = @(z) A_I_transpose(At,z,tt);
    qq = PP_transpose_tt(samples);
    PPtranspose_PP_tt = @(z) PP_transpose_tt(PP_tt(z));
    tstart_cgs = tic;
    w = cgsolve(PPtranspose_PP_tt,qq, 1e-4, 20, 0);
    tcgs = tcgs + toc(tstart_cgs);
    %w = cgs(PPtranspose_PP_tt,qq, 1e-4, 100);
    bb= 0*s_cosamp; bb(tt)= w;
    
    %------OPTIONAL: Merge
    if opts.hhs
        bb = s_cosamp + bb;
    end
    
    %------Prune
    switch recflag
        case 'sparsity'
            tstart_proj = tic;
            [~,idx] = thresh(bb,s);
            supp = 0*bb; supp(idx) = 1;
            tproj = tproj + toc(tstart_proj);
            s_cosamp = bb.*supp;
    end
    
    x_est = s_cosamp;
    
    errTemp = norm(A(x_est)-y)/norm(y);
    if (opts.verbose)
        fprintf('Iter: %d Err: %f\n',iterCnt,errTemp);
    end
    
    if (abs((err-errTemp)/err) <= opts.tol_early || errTemp <= opts.tol_early) %Early termination condition
        % err = errTemp;
        if (opts.verbose)
            fprintf('Terminating.\n');
        end
        break
    end
    err = errTemp;
end

if opts.debias
    tt = find(supp);
    PP_tt = @(z) A_I(A,z,tt,n);
    PP_transpose_tt = @(z) A_I_transpose(At,z,tt);
    qq = PP_transpose_tt(y);
    PPtranspose_PP_tt = @(z) PP_transpose_tt(PP_tt(z));
    tstart_cgs = tic;
    w = cgsolve(PPtranspose_PP_tt,qq, 1e-4, 100, 0);
    tcgs = tcgs + toc(tstart_cgs);
    xhat = 0*s_cosamp; xhat(tt)= w;
else
    xhat = x_est;
end

end

