clc;clear;
close all
addpath Utils/
%%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = 2^18;                % Ambient diemnsions of the signal
s = 7000;                % Sparsity of the signal

opts.basis = 'haar';
opts.cs_alg = 'cosampDIC';
opts.verbose = 0;   % = 1: print the errors,    = 0: no print for DMF and GHT
imageinput = 'peppers.jpg';

switch opts.basis
    case 'id'
        Phi = @(w) w;
        PhiT = @(w) w;
    case 'haar'
        Phi =  @(w) haar2D(w, 'inverse');
        PhiT = @(w) haar2D(w, 'forward');
end

Im = imread(imageinput);
Im = imresize(Im,[sqrt(n) sqrt(n)],'bicubic');
Im = im2double(rgb2gray(Im));
wavelet_coeff = PhiT(Im(:));
w = thresh(wavelet_coeff,s);
x = Phi(w);  %Im(:);

X = reshape(x,[],sqrt(n));
figure
imshow(X,[]);