clc;clear;
close all;

x = 700*pi/3;

a1 = randn;
a2 = randn;
y1 = sin(a1*x);
y2 = sin(a2*x);
k=-1000:1000;

f1 = ((-1).^k)*(asin(y1)/a1) + pi*k/a1;
f2 = ((-1).^k)*(asin(y2)/a2) + pi*k/a2;

dif = zeros(length(f1),length(f2));

for i =1:length(f1)
    for j = 1:length(f2)
        dif(i,j) = abs(f1(i)-f2(j));
    end
end
min(min(dif));

[r,c] = find( dif == min(min(dif)) );

fprintf('True angle = %f\n',x);
fprintf('The value of first arithmetic progression = %f\n',f1(r));
fprintf('The value of second arithmetic progression = %f\n',f2(c));

% plot(k,f1)
% hold on
% plot(k,f2,'r')