recopts.s1 = s1;
        recopts.s2 = s2;
        recopts.m = q;
        recopts.th = 0.001;
        recopts.it = 1500;
        recopts.init = 'oneshot';  % 'zeros', 'random'
        recopts.n = n;
        recopts.lambda = 1;
        recopts.eta = 1;
        
        recopts.recflag = 'totally_sparse'; 
%         recopts.recflag = 'individually_sparse'; 
%         recopts.eta = 5;
        recopts.verbose = 1;
        [xhat,w_hatm,z_hatm,~,~,~,~] = htdemix(z_hatDMF,f,B,BT,Phi,PhiT,Psi,PsiT,recopts);