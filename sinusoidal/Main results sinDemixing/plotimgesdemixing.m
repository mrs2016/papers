close all;
% X = reshape(x,[],sqrt(n));
% W = reshape(Phi(comp1_coeff),[],sqrt(n));
% Z = reshape(Psi(comp2_coeff),[],sqrt(n));

Xhat = reshape(xhat,[],sqrt(n));
w_hat = reshape(Phi(w_hatm),[],sqrt(n));
z_hat = reshape(Psi(z_hatm),[],sqrt(n));

% XO = reshape(xo,[],sqrt(n));
% WO = reshape(Phi(wo),[],sqrt(n));
% ZO = reshape(Psi(zo),[],sqrt(n));


figure,
imshow(X,[]);
print(gcf,'-r300','-dpdf','GS.pdf')
movefile('GS.pdf','~/Documents/Figs')

% figure,
% imshow(W,[])
% print(gcf,'-r300','-dpng','phiwT.png')
% movefile('phiwT.png','~/Documents/Figs')
% figure,
% imshow(Z,[])
% print(gcf,'-r300','-dpng','psizT.png')
% movefile('psizT.png','~/Documents/Figs')


% figure,
% imshow(Xhat,[])
% print(gcf,'-r300','-dpng','sHTM.png')
% movefile('sHTM.png','~/Documents/Figs')
figure,
imshow(w_hat,[])
print(gcf,'-r300','-dpdf','phiwHTM.pdf')
movefile('phiwHTM.pdf','~/Documents/Figs')
figure,
imshow(z_hat,[])
print(gcf,'-r300','-dpdf','psizHTM.pdf')
movefile('psizHTM.pdf','~/Documents/Figs')

% figure,
% imshow(XO,[])
% print(gcf,'-r300','-dpng','sOneshot.png')
% movefile('sOneshot.png','~/Documents/Figs')
% figure,
% imshow(WO,[])
% print(gcf,'-r300','-dpng','phiwOneshot.png')
% movefile('phiwOneshot.png','~/Documents/Figs')
% figure,
% imshow(ZO,[])
% print(gcf,'-r300','-dpng','psizOneshot.png')
% movefile('psizOneshot.png','~/Documents/Figs')
