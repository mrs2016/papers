function [x_hat,err_trace] = GHT(y,D,B,BT,g,s,n,eta,tol,iter,opts)
% A = D*B  and AT = BT*DT
log = opts.verbose;
err = 1;
x_est = zeros(n,1);
iterCnt = 0;

while ((err>tol) && (iterCnt<=iter))
    
    iterCnt = iterCnt + 1;
    err_trace(iterCnt) = err;
    
    x_prev = x_est;
    resid = y - g(D*B(x_prev));
    proxy = BT(D'*(resid));
    
    x_update = x_prev + eta*proxy;
    x_est = thresh(x_update,s);
    x_hat = x_est;
    
    err_temp = norm(resid)/norm(y);
    
    if log == 1
        fprintf('Iteration = %d,  Relative error = %f,  Norm = %f\n',iterCnt, err_temp, norm(x_hat));
    end
    if err_temp <= tol
        if log == 1
            fprintf('Terminating...\n')
        end
        break;
    end
%     if (iterCnt > 1) && (err_trace(iterCnt) - err_trace(iterCnt-1) <= 1e-10)
%         if log == 1
%             fprintf('Terminating...\n')
%         end
%         break;
%     end
    
    err = err_temp;
end