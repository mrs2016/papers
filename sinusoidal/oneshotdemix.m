function [xhat,w,z] = oneshot(y,~,AT,Phi,PhiT,Psi,PsiT,recopts)

s1 = recopts.s1;
s2 = recopts.s2;

m = length(y);

xlin = 1/m*AT(y);

w = PhiT(xlin);
w = thresh(w,s1);

z = PsiT(xlin);
z = thresh(z,s2);

xhat = Phi(w) + Psi(z);

end
